
global - tcpQueue de msgs -- https://www.geeksforgeeks.org/queue-set-1introduction-and-array-implementation/
       - mensagem lastStatus
       - struct Maquina
--------------------------------------------------------------------------


main
    receber id maquina , ip do server tcp e cadencia

    iniciar tcpQueue

    criar threads tcpMonitor(ipServer) e udpMonitor

    createHelloMsg(idMaq) e colocar na tcpQueue

    while(ficheiro has msgs)
        struct msg = createMSG(linhaFicheiro)
        adicionar msg à queue
        sleep(cadencia)
    --
--------------------------------------------------------------------------

tcpMonitor

    abrir socket

    while(1)
        esperar que haja msgs na queue
            caso haja envia a msg
        
        caso a msg enviada seja hello 
            receber ack ou nack e colocar msg no lastStatus
            caso nack
                shutdown ig?
        --
        
        ou

        receber ack ou nack e colocar msg no lastStatus
            caso nack
                shutdown ig?

    --
    fechar socket

--------------------------------------------------------------------------
udpMonitor
    abrir socket

    while(1)
        esperar por msg do SMM
        caso reset
            createHelloMsg(idMaq) e add à queue
        --
        enviar msg com lastStatus
    --
--------------------------------------------------------------------------

NOTAS DE DESENVOLVIMENTO:
    -Verificar metodo createMSG do header (possivel problema com a string passada por parametro)
    -Verificar ip passado por parametro para a thread tcp