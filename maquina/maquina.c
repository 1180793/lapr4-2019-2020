/*
 * maquina.c
 *
 * Created on: 25/05/2020
 * Author: isep
 */
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <netdb.h> 
#include <unistd.h> 
#include <time.h> 
#include <pthread.h> 
#include "header.h" 
#include <sys/ioctl.h> 
#include <openssl/crypto.h> 
#include <openssl/bio.h> 
#include <openssl/ssl.h> 
#include <openssl/err.h> 
#include <openssl/conf.h> 
#include <openssl/x509.h> 
#define SERVER_SSL_CERT_FILE "server_J.pem" 
#define BUF_SIZE 512 
#define SERVER_PORT "30602"
int sock; 

int helloFlag; 
int ra; 
SSL *sslConn;
//pthread_cond_t statusMutex; 
// read a string from stdin protecting buffer overflow


#define GETS(B,S) {fgets(B,S-2,stdin);B[strlen(B)-1]=0;}

void* maquina(void* args){
	int i;
	maq s = *((maq*) args);
	unsigned char bt;
	char *msg = (char*) malloc(MAX_SIZE);
	FILE *file;
	// SENDS HELLO MSG
	mensagem hello;
	hello.version = VERSION_BY_DEFAULT;
	hello.code = HELLO;
	hello.id = s.idMaquina;
	hello.dataLength = 0;
	SSL_write(sslConn, &hello, DEFAULT_LENGTH);
	
	//Waits for a ACK or NACK Response
	mensagem response;
	response.dataLength = 33;
	response.code = 13;
	response.version = 8;
		
	SSL_read(sslConn, &response.version, 1);
    SSL_read(sslConn, &response.code, 1);
	int id = 0;
		int aux = 1;
    	for (i = 0; i < 2; i++) {
			SSL_read(sslConn, &bt, 1);
			id = id + bt * aux;
			aux = aux * 256;
    }
	response.id = id;
	int dataLength = 0;
		aux = 1;
		for (i = 0; i < 2; i++) {
        	SSL_read(sslConn, &bt, 1);
        	dataLength = dataLength + bt * aux;
        	aux = aux * 256;
    }
	response.dataLength = dataLength;
	
	if(dataLength > 0) {
		response.rawData = malloc(response.dataLength + 1);
		SSL_read(sslConn, &response.rawData, response.dataLength);
	}
	printf("Message Received: \n");
	printf("Version: %d\n ", response.version);
	printf("ID: %d\n " ,response.id);
	printf("Code %d\n",response.code);
	printf("Data Length %d\n " ,response.dataLength);
	printf("Raw Data %s\n " ,response.rawData);
	
	if( response.code == NACK ){
		pthread_exit((void*)NULL);
	}
	int timer = s.cadencia;
	char fileName[] = "Mensagens.txt";
	if ((file = fopen(fileName, "r")) == NULL) {
		perror("fopen error...");
		pthread_exit((void*)NULL);
	}
	mensagem m;
	while (fgets(msg, MAX_SIZE, (FILE*) file) != NULL) {
		//SENDS MSG TO SCM
		m.version = VERSION_BY_DEFAULT;
		m.code = MSG;
		m.id = s.idMaquina;
		m.dataLength = strlen(msg);
		m.rawData = malloc( strlen(msg) + 1 );
		strcpy(m.rawData, msg);
		SSL_write(sslConn, &m, DEFAULT_LENGTH);
		for( i = 0; i < strlen(m.rawData); i++){
			SSL_write(sslConn, &m.rawData[i], 1);
		}
		//Waits for a ACK or NACK Response
		
		
		SSL_read(sslConn, &response.version, 1);
		SSL_read(sslConn, &response.code, 1);
		int idRes = 0;
		int auxRes = 1;
		for (i = 0; i < 2; i++) {
			SSL_read(sslConn, &bt, 1);
			idRes = idRes + bt * auxRes;
			auxRes = auxRes * 256;
		}
		response.id = idRes;
		int dataLengthRes = 0;
		auxRes = 1;
		for (i = 0; i < 2; i++) {
			SSL_read(sslConn, &bt, 1);
			dataLengthRes = dataLengthRes + bt * auxRes;
			auxRes = auxRes * 256;
		}
		response.dataLength = dataLengthRes;
	
		if(dataLengthRes > 0) {
			response.rawData = malloc(response.dataLength + 1);
			SSL_read(sslConn, &response.rawData, response.dataLength);
		}
		if( response.code == ACK ){
			printf("ACK\n");
			helloFlag = 1; //SEND TO UDP STATUS
		}else{
			printf("NACK\n");
			helloFlag = 0;
			fclose(file);
			pthread_exit((void*)NULL);
			
		}
		sleep(timer);
	}
	fclose(file);
	pthread_exit((void*)NULL);
}
int main(int argc, char **argv) {
	setvbuf(stdout,NULL,_IONBF,0);
	int err;
	char line[BUF_SIZE];
	//char linha[BUF_SIZE];
	struct addrinfo req, *list;
	
	//pthread_mutex_t mutexStatus;
	helloFlag=0;
	//pthread_mutex_init(&mutexStatus, NULL);
	if (argc != 5) {
		puts("Correct usage: ./maquina <address> <certificate> <id> <cadence>");
		exit(1);
	}
	
	bzero((char *) &req, sizeof(req));
	// let getaddrinfo set the family depending on the supplied server address
	req.ai_family = AF_UNSPEC;
	req.ai_socktype = SOCK_STREAM;
	err = getaddrinfo(argv[1], SERVER_PORT, &req, &list);
	if (err) {
		printf("Failed to get server address, error: %s\n", gai_strerror(err));
		exit(1);
	}
	sock = socket(list->ai_family, list->ai_socktype, list->ai_protocol);
	if (sock == -1) {
		perror("Failed to open socket");
		freeaddrinfo(list);
		exit(1);
	}
	if (connect(sock, (struct sockaddr *) list->ai_addr, list->ai_addrlen) == -1) {
			perror("Failed connect");
			freeaddrinfo(list);
			close(sock);
			exit(1);
	}
	const SSL_METHOD *method=SSLv23_client_method();
    	SSL_CTX *ctx = SSL_CTX_new(method);
	
	// Load client's certificate and key
	strcpy(line,argv[2]);strcat(line,".pem");
	SSL_CTX_use_certificate_file(ctx, line, SSL_FILETYPE_PEM);
	strcpy(line,argv[2]);strcat(line,".key");
	SSL_CTX_use_PrivateKey_file(ctx, line, SSL_FILETYPE_PEM);
	if (!SSL_CTX_check_private_key(ctx)) {
		puts("Error loading client's certificate/key");
		close(sock);
		exit(1);
	}
	
	SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER,NULL);
        // THE SERVER'S CERTIFICATE IS TRUSTED
        SSL_CTX_load_verify_locations(ctx,SERVER_SSL_CERT_FILE,NULL);
	// Restrict TLS version and cypher suites
        SSL_CTX_set_min_proto_version(ctx,TLS1_2_VERSION);
	SSL_CTX_set_cipher_list(ctx, "HIGH:!aNULL:!kRSA:!PSK:!SRP:!MD5:!RC4");
	sslConn = SSL_new(ctx);
        SSL_set_fd(sslConn, sock);
        if(SSL_connect(sslConn)!=1) {
		puts("TLS handshake error");
                SSL_free(sslConn);
                close(sock);
                exit(1);
	}
	printf("TLS version: %s\nCypher suite: %s\n",SSL_get_cipher_version(sslConn),SSL_get_cipher(sslConn));
	if(SSL_get_verify_result(sslConn)!=X509_V_OK) {
		puts("Sorry: invalid server certificate");
                SSL_free(sslConn);
                close(sock);
                exit(1);
        }
	X509* cert=SSL_get_peer_certificate(sslConn);
        X509_free(cert);
        if(cert==NULL) {
                puts("Sorry: no certificate provided by the server");
                SSL_free(sslConn);
                close(sock);
                exit(1);
        }
	
	
	//Criação da Máquina
	
	//printf("id da maquina\n");
	//scanf(" [^\n]%hu", &id);
	//printf("candencia:");
	//scanf(" [^\n]%d", &cad);
	
	maq sMaq;
	//----
	sMaq.idMaquina = atoi(argv[3]);
	sMaq.cadencia = atoi(argv[4]);
	//----
	//pthread_t thread[2];
	pthread_t thread;
	printf("Machine connected to SCM!\n");
	pthread_create(&thread, NULL, maquina, (void*) &sMaq);
	//pthread_create(&thread[1], NULL, UDPMonitor, (void*) sMaq);
	
	//for(i = 0; i < 1; i++){
	pthread_join(thread, (void*) NULL);
	SSL_free(sslConn);
	//}
	//pthread_mutex_destroy(statusMutex);
	close(sock);
	printf("The machine is powering off...\n");
	return 0;
}
