/*
 * header.h
 *
 * Created on: 26/05/2020
 * Author: isep
 */
#ifndef HEADER_H_ 
#define HEADER_H_ 
#define MAX_SIZE 500 
#define POS 3 
#define N_THREADS 1 
#define VERSION_BY_DEFAULT 0 
#define HELLO 0 
#define MSG 1 
#define CONFIG 2 
#define RESET 3 
#define ACK 150 
#define NACK 151 
#define DEFAULT_LENGTH 6 
#define SERVER_PORT "30602" 
#define FILE_MAQUINAS "Maquinas.txt"

typedef struct{
	unsigned int idMaquina;
	int cadencia;
}maq;
typedef struct{
	unsigned char version;
	unsigned char code;
	unsigned short id;
	unsigned short dataLength;
	char* rawData;
}mensagem;
#endif /* HEADER_H_ */

