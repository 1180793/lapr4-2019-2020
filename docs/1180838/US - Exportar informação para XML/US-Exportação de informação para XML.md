# 1. Requisitos

O Gestor de Produção inicia a exportação da informação para um ficheiro XML. O sistema pede informação necessária à exportação dos dados do chão de fábrica(i.e. nome do ficheiro, informação a exportar e filtros temporais(opcional)). O Gestor introduz a informação requirida. O sistema reúne a informação desejada e procede à exportação XML.

# 2. Análise

## Requisitos 

    * N/A

# 3. Design

![US2007.png](US2007.png)

## 3.1. Realização da Funcionalidade

1. O Gestor de Produção inicia a exportação da informação para um ficheiro XML. 
2. O sistema pede informação necessária à exportação dos dados do chão de fábrica(i.e. nome do ficheiro, informação a exportar).
3. O Gestor introduz a informação requirida. 
4. O sistema dá a opção da utilização de filtros temporais em dados que suportem.
5. O Gestor aceita e introduz filtros temporais(data inicial e final).
6. O sistema reúne a informação desejada e procede à exportação XML.
