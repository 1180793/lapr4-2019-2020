# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas

#### Classes de Dominio
  * OrdemProducao

#### User Interface
  * AdicionarOrdemProducaoUI

#### Controlador
  * AdicionarOrdemProducaoController

#### Repository
  * CategoriaMateriaPrimaRepository

# Diagram de Sequência
![US 2010 - System Diagram.png](US2010_SystemDiagram.png)
