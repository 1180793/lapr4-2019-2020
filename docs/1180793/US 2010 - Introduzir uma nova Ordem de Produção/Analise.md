# Analise OO

* O "menu manager" utiliza a aplicação backoffice para adicionar uma nova ordem de produção à aplicação.
* A ordem de produção é caracterizada por um código (alfanumérico), uma data de emissão, uma data prevista de execução, produto, quantidade do produto, unidade e encomendas associadas.


# Regras de Negócio

* Uma ordem de produção deve ser unica


# Testes Unitários

* Verificar se não é possivel adicionar ordens de produção com códigos iguais.
* Verificar se não é possivel adicionar ordens de produção com data de execução anterior à data prevista de execução.
