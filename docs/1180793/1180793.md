**Gonçalo Corte Real - 1180793**
===============================

### Índice das Funcionalidade Desenvolvidas ###

| Sprint | User Story |  Funcionalidade   |
|--------|------------|--------------------|
| **B**  |    1005    | Inicialização (bootstrap) de algumas categorias de matérias-primas. |
| **B**  |    1006    | Inicialização (bootstrap) de alguns produtos. |
| **B**  |    2002    | Definir uma nova categoria de matérias-primas. |
| **C**  |    2010    | Introduzir manualmente uma nova ordem de produção. |
| **C**  |    4001    | Importar as mensagens existentes no diretório especificado, de forma a disponibilizar as mesmas para processamento. |
| **C**  |    4002    | Recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção. |
| **C**  |    5001    | Processamento das mensagens disponiveis no sistema. |
| **D**  |    5002    | Processamento das mensagens disponiveis no sistema. |
| **D**  |    1015    | Proteger comunicações entre o simulador de máquina e o SCM. |
