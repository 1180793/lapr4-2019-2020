# Design

* Para o Design foi utilizado o controlador já implementado no US 2002 (Definir uma Categoria de Matérias-Primas) de forma a evitar a duplicação de código. O diagrama de sequência que demonstra o funcionamento do controller encontra-se presente no mesmo US.

#### Classes de Dominio
  * CategoriaMateriaPrima

#### Controlador
  * EspecificarCategoriaMateriaPrimaController

#### Repository
  * JpaCategoriaMateriaPrimaRepository
