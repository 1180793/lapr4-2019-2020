# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas

#### Classes de Dominio
  * CatalogoMateriaPrima

#### User Interface
  * EspecificarCategoriaMateriaPrimaUI

#### Controlador
  * EspecificarCategoriaMateriaPrimaController

#### Repository
  * CategoriaMateriaPrimaRepository

# Diagram de Sequência
![US 2002 - System Diagram.png](US2002_SystemDiagram.png)
