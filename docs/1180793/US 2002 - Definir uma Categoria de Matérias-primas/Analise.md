# Analise OO

* O "menu manager" utiliza a aplicação backoffice para adicionar categorias de matérias-primas à aplicação.
* A categoria de matéria-prima é caracterizada por um código (alfanumérico com 10 caracteres no máximo) e uma descrição.


# Regras de Negócio

* Uma categoria materia-prima:
    * deve ser unica
	* deve ser identificada por um código alfanumérico não nulo e com 10 caracteres no máximo


# Testes Unitários
* Verificar se não é possivel adicionar categorias com códigos iguais.
