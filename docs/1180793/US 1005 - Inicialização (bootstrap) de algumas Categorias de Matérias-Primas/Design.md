# Design

* Para o Design foi utilizado o controlador já implementado no US 2006 (Adicionar um novo Produto ao Catálogo de Produtos) de forma a evitar a duplicação de código. O diagrama de sequência que demonstra o funcionamento do controller encontra-se presente no mesmo US.

#### Classes de Dominio
  * Produto

#### Controlador
  * AdicionarProdutoController

#### Repository
  * JpaProdutoRepository
