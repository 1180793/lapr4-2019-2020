# Projeto Integrador da LEI-ISEP Sem4 2019-20

# 1. Constituição do Grupo de Trabalho

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **[1160814](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1160814/)** | José Pessoa|
| **[1171156](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1171156/)**  | David Monteiro|
| **[1180793](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180793/)**  | Gonçalo Corte|
| **[1180837](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180837/)**  | Vasco Furtado|
| **[1180838](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180838/)**  | Gil Pereira|



# 2. Distribuição de Funcionalidades ###

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

| Aluno Nr.	| Sprint B | Sprint C | Sprint D |
|------------|----------|----------|----------|
| [**1160814**](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1160814/)| [USDemo1](/docs/USDemo1)| [USDemo2](/docs/USDemo2)| [USDemo3](/docs/USDemo3) |
| [**1171156**](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1171156/)| [USDemo1](/docs/USDemo1)| [USDemo2](/docs/USDemo2)| [USDemo3](/docs/USDemo3) |
| [**1180793**](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180793/)| [USDemo1](/docs/USDemo1)| [USDemo2](/docs/USDemo2)| [USDemo3](/docs/USDemo3) |
| [**1180837**](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180837/)| [USDemo1](/docs/USDemo1)| [USDemo2](/docs/USDemo2)| [USDemo3](/docs/USDemo3) |
| [**1180838**](https://bitbucket.org/1180793/lei_isep_2019_20_sem4_2df_1180793_1180837_1171156_1160814/src/master/docs/1180838/)| [USDemo1](/docs/USDemo1)| [USDemo2](/docs/USDemo2)| [USDemo3](/docs/USDemo3) |
