# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas



#### Classes de Dominio
* MateriaPrima
* CategoriaMateriaPrima  

#### Controlador
  * RegistaMateriaPrimaController

#### Repository
  * MateriaPrimaRepository
  * CategoriaMateriaPrimaRepository
# Diagram de Sequência
![UC-2001-SD.jpg](UC-2001-SD.jpg)
