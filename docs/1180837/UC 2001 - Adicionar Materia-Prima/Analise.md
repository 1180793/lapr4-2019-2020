# Analise OO

* O "gestor de produção" utiliza a aplicação backoffice para adicionar matérias-primas ao catalogo de matérias-primas.
* A matéria-prima é caracterizada por uma descrição e por um código interno.
* Possui uma ficha técnica.
* Cada materia-prima é caracterizada por uma categoria.



# Regras de Negócio
* Uma materia-prima:


    * nao pode ser um produto final
    * é usada no fabrico de um ou mais produtos
    * so pode estar inserida numa categoria
    * é identificada pelo codigo interno


# Testes Unitários
* Verificar se a materia-prima possui uma categoria
* Verificar se o codigo interno é valido
