# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas

#### Classes de Dominio
  * OrdemProducao


#### User Interface
  * ImportarOrdensProducaoUI

#### Controlador
  * ImportarOrdensProducaoController

#### Repository
  * OrdemProducaoRepository

# Diagram de Sequência
![US2009-SD.jpg](US2009-SD.jpg)
