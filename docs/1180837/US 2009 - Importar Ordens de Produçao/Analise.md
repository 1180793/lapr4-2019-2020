# Analise OO

* O gestor de produção utiliza a aplicação backoffice para importar as ordens de produção através dum ficheiro csv.
* A coluna do ficheiro csv que contem as datas tem de ser segundo a seguinte formatação: dd/MM/yyyy


# Regras de Negócio
* Quantidade do produto não pode ser negativa
* Código de fabrico deve referenciar um produto já instanciado
* Data prevista de execução deve ser posteriori á data de emissão.



# Testes Unitários
* Verificar se não e possível adicionar ordens de produção com códigos iguais.
* Verificar se o código de fabrico corresponde a um produto conhecido.
* Verificar se a data prevista de execução é posteriori á data de emissão.
