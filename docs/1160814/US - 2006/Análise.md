# Analise

* Um produto caracteriza-se por um código de fabrico, um código comercial, uma descrição breve e outra completa, uma uniade de medida e uma categoria.
* O código de fabrico do produto deve ser único.
* Deve ser criada e associada a cada produto a informação relativa à sua ficha de produção.
* A inexistência de uma ficha de produção inviabiliza a produção do produto.


# Regras de Negócio

* O código de fabrico do produto é um código alfanumérico e tem no maximo 15 caracteres. 
* um produto pode ser utilizado como matéria-prima para a produção de outro produto.


# Testes Unitários

* O produto tem que ter um código de fabrico.
* O código de fabrico do produto não pode ser vazio nem ter espaço(s).
* O produto tem que ter um código comercial.
* O código comercial do produto não pode ser vazio nem ter espaço(s).
* O produto tem que ter uma descrição breve e outra completa.
* O produto tem que ter uma uniade de medida.
* O produto tem que ter uma categoria.