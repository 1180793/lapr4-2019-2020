# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas

#### Classes de Dominio
* Produto

#### Controlador
  * AddProdutoController

#### Repository
  * ProdutoRepository

# Diagrama de Sequência
![SD - Add Produto.png](SD - Add Produto.png)
