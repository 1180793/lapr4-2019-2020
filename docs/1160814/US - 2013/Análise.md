# Analise

* Uma visualização/transformação é aplicada a um ficheiro XML anteriormente gerado/exportado através do sistema.


# Regras de Negócio

* Uma visualização/transformação a um ficheiro XML pode ser gerada para HTML, JSON ou texto.
* Uma visualização/transformação a um ficheiro XML é feita sempre através de XSLT.
* Deve ser gerado outro ficheiro para o resultado/output, mantendo o ficheiro original inalterado.


# Testes Unitários

* O ficheiro XML de input têm de existir.
* O ficheiro XSLT de input têm de existir.
* O nome do ficheiro de output não pode ser vazio nem ter espaços.
* Verificar a integridade dos ficheiros de input.