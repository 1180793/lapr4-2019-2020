# Analise

* Linha de produção é uma sequência continua de máquinas.
* Uma Linha de produção caracteriza-se por um identificador.
* O identificador da linha de produção deve ser único. 

# Regras de Negócio

* O identificador da linha de produção é um código alfanumérico.

# Testes Unitários

* A linha de produção tem que ter um identificador.
* O identificador da linha de produção não pode ser vazio nem ter espaço(s).
