# Design

* Utilizar a estrutura base standard da aplicaçao baseada em camadas

#### Classes de Dominio
* LinhaProducao

#### Controlador
* EspecificarLinhaProducaoController

#### Repository
* LinhaProducaoRepository

# Diagrama de Sequência
![SD - Especificar Linha de Produção.png](SD - Especificar Linha de Produção.png)
