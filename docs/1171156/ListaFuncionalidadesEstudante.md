** Aluno Exemplo [1171156](./)**
===============================


### Índice das Funcionalidade Desenvolvidas ###


| Sprint | Funcionalidade     |
|--------|--------------------|
| **B**  | [US-2003](Consultar os produtos que não têm Ficha de Produção definida) |
| **B**  | [US-3003](Especificar um novo depósito) |
| **C**  | [US-1010](Especificar Documento XSD a ser usado) |
| **C**  | [US-2011](Consultar as ordens de produção por estado) |
| **C**  | [US-2012](Consultar as ordens de produção de uma dada encomenda) |
| **D**  | [US-1013](ProtegerComunicaçõesEntreSCMeMaquinas) |
| **D**  | [US-1015](ProtegerComunicaçõesEntreMaquinasESCM) |
