/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author PC
 */
public class InMemoryFichaProducaoRepository extends InMemoryDomainRepository<FichaProducao, CodFabrico> implements FichaProducaoRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<FichaProducao> findByCodigo(final CodFabrico who) {
        return match(e -> e.identity().equals(who));
    }

}
