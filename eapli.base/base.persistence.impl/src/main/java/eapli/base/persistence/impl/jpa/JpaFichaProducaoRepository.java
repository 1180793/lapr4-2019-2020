/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author PC
 */
public class JpaFichaProducaoRepository extends BaseJpaRepositoryBase<FichaProducao, Long, CodFabrico>
        implements FichaProducaoRepository {

    public JpaFichaProducaoRepository() {
        super("");
    }

    @Override
    public Iterable<FichaProducao> findByCodigo(CodFabrico who) {
        final Map<String, Object> params = new HashMap<>();
        params.put("codigoInterno", who);
        return match("e.Produto = :codigoInterno", params);
    }


}
