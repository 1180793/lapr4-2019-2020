/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * @author David
 */
public class JpaLinhasProducaoRepository extends BaseJpaRepositoryBase<LinhaProducao, Long, Identificador> implements LinhasProducaoRepository {

    JpaLinhasProducaoRepository() {
        super("identificador");
    }

    @Override
    public Iterable<LinhaProducao> linhasProducao() {
        return findAll();
    }
    
    @Override
    public Iterable<LinhaProducao> findByCodigo(Identificador who) {
        final Map<String, Object> params = new HashMap<>();
        params.put("identificador", who);
        return match("e.identificador = :identificador", params);
    }
}
