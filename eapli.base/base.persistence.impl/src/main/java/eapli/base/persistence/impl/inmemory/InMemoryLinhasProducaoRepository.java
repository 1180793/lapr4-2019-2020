/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author David
 */
public class InMemoryLinhasProducaoRepository extends InMemoryDomainRepository<LinhaProducao, Identificador> implements LinhasProducaoRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<LinhaProducao> linhasProducao() {
        return findAll();
    }

    @Override
    public Iterable<LinhaProducao> findByCodigo(final Identificador who) {
        return match(e -> e.identificador().equals(who));
    }

}
