/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author David
 */
public class InMemoryDepositosRepository extends InMemoryDomainRepository<Deposito, CodigoInternoDeposito> implements DepositosRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<Deposito> depositos() {
        return findAll();
    }

    @Override
    public Iterable<Deposito> findByCodigo(CodigoInternoDeposito who) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
