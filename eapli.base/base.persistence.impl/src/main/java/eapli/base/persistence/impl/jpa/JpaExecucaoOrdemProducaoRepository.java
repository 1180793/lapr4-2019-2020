/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class JpaExecucaoOrdemProducaoRepository extends BaseJpaRepositoryBase<ExecucaoOrdemProducao, Long, CodInternoOrdemDeProducao> implements ExecucaoOrdemProducaoRepository {

    public JpaExecucaoOrdemProducaoRepository() {
        super("codInterno");
    }

    @Override
    public ExecucaoOrdemProducao findByOrdem(CodInternoOrdemDeProducao who) {
        for (ExecucaoOrdemProducao execucao : findAll()) {
            if (execucao.identity().equals(who)) {
                return execucao;
            }
        }
        return null;
    }

}
