/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.persistence.impl.inmemory;

import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InMemoryExecucaoOrdemProducaoRepository  extends InMemoryDomainRepository<ExecucaoOrdemProducao, CodInternoOrdemDeProducao> implements ExecucaoOrdemProducaoRepository {

    @Override
    public ExecucaoOrdemProducao findByOrdem(CodInternoOrdemDeProducao who) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
