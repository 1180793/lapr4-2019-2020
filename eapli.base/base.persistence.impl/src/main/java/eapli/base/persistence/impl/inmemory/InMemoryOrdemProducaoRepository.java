/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author David
 */
public class InMemoryOrdemProducaoRepository extends InMemoryDomainRepository<OrdemProducao, CodInternoOrdemDeProducao> implements OrdemProducaoRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<OrdemProducao> ordensProducao() {
        return findAll();
    }

    @Override
    public Iterable<OrdemProducao> findByCodigo(final CodInternoOrdemDeProducao who) {
        return match(e -> e.identity().equals(who));
    }

    @Override
    public Iterable<OrdemProducao> findByStatus(final Status status) {

        return match(e -> e.status().equals(status));

    }

    @Override
    public Iterable<OrdemProducao> findByEncomenda(Encomenda order) {
        
//         return match(e -> e.encomenda().equals(order));
         return null;
        
    }

}
