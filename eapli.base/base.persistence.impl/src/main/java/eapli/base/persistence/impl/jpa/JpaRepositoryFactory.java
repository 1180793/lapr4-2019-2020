package eapli.base.persistence.impl.jpa;

import eapli.base.Application;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.JpaAutoTxUserRepository;
import eapli.framework.infrastructure.repositories.impl.jpa.JpaAutoTxRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.repositories.ProdutoRepository;

/**
 *
 * Created by nuno on 21/03/16.
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public UserRepository users(final TransactionalContext autoTx) {
        return new JpaAutoTxUserRepository(autoTx);
    }

    @Override
    public UserRepository users() {
        return new JpaAutoTxUserRepository(Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }

    @Override
    public JpaClientUserRepository clientUsers(final TransactionalContext autoTx) {
        return new JpaClientUserRepository(autoTx);
    }

    @Override
    public JpaClientUserRepository clientUsers() {
        return new JpaClientUserRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext autoTx) {
        return new JpaSignupRequestRepository(autoTx);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return new JpaSignupRequestRepository(Application.settings().getPersistenceUnitName());
    }

    @Override
    public CategoriaMateriaPrimaRepository categoriasMateriaPrima() {
        return new JpaCategoriaMateriaPrimaRepository();
    }

    @Override
    public TransactionalContext newTransactionalContext() {
        return JpaAutoTxRepository.buildTransactionalContext(Application.settings().getPersistenceUnitName(),
                Application.settings().getExtendedPersistenceProperties());
    }

    @Override
    public FichaProducaoRepository fichaProducao() {
        return new JpaFichaProducaoRepository();
    }

    @Override
    public LinhasProducaoRepository linhasProducao() {
        return new JpaLinhasProducaoRepository();
    }
    
    public OrdemProducaoRepository ordensProducao(){
        return new JpaOrdemProducaoRepository();
    }

    @Override
    public DepositosRepository depositos() {
        return new JpaDepositosRepository();
    }

    @Override
    public MateriaPrimaRepository materiasPrimas() {
        return new JpaMateriaPrimaRepository();
    }

    @Override
    public MaquinasRepository maquinas() {
        return new JpaMaquinaRepository();
    }

    @Override
    public ProdutoRepository produtos() {
        return new JpaProdutoRepository();
    }
    
    @Override
    public MensagemRepository mensagens() {
        return new JpaMensagemRepository();
    }

    @Override
    public ExecucaoOrdemProducaoRepository execucaoOrdensProducao() {
        return new JpaExecucaoOrdemProducaoRepository();
    }

}
