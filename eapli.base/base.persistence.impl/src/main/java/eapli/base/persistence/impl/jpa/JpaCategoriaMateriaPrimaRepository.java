/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.materiaprima.domain.CategoriaMateriaPrima;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;

/**
 *
 * @author Goncalo
 */
public class JpaCategoriaMateriaPrimaRepository extends BaseJpaRepositoryBase<CategoriaMateriaPrima, Long, String>
        implements CategoriaMateriaPrimaRepository {

    JpaCategoriaMateriaPrimaRepository() {
        super("codAlfaNumerico");
    }

    @Override
    public Iterable<CategoriaMateriaPrima> categoriasMateriaPrima() {
        return findAll();
    }
}
