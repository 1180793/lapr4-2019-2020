/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author PC
 */
public class JpaMaquinaRepository extends BaseJpaRepositoryBase<Maquina, Long, CodigoInternoMaquina> implements MaquinasRepository {

    public JpaMaquinaRepository() {
        super("codInterno");
    }

    @Override
    public Iterable<Maquina> findByCodigo(CodigoInternoMaquina cod) {
        final Map<String, Object> params = new HashMap<>();
        params.put("codInterno", cod);
        return match("e.codInterno = :codInterno", params);
    }

//    @Override
//    public Iterable<Maquina> findByCodigoComunicacao(Long id) {
//        final Map<String, Object> params = new HashMap<>();
//        params.put("uniqueIdentity", id);
//        return match("e.uniqueIdentity = :uniqueIdentity", params);
//
//    }

    @Override
    public Iterable<Maquina> findByCodigoComunicacao(Long id) {
        Iterable<Maquina> maquinas = findAll();
        List<Maquina> maqRet = new ArrayList<>();

        for (Maquina maq : maquinas) {
            if (Objects.equals(maq.identificadorUnico(), id)) {
                maqRet.add(maq);
            }
        }
        return maqRet;

    }
}
