/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author David
 */
public class JpaDepositosRepository extends BaseJpaRepositoryBase<Deposito, Long, CodigoInternoDeposito> implements DepositosRepository {

    JpaDepositosRepository() {
        super("codInterno");
    }

    @Override
    public Iterable<Deposito> depositos() {
        return findAll();
    }

    @Override
    public Iterable<Deposito> findByCodigo(CodigoInternoDeposito who) {
        final Map<String, Object> params = new HashMap<>();
        params.put("codInterno", who);
        return match("e.codInterno = :codInterno", params);
    }

}
