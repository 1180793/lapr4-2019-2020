/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.materiaprima.domain.CategoriaMateriaPrima;
import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.domain.MateriaPrima;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class InMemoryMateriaPrimaRepository extends InMemoryDomainRepository<MateriaPrima, CodigoInterno>
        implements MateriaPrimaRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<MateriaPrima> materiasPrimas() {
        throw new UnsupportedOperationException();
    }

}
