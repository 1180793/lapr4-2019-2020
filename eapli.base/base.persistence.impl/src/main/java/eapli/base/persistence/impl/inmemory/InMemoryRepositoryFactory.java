package eapli.base.persistence.impl.inmemory;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;
import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.infrastructure.bootstrapers.BaseBootstrapper;
import eapli.base.infrastructure.persistence.RepositoryFactory;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;
import eapli.framework.infrastructure.authz.repositories.impl.InMemoryUserRepository;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new BaseBootstrapper().execute();
    }

    @Override
    public UserRepository users(final TransactionalContext tx) {
        return new InMemoryUserRepository();
    }

    @Override
    public UserRepository users() {
        return users(null);
    }

    @Override
    public ClientUserRepository clientUsers(final TransactionalContext tx) {

        return new InMemoryClientUserRepository();
    }

    @Override
    public ClientUserRepository clientUsers() {
        return clientUsers(null);
    }

    @Override
    public SignupRequestRepository signupRequests() {
        return signupRequests(null);
    }

    @Override
    public CategoriaMateriaPrimaRepository categoriasMateriaPrima() {
        return new InMemoryCategoriaMateriaPrimaRepository();
    }

    @Override
    public SignupRequestRepository signupRequests(final TransactionalContext tx) {
        return new InMemorySignupRequestRepository();
    }


    @Override
    public TransactionalContext newTransactionalContext() {
        // in memory does not support transactions...
        return null;
    }

    @Override

    public MateriaPrimaRepository materiasPrimas() {
        return new InMemoryMateriaPrimaRepository();
    }

    @Override
    public FichaProducaoRepository fichaProducao() {
        return new InMemoryFichaProducaoRepository();
    }

    @Override
    public LinhasProducaoRepository linhasProducao() {
        return new InMemoryLinhasProducaoRepository();
    }
    
    @Override
    public OrdemProducaoRepository ordensProducao() {
        return new InMemoryOrdemProducaoRepository();
    }

    @Override
    public DepositosRepository depositos() {
        return new InMemoryDepositosRepository();
    }

    @Override
    public MaquinasRepository maquinas() {
        return new InMemoryMaquinasRepository();
    }

    @Override
    public ProdutoRepository produtos() {
        return new InMemoryProdutoRepository();
    }

    @Override
    public MensagemRepository mensagens() {
        return new InMemoryMensagemRepository();
    }

    @Override
    public ExecucaoOrdemProducaoRepository execucaoOrdensProducao() {
        return new InMemoryExecucaoOrdemProducaoRepository();
    }
}
