/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author David
 */
public class JpaOrdemProducaoRepository extends BaseJpaRepositoryBase<OrdemProducao, Long, CodInternoOrdemDeProducao> implements OrdemProducaoRepository {
    
    JpaOrdemProducaoRepository() {
        super("code");
    }

    @Override
    public Iterable<OrdemProducao> ordensProducao() {
        return findAll();
    }
    
    @Override
    public Iterable<OrdemProducao> findByCodigo(CodInternoOrdemDeProducao who) {
        final Map<String, Object> params = new HashMap<>();
        params.put("code", who);
        return match("e.code = :code", params);
    }
    
     @Override
    public Iterable<OrdemProducao> findByStatus(Status status) {
        final Map<String, Object> params = new HashMap<>();
        params.put("status", status);
        return match("e.status = :status", params);
    }
    
    @Override
    public Iterable<OrdemProducao> findByEncomenda(Encomenda encomenda) {
//        final Map<String, Object> params = new HashMap<>();
//        params.put("encomenda", encomenda);
//        return match("e.encomenda = :encomenda", params);
          List<OrdemProducao> result = new ArrayList<>();  
          Iterable<OrdemProducao> all = ordensProducao();
          for (OrdemProducao o:all){
              if(o.encomenda().contains(encomenda))
                  result.add(o);
          }
          if(result.isEmpty()){
              return null;
          } else return result;
    }
}
