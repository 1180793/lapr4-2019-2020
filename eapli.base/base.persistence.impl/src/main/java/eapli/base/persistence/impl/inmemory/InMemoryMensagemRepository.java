/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.persistence.impl.inmemory;

import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InMemoryMensagemRepository  extends InMemoryDomainRepository<Mensagem, Long> implements MensagemRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<Mensagem> mensagens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Mensagem> findByTipoMensagem(TipoMensagem tipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Mensagem> findByLinhaProducao(LinhaProducao linha) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Mensagem> findByMaquina(Maquina maquina) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Mensagem> findPorProcessarByLinhaProducao(LinhaProducao linha) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Mensagem> findPorProcessarByMaquina(Maquina maquina) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
