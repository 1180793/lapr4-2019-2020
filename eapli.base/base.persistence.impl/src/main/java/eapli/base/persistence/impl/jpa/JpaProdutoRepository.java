/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class JpaProdutoRepository extends BaseJpaRepositoryBase<Produto, Long, CodFabrico> implements ProdutoRepository {

    public JpaProdutoRepository() {
        super("codFabrico");
    }

    @Override
    public Iterable<Produto> produtos() {
        return findAll();
    }

    @Override
    public Iterable<Produto> findProdutoByCodigoFabrico(CodFabrico codigo) {
        final Map<String, Object> params = new HashMap<>();
        params.put("codFabrico", codigo);
        return match("e.codFabrico = :codFabrico", params);
    }

    @Override
    public Iterable<Produto> findProdutosWithoutFichaProducao() {
        List<Produto> lstProd = new ArrayList<>();
        for (Produto prod : produtos()) {
            if (prod.fichaProducao() == null) {
                lstProd.add(prod);
            }
        }
        return lstProd;

    }

}
