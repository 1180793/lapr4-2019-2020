package eapli.base.persistence.impl.jpa;

import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class JpaMensagemRepository extends BaseJpaRepositoryBase<Mensagem, Long, Long> implements MensagemRepository {

    public JpaMensagemRepository() {
        super("id");
    }

    @Override
    public Iterable<Mensagem> mensagens() {
        return findAll();
    }

    @Override
    public Iterable<Mensagem> findByTipoMensagem(TipoMensagem tipo) {
        List<Mensagem> lstMensagens = new ArrayList<>();
        for (Mensagem m : mensagens()) {
            lstMensagens.add(m);
        }
        return lstMensagens;
    }

    @Override
    public Iterable<Mensagem> findByLinhaProducao(LinhaProducao linha) {
        List<Mensagem> lstMensagens = new ArrayList<>();
        for (Mensagem msg : mensagens()) {
            if (msg.maquina() != null) {
                if (msg.maquina().linhaProducao().sameAs(linha)) {
                    lstMensagens.add(msg);
                }
            }
        }
        return lstMensagens;
    }

    @Override
    public Iterable<Mensagem> findByMaquina(Maquina maquina) {
        List<Mensagem> lstMensagens = new ArrayList<>();
        for (Mensagem msg : mensagens()) {
            if (msg.maquina() != null) {
                if (msg.maquina().sameAs(maquina)) {
                    lstMensagens.add(msg);
                }
            }
        }
        return lstMensagens;
    }

    @Override
    public Iterable<Mensagem> findPorProcessarByLinhaProducao(LinhaProducao linha) {
        List<Mensagem> lstMensagens = new ArrayList<>();
        for (Mensagem msg : mensagens()) {
            if (msg.maquina() != null && msg.tipoMensagem() == TipoMensagem.POR_PROCESSAR) {
                if (msg.maquina().linhaProducao().sameAs(linha)) {
                    lstMensagens.add(msg);
                }
            }
        }
        return lstMensagens;
    }

    @Override
    public Iterable<Mensagem> findPorProcessarByMaquina(Maquina maquina) {
        List<Mensagem> lstMensagens = new ArrayList<>();
        for (Mensagem msg : mensagens()) {
            if (msg.maquina() != null && msg.tipoMensagem() == TipoMensagem.POR_PROCESSAR) {
                if (msg.maquina().sameAs(maquina)) {
                    lstMensagens.add(msg);
                }
            }
        }
        return lstMensagens;
    }

}
