/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author PC
 */
public class InMemoryMaquinasRepository extends InMemoryDomainRepository<Maquina, CodigoInternoMaquina> implements MaquinasRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<Maquina> findByCodigo(CodigoInternoMaquina who) {
        return match(e -> e.identity().equals(who));
    }

    @Override
    public Iterable<Maquina> findByCodigoComunicacao(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
