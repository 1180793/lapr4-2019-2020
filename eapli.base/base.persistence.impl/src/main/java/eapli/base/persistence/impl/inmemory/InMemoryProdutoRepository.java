/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.inmemory;

import eapli.base.produto.domain.Produto;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.framework.infrastructure.repositories.impl.inmemory.InMemoryDomainRepository;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class InMemoryProdutoRepository extends InMemoryDomainRepository<Produto, CodFabrico> implements ProdutoRepository {

    static {
        InMemoryInitializer.init();
    }

    @Override
    public Iterable<Produto> produtos() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterable<Produto> findProdutoByCodigoFabrico(CodFabrico codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Produto> findProdutosWithoutFichaProducao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
