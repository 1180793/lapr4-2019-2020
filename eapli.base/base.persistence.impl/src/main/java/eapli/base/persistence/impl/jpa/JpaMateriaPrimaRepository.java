/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.persistence.impl.jpa;

import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.domain.MateriaPrima;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class JpaMateriaPrimaRepository extends BaseJpaRepositoryBase<MateriaPrima, Long, CodigoInterno> implements MateriaPrimaRepository {

    public JpaMateriaPrimaRepository(){
        super("codInterno");
    }

    @Override
    public Iterable<MateriaPrima> materiasPrimas() {
        return findAll();
    }

}
