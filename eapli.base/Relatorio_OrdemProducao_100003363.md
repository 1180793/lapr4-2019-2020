## Relatório Ordem de Produção #100003363 ##
*Relatório gerado a: 2020/06/14 22:54:54*
### Linha de Produção + Máquinas Usadas ###

Linha de Produção: LP004

- DD4 -> Transportadora
- T3 -> Transformadora/Cortadora

Linha = DD4 -> T3

### Ficha de Produção ###

ID do Produto: 50000106

Para fabricar 1000.0 UN de Placas de Madeira(50000106) são necessários: 
- 24.4 unidades de Teste Matéria Prima(32000142)

### Resultados ###

#### Produção ####
- 65010.0 unidades de Placas de Madeira(50000106) divididos nos seguintes lotes: 

| Lote | Quantidade |
|:--------  |:-------------- |
| DE06191071 | 7517.0 |
| DE06191072 | 11213.0 |
| DE06191073 | 3949.0 |
| DE06191081 | 3308.0 |
| DE06191082 | 4569.0 |
| DE06191083 | 4604.0 |
| DE06191092 | 3757.0 |
| DE06191093 | 2316.0 |
| DE06191105 | 5239.0 |
| DE06191131 | 2444.0 |
| DE06191132 | 5586.0 |
| DE06191133 | 2470.0 |
| DE06191141 | 3093.0 |
| DE06191142 | 3037.0 |
| DE06191143 | 1908.0 |


#### Consumos ####
- Foram consumidas 1691.0 unidades de Placas de Madeira(50000106) do Depósito L030

#### Estornos ####
- Foram estornadas 99.0 unidades de Restos de Madeira(41000651) para o Depósito L030

#### Entregas de Produção ####

- Foram entregues 41233.0 unidades de  para o Depósito L030
- Foram entregues 23777.0 unidades de  para o Depósito L030

#### Desvios ####
- Foram produzidas mais 10.0 unidades de Placas de Madeira(50000106) do que o previsto para a Ordem de Produção 100003363
- Foram consumidas mais 105.0 unidades de Teste Matéria Prima(32000142) do que o estimado pela Ficha de Produção.

#### Tempos ####
- Início/Fim da Ordem de Produção: 2019/03/26 15:14:46 até 2019/03/26 15:20:57
- Início/Fim Operação T3: 2019/03/26 15:14:46 até 2019/03/26 15:20:42
- Início/Fim Operação DD4: 2019/03/26 15:17:10 até 2019/03/26 15:20:57


- Tempo Bruto Ordem Produção: 6 minutos e 11 segundos
- Tempo Bruto DD4: 3 minutos e 47 segundos
- Tempo Bruto T3: 5 minutos e 56 segundos


- Tempo Efetivo Ordem Produção: 6 minutos e 1 segundos
- Tempo Efetivo DD4: 3 minutos e 37 segundos
- Tempo Efetivo T3: 5 minutos e 26 segundos
