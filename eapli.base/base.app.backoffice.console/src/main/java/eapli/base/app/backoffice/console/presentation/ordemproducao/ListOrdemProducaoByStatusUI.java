/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.ordemproducao;

import eapli.base.ordemproducao.application.ListOrdemProducaoByStatusController;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.util.Collections;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class ListOrdemProducaoByStatusUI extends AbstractUI {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    private final /**
             * Controller instance.
             */
            ListOrdemProducaoByStatusController theController = new ListOrdemProducaoByStatusController();

    @Override
    protected boolean doShow() {

        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.GESTOR_PRODUCAO);

        final Iterable<Status> listStatus = this.theController.status();
        Iterable<OrdemProducao> result = new ArrayList<>();

        final SelectWidget<Status> selector = new SelectWidget<>("Status Ordens Producao :", listStatus,
                new StatusPrinter());
        selector.show();

        final Status theStatus = selector.selectedElement();

        try {
            result = theController.listOrdemProducaoByStatus(theStatus);

            printOrdem(result);
        } catch (Exception e) {
            System.out.println("Nao foram encontradas Ordens de Producao com o status selecionado");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Listar todas as ordens de produção com um certo status.";
    }

    /**
     * Mehod that prints iterable.
     *
     * @param ordens
     */
    private void printOrdem(Iterable<OrdemProducao> ordens) {
        if (Collections.sizeOf(ordens) == 0) {
            System.out.println("Nao foram encontradas ordens de produçao com o status selecionado");
        } else {
            for (OrdemProducao dto : ordens) {
                System.out.println(dto.toString() + "\n");
            }
        }

    }

}
