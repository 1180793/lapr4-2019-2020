/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.deposito;

import eapli.base.deposito.application.EspecificarDepositoController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

/**
 *
 * @author David
 */
public class EspecificarDepositoUI extends AbstractUI {
  
    private final EspecificarDepositoController theController = new EspecificarDepositoController();

    @Override
    protected boolean doShow() {
        final String codigo = Console.readLine("Código Alfanumérico: ");
        final String descricao = Console.readLine("Descrição: ");

        try {
            this.theController.addDeposito(codigo, descricao);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("Um Depósito com esse código já se encontra definido.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Especificar um Novo Depósito.";
    }  
}
