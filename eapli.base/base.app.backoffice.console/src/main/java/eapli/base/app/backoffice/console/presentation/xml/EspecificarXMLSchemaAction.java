/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.xml;

import eapli.framework.actions.Action;

/**
 *
 * @author David
 */
public class EspecificarXMLSchemaAction implements Action {

    EspecificarXMLSchemaUI ui = new EspecificarXMLSchemaUI();
    
    @Override
    public boolean execute() {
        return ui.show();
    }
    
}
