/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.xml;

import eapli.base.xml.application.*;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;

/**
 *
 * @author David
 */
public class EspecificarXMLSchemaUI extends AbstractUI {

    private static final String NAMESPACE_URI = "ChaoFabrica";
    
    private String fileName="defaultName";
    
    private Boolean result=false; 

    ChaoFabricaSchemaGeneratorXSD generator = new ChaoFabricaSchemaGeneratorXSD();
    ChaoFabricaSchemaOutputResolverXSD resolver = new ChaoFabricaSchemaOutputResolverXSD();

    @Override
    protected boolean doShow() {
        try {
             fileName = Console.readLine("Escolha o nome do ficheiro: ");
             
//        if (!fileName.substring(fileName.length()-3).equals(".xsd")){
//            fileName=fileName+".xsd";
//        }
            generator.exportXSD(fileName);
        } catch (JAXBException ex) {
            Logger.getLogger(EspecificarXMLSchemaUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(EspecificarXMLSchemaUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("Verifique se escreveu um nome de ficheiro válido!");
            Logger.getLogger(EspecificarXMLSchemaUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            if (resolver.createOutput(NAMESPACE_URI,fileName) != null) {
                result = true;
            } else {
                result = false;
            }
        } catch (IOException ex) {
            Logger.getLogger(EspecificarXMLSchemaUI.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Verifique se escreveu um nome de ficheiro válido!");
        }
        return result;
    }

    @Override
    public String headline() {
        return "EspecificarXMLSchemaUI";
    }

}
