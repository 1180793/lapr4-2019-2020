/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.produto;

import eapli.base.produto.application.ListProdutosWithoutFichaProducaoController;
import eapli.base.produto.domain.Produto;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author David
 */
public class ListProdutosWithoutFichaProducaoUI extends AbstractListUI<Produto> {
    
    private final ListProdutosWithoutFichaProducaoController theController = new ListProdutosWithoutFichaProducaoController();

    @Override
    protected Iterable<Produto> elements() { 
       return this.theController.listProdutosWithoutFichaProducao();
    }

    @Override
    protected Visitor<Produto> elementPrinter() {
        return new ProdutosWithoutFichaProducaoPrinter();
    }

    @Override
    protected String elementName() {
        return "Produto";
    }

    @Override
    protected String listHeader() {
        return "Produtos sem Ficha de Produção";
    }

    @Override
    protected String emptyMessage() {
        return "Não existem produtos sem ficha de produção.";
    }

    @Override
    public String headline() {
        return "Listar todos os produtos sem Ficha de Produção associada.";
    }

}


