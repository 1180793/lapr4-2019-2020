/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.xml;

import eapli.base.AppSettings;
import eapli.base.xml.application.ToXMLController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.validation.Validator;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

/**
 *
 * @author Goncalo
 */
public class ExportarXMLUI extends AbstractUI {

    private final ToXMLController theController = new ToXMLController();
    private final AppSettings settings = new AppSettings();

    @Override
    protected boolean doShow() {

        String path = Console.readLine("Nome do ficheiro: ");

        try {
            theController.export(path);
        } catch (Exception ex) {
            System.out.println("Ficheiro Inválido ou Inexistente.");
            Logger.getLogger(ExportarXMLUI.class.getName()).log(Level.SEVERE, null, ex);
        }
//        //CAMINHO PARA XML ??
//        File xml = new File(path + ".xml");
//        //CAMINHO PARA XSD
//        File xsd = new File("xsd" + File.separator + "ChaoFabricaDefinition.xsd");
//
//        try {
//            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema schema = factory.newSchema(xsd);
//            Validator validator = schema.newValidator();
//            validator.validate(new StreamSource(xml));
//        } catch (IOException e) {
//            System.out.println("Exception: " + e.getMessage());
//            System.out.println("Failed to validate the created XML file.");
//            return false;
//        } catch (org.xml.sax.SAXException ex) {
//            Logger.getLogger(ExportarXMLUI.class.getName()).log(Level.SEVERE, null, ex);
//            System.out.println("Exception: " + ex.getMessage());
//            System.out.println("Failed to validate the created XML file.");
//            return false;
//        }
        return true;

    }

    @Override
    public String headline() {
        return "Exportar informação do Chão de Fábrica";
    }

}
