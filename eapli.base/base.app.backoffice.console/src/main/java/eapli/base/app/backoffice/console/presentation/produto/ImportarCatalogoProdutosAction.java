/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.produto;

import eapli.framework.actions.Action;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class ImportarCatalogoProdutosAction implements Action {

    @Override
    public boolean execute() {
        return new ImportarCatalogoProdutosUI().show();
    }
    
}
