/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.ordemproducao;

import eapli.base.ordemproducao.domain.Status;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@SuppressWarnings("squid:S106")
public class StatusPrinter implements Visitor<Status> {

    @Override
    public void visit(Status visitee) {
        System.out.println(visitee.name());
    }
}
