package eapli.base.app.backoffice.console.presentation.linhaproducao;

import eapli.base.linhaproducao.application.AlterarEstadoProcessamentoController;
import eapli.framework.io.util.Console;
import eapli.framework.presentation.console.AbstractUI;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AlterarEstadoProcessamentoUI extends AbstractUI {

    private final AlterarEstadoProcessamentoController theController = new AlterarEstadoProcessamentoController();

    @Override
    protected boolean doShow() {
        String codigo = Console.readLine("Identificador da Linha de Produção: ");

        try {
            this.theController.setLinha(codigo);
        } catch (IllegalArgumentException ex) {
            System.out.println("Não foi encontrada nenhuma Linha de Produção com o código '" + codigo + "'.");
            return false;
        }

        if (this.theController.getCurrentProcessamentoState() == true) {
            System.out.println("Estado do Processamento de Mensagens para a Linha de Produção '" + codigo + "': ATIVADO");
            System.out.println("Data do Ultimo Processamento: " + this.theController.getUltimoProcessamento());
            String resposta = Console.readLine("Deseja Desativar o Processamento de Mensagens para esta Linha de Produção? (1 - Sim | 2 - Não)");
            if (resposta.trim().equalsIgnoreCase("1")) {
                this.theController.desativarProcessamento();
                System.out.println("O Processamento de Mensagens para a Linha de Produção '" + codigo + "' foi Desativado");
            }
        } else {
            System.out.println("Estado do Processamento de Mensagens para a Linha de Produção '" + codigo + "': DESATIVADO");
            System.out.println("Data do Ultimo Processamento: " + this.theController.getUltimoProcessamento());
            String resposta = Console.readLine("Deseja Ativar o Processamento de Mensagens para esta Linha de Produção? (1 - Sim | 2 - Não)");
            if (resposta.trim().equalsIgnoreCase("1")) {
                this.theController.ativarProcessamento();
                System.out.println("O Processamento de Mensagens para a Linha de Produção '" + codigo + "' foi Ativado");
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Alterar Estado de Processamento de Mensagens";
    }

}
