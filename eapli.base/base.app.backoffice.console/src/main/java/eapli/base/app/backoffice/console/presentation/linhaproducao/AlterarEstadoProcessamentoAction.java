/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.app.backoffice.console.presentation.linhaproducao;

import eapli.framework.actions.Action;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AlterarEstadoProcessamentoAction implements Action {

    @Override
    public boolean execute() {
        return new AlterarEstadoProcessamentoUI().show();
    }
}