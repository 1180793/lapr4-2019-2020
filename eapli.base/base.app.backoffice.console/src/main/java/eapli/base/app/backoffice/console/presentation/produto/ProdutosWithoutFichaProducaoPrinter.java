/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.produto;

import eapli.base.produto.domain.Produto;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Goncalo
 */
public class ProdutosWithoutFichaProducaoPrinter implements Visitor<Produto> {

    @Override
    public void visit(final Produto visitee) {
        System.out.printf(visitee.toString());
    }
}
