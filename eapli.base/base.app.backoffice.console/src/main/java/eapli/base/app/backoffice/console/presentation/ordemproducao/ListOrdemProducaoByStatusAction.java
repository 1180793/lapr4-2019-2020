/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.ordemproducao;

import eapli.framework.actions.Action;

/**
 *
 * @author David
 */
public class ListOrdemProducaoByStatusAction implements Action {

    ListOrdemProducaoByStatusUI ui = new ListOrdemProducaoByStatusUI();
    
    @Override
    public boolean execute() {
       return ui.show();
    }
    
}
