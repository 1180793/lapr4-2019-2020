package eapli.base.app.backoffice.console.presentation.ordemproducao;

import eapli.base.ordemproducao.application.ListOrdemProducaoByEncomendaController;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.representations.dto.GeneralDTO;
import eapli.framework.io.util.Console;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class ListOrdemProducaoByEncomendaUI extends AbstractUI {

    ListOrdemProducaoByEncomendaController theController = new ListOrdemProducaoByEncomendaController();
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    
    @Override
    protected boolean doShow() {
        
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.GESTOR_PRODUCAO);
        
        List<OrdemProducao> list=new ArrayList<>();
        
        String identificador = Console.readLine("Digite o código da encomenda desejada:\n");

        list = theController.listOrdemProducaoByEncomenda(identificador);


        if (list != null) {
            for(OrdemProducao o : list) {
                System.out.println(o.toString() + "\n");
            }
            return true;
        } else {
            System.out.println("0 resultados. Verifique se digitou a referência corretamente.");
            return false;
        }
    }

    @Override
    public String headline() {
        return "Listar todas as ordens de produção de uma dada encomenda.";
    }

}
