/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.produto;

import eapli.base.produto.application.ImportarCatalogoProdutosController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class ImportarCatalogoProdutosUI extends AbstractUI {

    private final ImportarCatalogoProdutosController theController = new ImportarCatalogoProdutosController();

    @Override
    protected boolean doShow() {
        
        boolean flag = true;
        String csvPath = null;
        while (flag) {
            csvPath = Console.readLine("Caminho para o ficheiro csv: ");
            
            File f = new File(csvPath);
            if (f.exists() && !f.isDirectory()) {
                flag = false;
            } else {
                System.out.println("Ficheiro Inválido ou Inexistente!\n");
            }
        }
        
        String decisao = Console.readLine("Se existir produtos repetidos pretende atualizar ou ignorar ?(a/i)");

        try {
            theController.importarCatalogoProdutos(csvPath, decisao);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("Um produto com esse codigo ja se encontra definido.");
        } catch (IOException ex) {
            Logger.getLogger(ImportarCatalogoProdutosUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    @Override
    public String headline() {
        return "Importar um catalogo de produtos";
    }

}
