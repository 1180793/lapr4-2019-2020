/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.app.backoffice.console.presentation;

import eapli.base.app.common.console.presentation.authz.MyUserMenu;
import eapli.base.Application;
import eapli.base.app.backoffice.console.presentation.authz.AddUserUI;
import eapli.base.app.backoffice.console.presentation.authz.DeactivateUserAction;
import eapli.base.app.backoffice.console.presentation.authz.ListUsersAction;
import eapli.base.app.backoffice.console.presentation.clientuser.AcceptRefuseSignupRequestAction;
import eapli.base.app.backoffice.console.presentation.materiaprima.AdicionarMateriaPrimaAoCatalogoAction;
import eapli.base.app.backoffice.console.presentation.materiaprima.EspecificarCategoriaMateriaPrimaAction;
import eapli.base.app.backoffice.console.presentation.produto.AdicionarProdutoAction;
import eapli.base.app.backoffice.console.presentation.produto.ImportarCatalogoProdutosAction;
import eapli.base.app.backoffice.console.presentation.fichaproducao.AddFichaProducaoAction;
import eapli.base.app.backoffice.console.presentation.linhaproducao.AdicionarMaquinaAction;
import eapli.base.app.backoffice.console.presentation.deposito.EspecificarDepositoAction;
import eapli.base.app.backoffice.console.presentation.linhaproducao.AdicionarFichaConfiguracaoMaquinaAction;
import eapli.base.app.backoffice.console.presentation.linhaproducao.AlterarEstadoProcessamentoAction;
import eapli.base.app.backoffice.console.presentation.linhaproducao.EspecificarLinhaProducaoAction;
import eapli.base.app.backoffice.console.presentation.ordemproducao.ListOrdemProducaoByEncomendaAction;
import eapli.base.app.backoffice.console.presentation.ordemproducao.ListOrdemProducaoByStatusAction;
import eapli.base.app.backoffice.console.presentation.produto.ListProdutosWithoutFichaProducaoAction;
import eapli.base.app.backoffice.console.presentation.xml.ExportarXMLAction;
import eapli.base.app.backoffice.console.presentation.ordemproducao.AdicionarOrdemProducaoAction;
import eapli.base.app.backoffice.console.presentation.ordemproducao.ImportarOrdensProducaoAction;
import eapli.base.app.backoffice.console.presentation.xml.TransformerXSLTAction;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.actions.Actions;
import eapli.framework.actions.menu.Menu;
import eapli.framework.actions.menu.MenuItem;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ExitWithMessageAction;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.menu.HorizontalMenuRenderer;
import eapli.framework.presentation.console.menu.MenuItemRenderer;
import eapli.framework.presentation.console.menu.MenuRenderer;
import eapli.framework.presentation.console.menu.VerticalMenuRenderer;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    private static final String RETURN_LABEL = "Return ";

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int USERS_OPTION = 2;
    private static final int PRODUCAO_OPTION = 3;
    private static final int CHAO_FABRICA_OPTION = 4;

    // USERS
    private static final int ADD_USER_OPTION = 1;
    private static final int LIST_USERS_OPTION = 2;
    private static final int DEACTIVATE_USER_OPTION = 3;
    private static final int ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION = 4;

    // PRODUCAO
    private static final int ADD_MATERIA_PRIMA_OPTION = 1;
    private static final int ADD_CATEGORIA_MATERIA_PRIMA_OPTION = 2;
    private static final int CONSULTAR_PRODUTOS_SEM_FICHA_PRODUCAO_OPTION = 3;
    private static final int ESPECIFICAR_FICHA_PRODUCAO_OPTION = 4;
    private static final int IMPORTAR_CATALOGO_PRODUTOS_OPTION = 5;
    private static final int ADD_PRODUTO_TO_CATALOGO_PRODUTOS_OPTION = 6;
    private static final int EXPORTAR_INFO_CHAO_FABRICA_XML_OPTION = 7;
    private static final int IMPORTAR_ORDENS_PRODUCAO_OPTION = 8;
    private static final int ADD_ORDEM_PRODUCAO_OPTION = 9;
    private static final int CONSULTAR_ORDEM_PRODUCAO_STATUS_OPTION = 10;
    private static final int CONSULTAR_ORDEM_PRODUCAO_ENCOMENDA_OPTION = 11;
    private static final int TRASFORMER_XSLT_OPTION = 12;

    // CHAO FABRICA
    private static final int ADD_MAQUINA_OPTION = 1;
    private static final int ADD_LINHA_PRODUCAO_OPTION = 2;
    private static final int ADD_DEPOSITO_OPTION = 3;
    private static final int ADD_FICHEIRO_CONFIG_MAQUINA_OPTION = 4;
    private static final int ALTERAR_ESTADO_PROCESSAMENTO_MENSAGENS_LINHA_PRODUCAO_OPTION = 5;

    private static final String SEPARATOR_LABEL = "--------------";

    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        } else {
            renderer = new VerticalMenuRenderer(menu, MenuItemRenderer.DEFAULT);
        }
        return renderer.render();
    }

    @Override
    public String headline() {

        return authz.session().map(s -> "Base [ @" + s.authenticatedUser().identity() + " ]")
                .orElse("Base [ ==Anonymous== ]");
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.addSubMenu(MY_USER_OPTION, myUserMenu);

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN)) {
            final Menu usersMenu = buildUsersMenu();
            mainMenu.addSubMenu(USERS_OPTION, usersMenu);
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO)) {
            final Menu producaoMenu = buildProducaoMenu();
            mainMenu.addSubMenu(PRODUCAO_OPTION, producaoMenu);
        }

        if (authz.isAuthenticatedUserAuthorizedTo(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_CHAO_FABRICA)) {
            final Menu chaoFabricaMenu = buildChaoFabricaMenu();
            mainMenu.addSubMenu(CHAO_FABRICA_OPTION, chaoFabricaMenu);
        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.addItem(MenuItem.separator(SEPARATOR_LABEL));
        }

        mainMenu.addItem(EXIT_OPTION, "Exit", new ExitWithMessageAction("Luva di nike"));

        return mainMenu;
    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.addItem(ADD_USER_OPTION, "Add User", new AddUserUI()::show);
        menu.addItem(LIST_USERS_OPTION, "List all Users", new ListUsersAction());
        menu.addItem(DEACTIVATE_USER_OPTION, "Deactivate User", new DeactivateUserAction());
        menu.addItem(ACCEPT_REFUSE_SIGNUP_REQUEST_OPTION, "Accept/Refuse Signup Request",
                new AcceptRefuseSignupRequestAction());
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildProducaoMenu() {
        final Menu menu = new Menu("Producao >");

        // SPRINT B
        menu.addItem(ADD_MATERIA_PRIMA_OPTION, "Adicionar uma Materia-prima ao Catalogo",
                new AdicionarMateriaPrimaAoCatalogoAction());
        menu.addItem(ADD_CATEGORIA_MATERIA_PRIMA_OPTION, "Definir uma nova Categoria de Materias-primas",
                new EspecificarCategoriaMateriaPrimaAction());
        menu.addItem(CONSULTAR_PRODUTOS_SEM_FICHA_PRODUCAO_OPTION, "Consultar Produtos sem Ficha de Producao definida",
                new ListProdutosWithoutFichaProducaoAction());
        menu.addItem(ESPECIFICAR_FICHA_PRODUCAO_OPTION, "Especificar a Ficha de Produção de um Produto",
                new AddFichaProducaoAction());
        menu.addItem(IMPORTAR_CATALOGO_PRODUTOS_OPTION, "Importar o Catalogo de Produtos através de um ficheiro de CSV",
                new ImportarCatalogoProdutosAction());
        menu.addItem(ADD_PRODUTO_TO_CATALOGO_PRODUTOS_OPTION, "Adicionar um novo Produto ao Catalogo de Produtos",
                new AdicionarProdutoAction());

        // SPRINT C
        menu.addItem(EXPORTAR_INFO_CHAO_FABRICA_XML_OPTION, "Exportar, para um ficheiro XML, toda a infomação subjacente ao Chão de Fábrica",
                new ExportarXMLAction());
        menu.addItem(IMPORTAR_ORDENS_PRODUCAO_OPTION, "Importar Ordens de Produção através de um ficheiro CSV",
                new ImportarOrdensProducaoAction());
        menu.addItem(ADD_ORDEM_PRODUCAO_OPTION, "Introduzir uma nova Ordem de Produção",
                new AdicionarOrdemProducaoAction());
        menu.addItem(CONSULTAR_ORDEM_PRODUCAO_STATUS_OPTION, "Consultar as Ordens de Produção que estão num determinado Estado",
                new ListOrdemProducaoByStatusAction());
        menu.addItem(CONSULTAR_ORDEM_PRODUCAO_ENCOMENDA_OPTION, "Consultar as Ordens de Produção de uma dada Encomenda",
                new ListOrdemProducaoByEncomendaAction());
        
        // SPRINT D
        menu.addItem(TRASFORMER_XSLT_OPTION, "Aplicar uma transformação a um ficheiro XML.",
                new TransformerXSLTAction());

        // EXIT
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

    private Menu buildChaoFabricaMenu() {
        final Menu menu = new Menu("Chao de Fabrica >");

        // SPRINT B
        menu.addItem(ADD_MAQUINA_OPTION, "Definir uma nova Maquina",
                new AdicionarMaquinaAction());
        menu.addItem(ADD_LINHA_PRODUCAO_OPTION, "Especificar uma nova Linha de Producao",
                new EspecificarLinhaProducaoAction());
        menu.addItem(ADD_DEPOSITO_OPTION, "Definir um novo Deposito",
                new EspecificarDepositoAction());

        // SPRINT C
        menu.addItem(ADD_FICHEIRO_CONFIG_MAQUINA_OPTION, "Associar um ficheiro de configuração a uma Máquina",
                new AdicionarFichaConfiguracaoMaquinaAction());
        
        // SPRINT D
        menu.addItem(ALTERAR_ESTADO_PROCESSAMENTO_MENSAGENS_LINHA_PRODUCAO_OPTION, "Alterar estado de Processamento de Mensagens de uma Linha de Produção",
                new AlterarEstadoProcessamentoAction());
        
        // EXIT
        menu.addItem(EXIT_OPTION, RETURN_LABEL, Actions.SUCCESS);

        return menu;
    }

}
