/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.produto;

import eapli.base.produto.application.AdicionarProdutoController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

/**
 *
 * @author ZDPessoa
 */
public class AdicionarProdutoUI extends AbstractUI{
    
    private final AdicionarProdutoController theController = new AdicionarProdutoController();

    @Override
    protected boolean doShow() {
        
        String codFabrico = Console.readLine("Código de Fabrico: ");
        String codComercial = Console.readLine("Código Comercial: ");
        String descricaoBreve = Console.readLine("Descrição Breve: ");
        String descricaoCompleta = Console.readLine("Descrição Completa: ");
        String unidade = Console.readLine("Unidade: ");
        String categoriaProduto = Console.readLine("Categoria do Produto: ");
        
        try {
            this.theController.adicionarProduto(codFabrico, codComercial, descricaoBreve, descricaoCompleta, unidade, categoriaProduto);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("Um produto com esse código já se encontra definido.");
        }

        return false;
    }

    @Override
    public String headline() {
        return "Adicionar um novo produto ao catálogo de produtos.";
    }
    
}
