/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.fichaproducao;

import eapli.base.fichaproducao.application.AddFichaProducaoController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;

/**
 *
 * @author PC
 */
public class AddFichaProducaoUI extends AbstractUI {

    private final AddFichaProducaoController theController = new AddFichaProducaoController();

    @Override
    protected boolean doShow() {
        boolean flagProd = true, flagQuantidade = true, flagComponentes = true;
        System.out.println("Digite 0 para cancelar a adição da ficha de produção");
        boolean update = false;
        while (flagProd) {
            String idProdFinal = Console.readLine("ID do Produto Final: ");
            if (idProdFinal.equals("0")) {
                return false;
            }
            boolean isValid = theController.isValidProduto(idProdFinal);
            if (isValid) {
                boolean alreadyHasFichaProducao = theController.hasFichaProducao(idProdFinal);
                if (alreadyHasFichaProducao) {
                    update = Console.readBoolean("Já existe uma Ficha de Produção associada a esse Produto. Pretende atualizar? (Y/N)");
                    if(update == false) {
                        return false;
                    }
                }
                while (flagQuantidade) {
                    try {
                        double quantidadeProdFinal = Console.readDouble("Quantidade de Produto Final: ");
                        if (quantidadeProdFinal <= 0) {
                            throw new IllegalArgumentException();
                        } else {
                            theController.setProdutoFinal(idProdFinal, quantidadeProdFinal);
                            flagQuantidade = false;
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("Quantidade de Produto Inválida!\n");
                    }
                }
                flagProd = false;
            } else {
                System.out.println("ID de Produto Inválido!\n");
            }
        }

        System.out.println("\nDigite 0 para terminar de introduzir Componentes");
        while (flagComponentes) {
            flagQuantidade = true;
            String material = Console.readLine("ID Componente: ");
            if (!material.equals("0")) {
                boolean isValid = theController.isValidMaterial(material);
                if (isValid) {
                    boolean hasRepeatedIdentity = theController.hasRepeatedIdentity(material);
                    int option = 0;
                    if (hasRepeatedIdentity) {
                        while (option != 1 && option != 2) {
                            System.out.println("Existe um Produto e uma Matéria-Prima com o mesmo código.");
                            option = Console.readInteger("Qual pretende introduzir? 1 - Matéria-Prima; 2 - Produto");
                            if (option != 1 && option != 2) {
                                System.out.println("Opção Inválida. Introduza '1' para Matéria-Prima ou '2' para Produto");
                            }
                        }
                    }
                    while (flagQuantidade) {
                        try {
                            double quantidadeComponente = Console.readDouble("Quantidade de Componente: ");
                            if (quantidadeComponente <= 0) {
                                throw new IllegalArgumentException();
                            } else {
                                if (hasRepeatedIdentity) {
                                    theController.addComponente(material, quantidadeComponente, option);
                                } else {
                                    theController.addComponente(material, quantidadeComponente);
                                }
                                flagQuantidade = false;
                            }
                        } catch (IllegalArgumentException e) {
                            System.out.println("Quantidade de Componente Inválido\n");
                        }
                    }
                } else {
                    Console.readLine("ID de Componente Inválido\n");
                }
            } else {
                flagComponentes = false;
            }
        }
        try {
            if (update) {
                this.theController.removePreviousFichaProducao();
            }
            this.theController.addFichaProducao();
        } catch (@SuppressWarnings("unused") final IntegrityViolationException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String headline() {
        return "Adicionar Ficha de Produção";
    }
}
