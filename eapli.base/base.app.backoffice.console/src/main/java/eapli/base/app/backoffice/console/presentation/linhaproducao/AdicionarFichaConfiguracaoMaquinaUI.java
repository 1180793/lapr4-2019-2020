/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.linhaproducao;

import eapli.base.linhaproducao.application.AdicionarFichaConfiguracaoMaquinaController;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

/**
 *
 * @author PC
 */
public class AdicionarFichaConfiguracaoMaquinaUI extends AbstractUI {

    private final AdicionarFichaConfiguracaoMaquinaController controller = new AdicionarFichaConfiguracaoMaquinaController();
    
    @Override
    protected boolean doShow() {
   
            boolean flagMaq = false;
            String maqID = null, filePath;
            
            while(!flagMaq){
                maqID = Console.readLine("Insira o ID da Máquina: ");
                if(maqID.equals("0")){
                    return false;
                }
                flagMaq = this.controller.validarMaqId(maqID);
                if(!flagMaq){
                    System.out.println("ID inválido");
                }
            } 
            
            filePath = Console.readLine("Introduza o path do ficheiro");

            return this.controller.adicionarFichaConfig(filePath, maqID);
            
    }

    @Override
    public String headline() {
        return "Adicionar Ficha de Configuração de uma Máquina";
    }
    
}
