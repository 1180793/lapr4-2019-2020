/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.xml;

import eapli.base.xml.application.TransformerXSLTController;
import eapli.framework.io.util.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZDPessoa
 */
public class TransformerXSLTUI {
    
    private final TransformerXSLTController theController = new TransformerXSLTController();
    private final static String HTML_OPTION = "html";
    private final static String HTML_OPTION_NUM = "1";
    private final static String JSON_OPTION = "json";
    private final static String JSON_OPTION_NUM = "2";
    private final static String TXT_OPTION = "txt";
    private final static String TXT_OPTION_NUM = "3";
    private final static String XML_OPTION = "xml";
    private final static String XSLT_HTML = "XSLTHTML.xsl";
    private final static String XSLT_JSON = "XSLTJSON.xsl";
    private final static String XSLT_TXT = "XSLTTXT.xsl";
    private final static String XSLFILE_PATH = "xslt\\xsl\\";
    private final static String OUTFILE_PATH = "xslt";
    private final static String INFILE_PATH = "xslt\\xml\\";

    protected boolean doShow() {
        
        String inFilename = "";
        String xslFilename = "";
        String outFilename = "";

        while (true) {
            System.out.println("Opções de trasformação: \n1 - HTML\n2 - JSON\n3 - TXT");
            String option = Console.readLine("\nFormato: ");
            
            System.out.println("\nXml Files available:");
            List<String> xmlFiles = getXmlFiles();
            for(int i = 0; i < xmlFiles.size(); i++){
                System.out.println(i+1 + " - " + xmlFiles.get(i));
            }
            String optionXml = Console.readLine("\nFicheiro XML: ");
            inFilename = xmlFiles.get(Integer.parseInt(optionXml)-1);

            if (option.equalsIgnoreCase(HTML_OPTION) || option.equalsIgnoreCase(HTML_OPTION_NUM)) {

                outFilename = Console.readLine("\nNome do ficheiro HTML: ");

                outFilename = outFilename.concat("." + HTML_OPTION);

                xslFilename = XSLT_HTML;

                break;

            } else if (option.equalsIgnoreCase(JSON_OPTION) || option.equalsIgnoreCase(JSON_OPTION_NUM)) {

                outFilename = Console.readLine("\nNome do ficheiro JSON: ");

                outFilename = outFilename.concat("." + JSON_OPTION);

                xslFilename = XSLT_JSON;

                break;

            } else if (option.equalsIgnoreCase(TXT_OPTION) || option.equalsIgnoreCase(TXT_OPTION_NUM)) {

                outFilename = Console.readLine("\nNome do ficheiro TXT: ");

                outFilename = outFilename.concat("." + TXT_OPTION);

                xslFilename = XSLT_TXT;

                break;

            } else {
                System.out.println("Opção inválida...");
            }
        }

        xslFilename = XSLFILE_PATH.concat(xslFilename);
        outFilename = OUTFILE_PATH.concat(File.separator).concat(outFilename);
        inFilename = INFILE_PATH.concat(inFilename);

        try {
            this.theController.applyXSLT(inFilename, outFilename, xslFilename);
        } catch (@SuppressWarnings("unused") final Exception e) {
            System.out.println("Erro ao aplicar o XSLT...");
        }

        return false;
    }

    public String headline() {
        return "Aplicar uma transformação a um ficheiro XML.";
    }
    
    /**
     * Gets all the XML files from the designated directory
     * @return 
     */
    private List<String> getXmlFiles() {
        
        List<String> fileNameList = new ArrayList<>();
        
        File folder = new File(OUTFILE_PATH.concat(File.separator).concat(XML_OPTION));
        File[] listOfFiles = folder.listFiles();

        for( File f : listOfFiles){
            if(f.isFile() && f.getName().contains(".".concat(XML_OPTION)))
                fileNameList.add(f.getName());
        }
        
        return fileNameList;
    }

}
