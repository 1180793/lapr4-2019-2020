/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.linhaproducao;

import eapli.base.linhaproducao.application.AdicionarMaquinaController;
import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

/**
 *
 * @author PC
 */
public class AdicionarMaquinaUI extends AbstractUI{
    private final AdicionarMaquinaController theController = new AdicionarMaquinaController();

    protected AdicionarMaquinaController controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String idLinha = Console.readLine("ID da Linha de Produção: ");
        final String codInterno =Console.readLine("Código Interno: ");
        final String numSerie = Console.readLine("Número de Série: ");
        final String descricao = Console.readLine("Descrição da Máquina: ");
        final String strDate = Console.readLine("Data da Instalação(yyyy-mm-dd): ");
        final String marca = Console.readLine("Marca: ");
        final String modelo = Console.readLine("Modelo: ");
        final Integer posicao = Console.readInteger("Posição(na Linha de Produção): ");
        
        try {
            this.theController.adicionarMaquina(codInterno, numSerie, descricao, strDate, marca, modelo, idLinha, posicao);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("That acronym is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Adicionar Máquina";
    }
}
