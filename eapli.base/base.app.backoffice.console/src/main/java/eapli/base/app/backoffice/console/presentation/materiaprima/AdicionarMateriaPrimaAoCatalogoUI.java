/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.materiaprima;

import eapli.base.materiaprima.application.AdicionarMateriaPrimaController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.util.Console;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@SuppressWarnings("java:S106")
public class AdicionarMateriaPrimaAoCatalogoUI extends AbstractUI {

    private final AdicionarMateriaPrimaController theController = new AdicionarMateriaPrimaController();

    @Override
    protected boolean doShow() {

        final String codigoInterno = Console.readLine("Codigo Interno: ");
        final String descricao = Console.readLine("Descrição: ");
        final String codAlfaNumerico = Console.readLine("Categoria(codigo alfanumerico): ");// fazer verificaçao se a categoria inserida ja existe
        final String fichaTecnica = Console.readLine("Ficha Tecnica(path): ");

        try {
            this.theController.adicionarMateriaPrima(codigoInterno, descricao, fichaTecnica, codAlfaNumerico);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException | IllegalArgumentException ex) {
            if (ex instanceof IntegrityViolationException) {
                System.out.println("Uma materia prima com esse codigo ja se encontra definida.");
            } else if (ex instanceof IllegalArgumentException) {
                System.out.println("Nao existe nenhuma categoria com esse código");
            }
        }

        return false;
    }

    @Override
    public String headline() {
        return "Adicionar uma nova Matéria-Prima ao catalogo";
    }

}
