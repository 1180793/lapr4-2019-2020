/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.app.backoffice.console.presentation.linhaproducao;

import eapli.base.linhaproducao.application.EspecificarLinhaProducaoController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;

/**
 *
 * @author ZDPessoa
 */
public class EspecificarLinhaProducaoUI extends AbstractUI {
    
    private final EspecificarLinhaProducaoController theController = new EspecificarLinhaProducaoController();
    
    @Override
    protected boolean doShow() {
        String codigo = Console.readLine("Identificador: ");
        
        try {
            this.theController.adicionarLinhaProducao(codigo);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("Uma linha de produção com esse identificador já se encontra definida.");
            e.printStackTrace();
        }

        return false;
    }
    
    @Override
    public String headline() {
        return "Especificar uma nova linha de produção.";
    }
    
}
