/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.app.backoffice.console.presentation.ordemproducao;

import eapli.base.ordemproducao.application.AddOrdemProducaoController;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.io.util.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AdicionarOrdemProducaoUI extends AbstractUI {

    private final AddOrdemProducaoController theController = new AddOrdemProducaoController();

    @Override
    protected boolean doShow() {

        boolean dataEmissaoFlag = true, dataPrevistaFlag = true, flagProd = true, qtddFlag = true, encomendasFlag = true;
        String codInterno, dataPrevistaExecucao = null, codigoFabricoProduto = null;
        Double quantidadeProduto = null;

        codInterno = Console.readLine("Código Interno: ");

        while (dataPrevistaFlag) {
            dataPrevistaExecucao = Console.readLine("Data Prevista de Execução(dd/MM/yyyy): ");
            try {
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                sdf1.parse(dataPrevistaExecucao);
                dataPrevistaFlag = false;
            } catch (ParseException ex) {
                System.out.println("Data Prevista de Execução Inválida!\n");
            }
        }

        while (flagProd) {
            codigoFabricoProduto = Console.readLine("ID do Produto Final: ");
            boolean isValid = theController.isValidProduto(codigoFabricoProduto);
            if (isValid) {
                flagProd = false;
            } else {
                System.out.println("Não foi encontrado um Produto com o Código de Fabrico introduzido.\n");
            }
        }

        while (qtddFlag) {
            quantidadeProduto = Double.parseDouble(Console.readLine("Quantidade do Produto: "));
            if (quantidadeProduto > 0) {
                qtddFlag = false;
            } else {
                System.out.println("Quantidade de Produto Inválida.\n");
            }
        }

        String unidade = Console.readLine("Unidade: ");

        System.out.println("Encomendas associadas à Ordem de Produção (digite '0' para terminar): ");
        Set<Encomenda> setEncomendas = new HashSet<>();
        int nEncomenda = 1;
        while (encomendasFlag) {
            String encomenda = Console.readLine("ID da Encomenda(#" + nEncomenda + "): ");
            if (encomenda.equals("0")) {
                if (setEncomendas.isEmpty()) {
                    System.out.println("É necessário haver pelo menos uma Encomenda associada a uma Ordem de Produção!\n");
                } else {
                    encomendasFlag = false;
                }
            } else {
                setEncomendas.add(Encomenda.valueOf(encomenda));
                nEncomenda++;
            }
        }
        try {
            theController.adicionarOrdemProducao(codInterno, setEncomendas, dataPrevistaExecucao, quantidadeProduto, unidade, codigoFabricoProduto);
        } catch (@SuppressWarnings("unused") final IntegrityViolationException e) {
            System.out.println("Uma ordem de producao com esse codigo ja se encontra definido.");
        } catch (ParseException ex) {
            Logger.getLogger(AdicionarOrdemProducaoUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(AdicionarOrdemProducaoUI.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }

        return false;
    }

    @Override
    public String headline() {
        return "Adicionar Ordem de Producao";
    }

}
