/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.application;

import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.representations.dto.GeneralDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class ListOrdemProducaoDTOService implements DTOable {

    public Iterable<GeneralDTO> iterableOrdemProducaoToDTO(Iterable<OrdemProducao> ordens) {
        return GeneralDTO.ofMany(ordens);
    }

    public String dtoToString(GeneralDTO dto) {
        return dto.toString();
    }

    public Iterable<String> iterableDtoToString(Iterable<GeneralDTO> dto) {
        List<String> result = new ArrayList<>();
        for (GeneralDTO d : dto) {
            result.add(d.toString());
        }

        return result;
    }

    @Override
    public Object toDTO() {
        return GeneralDTO.of(this);
    }

}
