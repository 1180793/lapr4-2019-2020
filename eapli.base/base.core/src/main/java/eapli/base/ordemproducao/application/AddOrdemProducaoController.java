/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Quantidade;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.base.utils.Utils;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;
import javax.persistence.PersistenceException;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@UseCaseController
public class AddOrdemProducaoController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final OrdemProducaoRepository repository = PersistenceContext.repositories().ordensProducao();
    private final ProdutoRepository prodRepo = PersistenceContext.repositories().produtos();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public OrdemProducao adicionarOrdemProducao(String codInterno, Set<Encomenda> encomenda, String dataEmissao, String dataPrevista, Double quantidadeProduto, String unidade, String produto) throws ParseException {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        
        if (!isValidProduto(produto)) {
            throw new IllegalArgumentException("Não foi possivel encontrar um Produto com esse Código de Fabrico!\n");
        }
        
        CodInternoOrdemDeProducao codInternoOrdem = CodInternoOrdemDeProducao.valueOf(codInterno);
        
        Calendar dataEmissaoOrdem = Calendar.getInstance();
        dataEmissaoOrdem.setTime(sdf.parse(dataEmissao));

        Calendar dataPrevistaExecucaoOrdem = Calendar.getInstance();
        dataPrevistaExecucaoOrdem.setTime(sdf.parse(dataPrevista));

        CodFabrico codigoFabricoProduto = CodFabrico.valueOf(produto);

        Quantidade quantidadeProdutoOrdem = Quantidade.valueOf(quantidadeProduto);
        Designation unidadeOrdem = Designation.valueOf(unidade);

        Produto produtoOrdem = prodRepo.findProdutoByCodigoFabrico(codigoFabricoProduto).iterator().next();
        
        try {
            OrdemProducao ordem = new OrdemProducao(codInternoOrdem, encomenda, Status.PENDENTE, dataEmissaoOrdem, dataPrevistaExecucaoOrdem, quantidadeProdutoOrdem, unidadeOrdem, produtoOrdem);
            return this.repository.save(ordem);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }
    }
    
    public OrdemProducao adicionarOrdemProducao(String codInterno, Set<Encomenda> encomenda, String dataPrevista, Double quantidadeProduto, String unidade, String produto) throws ParseException {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        
        String dataEmissao = sdf.format(Calendar.getInstance().getTime());
        
        return this.adicionarOrdemProducao(codInterno, encomenda, dataEmissao, dataPrevista, quantidadeProduto, unidade, produto);
        
    }

    public OrdemProducao adicionarOrdemProducao(OrdemProducao ordemProducao) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        return this.repository.save(ordemProducao);
    }

    public void readOrdensProducaoCsv(String filePath) throws IOException {
        Utils.readOrdemProducaoCsv(filePath);
    }
    
    public boolean isValidProduto(String id) {
        return getProduto(id) != null;
    }
    
    private Produto getProduto(String id) {
        if (this.prodRepo.containsOfIdentity(CodFabrico.valueOf(id))) {
            return this.prodRepo.ofIdentity(CodFabrico.valueOf(id)).get();
        }
        return null;
    }

}
