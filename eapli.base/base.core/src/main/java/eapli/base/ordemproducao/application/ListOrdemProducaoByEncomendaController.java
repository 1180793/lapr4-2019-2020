/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.representations.dto.GeneralDTO;
import java.util.List;

/**
 *
 * @author David
 */
public class ListOrdemProducaoByEncomendaController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final OrdemProducaoRepository repositoryOrdemProducao = PersistenceContext.repositories().ordensProducao();
    private final ListOrdemProducaoDTOService dto = new ListOrdemProducaoDTOService();

    public List<OrdemProducao> listOrdemProducaoByEncomenda(String order) {

        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        
        Encomenda encomenda = new Encomenda(order);

        Iterable<OrdemProducao> queryResult;

        queryResult = repositoryOrdemProducao.findByEncomenda(encomenda);

//        Iterable<GeneralDTO> dtoResult = dto.iterableOrdemProducaoToDTO(queryResult);
        
        return (List<OrdemProducao>) queryResult;
    }
}
