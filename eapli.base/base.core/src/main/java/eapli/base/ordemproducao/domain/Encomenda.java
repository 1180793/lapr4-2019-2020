/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.util.StringPredicates;
import java.util.Objects;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author David
 */
@Embeddable
public class Encomenda implements ValueObject {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    private String identificador;

    protected Encomenda() {
    }

    /**
     * Construtor de encomenda
     *
     * @param identificador
     */
    public Encomenda(String identificador) {
        setIdentificador(identificador);

    }

    /**
     * Metodo para atribuir um identificador a encomenda
     *
     * @param identificador
     */
    private void setIdentificador(String identificador) {
        if (StringPredicates.isNullOrEmpty(identificador)) {
            throw new IllegalArgumentException(
                    "Identificador da encomenda nao deve ser nulo nem vazio");
        }
        this.identificador = identificador;
    }
    
    public static Encomenda valueOf(final String identificador) {
        return new Encomenda(identificador);
    }

    @Override
    public int hashCode() {
        return this.identificador.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Encomenda other = (Encomenda) obj;
        return Objects.equals(this.identificador, other.identificador);
    }

    @Override
    public String toString() {
        return String.format("Encomenda: %s", this.identificador);
    }

}
