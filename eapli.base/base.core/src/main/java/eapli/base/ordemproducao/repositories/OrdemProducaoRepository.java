/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.repositories;

import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author David
 */
public interface OrdemProducaoRepository extends DomainRepository<CodInternoOrdemDeProducao, OrdemProducao> {
     /**
     * Returns the active ordens de producao.
     *
     * @return An iterable for Linha Producao.
     */
    Iterable<OrdemProducao> ordensProducao();
    
    Iterable<OrdemProducao> findByCodigo(final CodInternoOrdemDeProducao who);
    
    Iterable<OrdemProducao> findByStatus(final Status status);
    
    Iterable<OrdemProducao> findByEncomenda(final Encomenda order);
}
