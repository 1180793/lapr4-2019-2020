/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.domain;

/**
 * Enum to save different status values for Ordem de Prodcao.
 *
 * @author David
 */
public enum Status {
    EM_EXECUCAO, EXECUCAO_PARADA_TEMP, CONCLUIDA, SUSPENSA, PENDENTE;
}


