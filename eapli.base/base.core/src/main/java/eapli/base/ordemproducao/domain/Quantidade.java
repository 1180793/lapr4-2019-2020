/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.domain;

import eapli.framework.domain.model.ValueObject;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
public class Quantidade implements ValueObject {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    private Double quantidadeProduto;

    protected Quantidade() {
    }

    public Quantidade(Double quantidadeProduto) {
        setQuantidade(quantidadeProduto);

    }

    private void setQuantidade(Double quantidadeProduto) {
        if (quantidadeProduto <= 0) {
            throw new IllegalArgumentException(
                    "Identificador da encomenda nao deve ser nulo nem vazio");
        }
        this.quantidadeProduto = quantidadeProduto;
    }

    public static Quantidade valueOf(final Double quantidadeProduto) {
        return new Quantidade(quantidadeProduto);
    }

    @Override
    public int hashCode() {
        return this.quantidadeProduto.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Quantidade other = (Quantidade) obj;
        return Objects.equals(this.quantidadeProduto, other.quantidadeProduto);
    }

    @Override
    public String toString() {
        return String.format("Quantidade de produto: %s", this.quantidadeProduto);
    }
    
    public Double quantidadeProduto() {
        return this.quantidadeProduto;
    }

}
