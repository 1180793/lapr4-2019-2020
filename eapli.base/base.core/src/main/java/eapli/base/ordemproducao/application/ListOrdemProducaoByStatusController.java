/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import eapli.framework.representations.dto.GeneralDTO;
import java.util.Arrays;

/**
 *
 * @author David
 */
public class ListOrdemProducaoByStatusController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final OrdemProducaoRepository repositoryOrdemProducao = PersistenceContext.repositories().ordensProducao();

    public Iterable<OrdemProducao> listOrdemProducaoByStatus(Status status) {

        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        Iterable<OrdemProducao> queryResult;

        queryResult = repositoryOrdemProducao.findByStatus(status);

        return queryResult;
    }

    public Iterable<Status> status() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        return Arrays.asList(Status.values());
    }

    public Iterable<GeneralDTO> statusDTO() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        return GeneralDTO.ofMany(status());
    }
}
