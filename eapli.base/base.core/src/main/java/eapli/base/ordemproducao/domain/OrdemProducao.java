/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.ordemproducao.domain;

import eapli.base.produto.domain.Produto;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.general.domain.model.Designation;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
@Entity
@Table(
        name = "OrdensProducao",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "code")
        }
)
@XmlRootElement
public class OrdemProducao implements AggregateRoot<CodInternoOrdemDeProducao> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @XmlTransient
    private Long id;

    @Version
    @XmlTransient
    private Long version;

    @Column(unique = true, nullable = false)
    @XmlElement
    private CodInternoOrdemDeProducao code;

    @ElementCollection
    @XmlElementWrapper
    private Set<Encomenda> encomenda;

    @XmlElement
    private Status status;

    @XmlElement
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataEmissao;
    
    @XmlElement
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataPrevista;

    @XmlElement
    private Quantidade quantidadeProduto;
    
    @AttributeOverride(name = "name", column = @Column(name = "Unidade"))
    @XmlElement
    private Designation unidade;

    @ManyToOne
    @XmlIDREF
    private Produto produto;

    /**
     * Contrutor completo.
     *
     * @param code
     * @param encomenda
     * @param status
     * @param qtProduto
     * @param dataEmissao
     * @param dataPrevista
     * @param unidade
     * @param produto
     */
    public OrdemProducao(CodInternoOrdemDeProducao code, Set<Encomenda> encomenda, Status status, Calendar dataEmissao, Calendar dataPrevista, Quantidade qtProduto, Designation unidade, Produto produto) {
        this.code = code;
        this.encomenda = encomenda;
        this.status = status;
        this.dataEmissao = dataEmissao;
        this.dataPrevista = dataPrevista;
        this.quantidadeProduto = qtProduto;
        this.unidade = unidade;
        this.produto = produto;
    }

    /**
     * Contrutor vazio(ORM).
     */
    protected OrdemProducao() {
        this.id = null;
        this.version = null;
        this.code = null;
        this.encomenda = null;
        this.status = null;
        this.dataEmissao = null;
        this.dataPrevista = null;
        this.quantidadeProduto = null;
        this.unidade = null;
        this.produto = null;
    }

    /**
     * Método sameAs reescrito. Devolve true se dois objetos de OrdemDeProduçao
     * forem iguais.
     *
     * @param other
     * @return
     */
    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof OrdemProducao)) {
            return false;
        }
        final OrdemProducao that = (OrdemProducao) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    /**
     * Métoddo identity reescrito. Devolve o codInterno que identifica uma
     * instância de OrdemDeProducao.
     *
     * @return
     */
    @Override
    public CodInternoOrdemDeProducao identity() {
        return code;
    }

    public Status status() {
        return status;
    }

    /**
     * Método que devolve o set de encomendas associado a uma ordem de produçao
     *
     * @return Set<Encomenda>
     */
    public Set<Encomenda> encomenda() {
        return encomenda;
    }

    @Override
    public String toString() {
        return String.format("Ordem producao nº %s , status: %s", this.code, this.status);
    }
    
    public List<Calendar> getDatas(){
        List<Calendar> datasOrdem = new ArrayList<>();
        datasOrdem.add(dataEmissao);
        datasOrdem.add(dataPrevista);
        return datasOrdem;
    }
    
    public Produto produto() {
        return this.produto;
    }
    
    public Quantidade quantidade() {
        return this.quantidadeProduto;
    }
    
    public void atualizarStatus(Status status) {
        this.status = status;
    }
}
