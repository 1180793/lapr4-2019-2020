/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import javax.persistence.PersistenceException;

/**
 *
 * @author ZDPessoa
 */
@UseCaseController
public class EspecificarLinhaProducaoController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final LinhasProducaoRepository repository = PersistenceContext.repositories().linhasProducao();

    public LinhaProducao adicionarLinhaProducao(String codigo) {
        final Identificador identificador = Identificador.valueOf(codigo);
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_CHAO_FABRICA);

        final LinhaProducao newLinhaProducao = new LinhaProducao(identificador);

        try {
            return this.repository.save(newLinhaProducao);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }
    }

}
