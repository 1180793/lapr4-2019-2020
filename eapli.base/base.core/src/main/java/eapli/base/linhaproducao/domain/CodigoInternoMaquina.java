/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author ZDPessoa
 */
@Embeddable
public class CodigoInternoMaquina implements ValueObject, Comparable<CodigoInternoMaquina>{
    
    private static final long serialVersionUID = 1L;

    @XmlAttribute
    String codInterno;

    protected CodigoInternoMaquina() {
        
    }

    public CodigoInternoMaquina(final String codInterno) {
        if (StringPredicates.isNullOrEmpty(codInterno)) {
            throw new IllegalArgumentException(
                    "Codigo Interno nao deve ser nulo nem vazio");
        }

        this.codInterno = codInterno;
    }

    public static CodigoInternoMaquina valueOf(final String codigoInterno) {
        return new CodigoInternoMaquina(codigoInterno);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodigoInternoMaquina)) {
            return false;
        }

        final CodigoInternoMaquina that = (CodigoInternoMaquina) o;
        return this.codInterno.equals(that.codInterno);
    }

    @Override
    public int hashCode() {
        return this.codInterno.hashCode();
    }

    @Override
    public String toString() {
        return this.codInterno;
    }

    @Override
    public int compareTo(CodigoInternoMaquina o) {
        return codInterno.compareTo(o.codInterno);
    }
    
}
