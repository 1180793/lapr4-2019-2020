/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.repositories;

import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author PC
 */
public interface MaquinasRepository extends DomainRepository<CodigoInternoMaquina, Maquina> {

    Iterable<Maquina> findByCodigo(final CodigoInternoMaquina who);

    Iterable<Maquina> findByCodigoComunicacao(final Long id);
}
