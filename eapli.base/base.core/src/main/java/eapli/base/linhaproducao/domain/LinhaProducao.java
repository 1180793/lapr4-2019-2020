/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ZDPessoa
 */
@Entity
@XmlRootElement
public class LinhaProducao implements AggregateRoot<Identificador> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @XmlTransient
    private Long id;

    @Version
    @XmlTransient
    private Long version;

    @Column(unique = true, nullable = false)
    @XmlElement
    Identificador identificador;

    @OneToMany(mappedBy = "linhaProducao", targetEntity = Maquina.class, cascade = CascadeType.ALL)
    @XmlElement
    Set<Maquina> maquinas;

    Boolean processamentoMensagens;

    @Temporal(TemporalType.TIMESTAMP)
    @XmlTransient
    Date ultimoProcessamento;

    public LinhaProducao(Identificador identificador) {
        Preconditions.noneNull(identificador);
        this.identificador = identificador;
        this.maquinas = new LinkedHashSet<>();
        this.processamentoMensagens = true;
        this.ultimoProcessamento = null;
    }

    protected LinhaProducao() {
        // ORM
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof LinhaProducao)) {
            return false;
        }
        final LinhaProducao that = (LinhaProducao) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    public Identificador identificador() {
        return identity();
    }

    public Set<Maquina> maquinas() {
        return maquinas;
    }

    public boolean hasProcessamentoMensagensAtivado() {
        return this.processamentoMensagens;
    }
    
    public void ativarProcessamentoMensagens() {
        this.processamentoMensagens = true;
    }
    
    public void desativarProcessamentoMensagens() {
        this.processamentoMensagens = false;
    }
    
    public Date ultimoProcessamento() {
        return this.ultimoProcessamento;
    }
    
    public void processarMensagens(Date date) {
        this.ultimoProcessamento = date;
    }
    
    @Override
    public Identificador identity() {
        return identificador;
    }
}
