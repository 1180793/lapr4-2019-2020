/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.repositories;

import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author David
 */
public interface LinhasProducaoRepository extends DomainRepository<Identificador, LinhaProducao> {
     /**
     * Returns the active categorias de materia prima.
     *
     * @return An iterable for Linha Producao.
     */
    Iterable<LinhaProducao> linhasProducao();
    
    Iterable<LinhaProducao> findByCodigo(final Identificador who);
    
}
