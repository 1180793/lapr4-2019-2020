/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.linhaproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@UseCaseController
public class AlterarEstadoProcessamentoController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final LinhasProducaoRepository repository = PersistenceContext.repositories().linhasProducao();
    private LinhaProducao linha = null;
    
    public void setLinha(String identificador) {
        if (repository.containsOfIdentity(Identificador.valueOf(identificador))) {
            this.linha = repository.ofIdentity(Identificador.valueOf(identificador)).get();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public boolean getCurrentProcessamentoState() {
        if (this.linha != null) {
            return this.linha.hasProcessamentoMensagensAtivado();
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    public String getUltimoProcessamento() {
        Date date = this.linha.ultimoProcessamento();
        if (date == null) {
            return "Nunca";
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return sdf.format(date);
        }
    }

    public void ativarProcessamento() {
        if (this.linha != null) {
            this.linha.ativarProcessamentoMensagens();
            this.saveChanges();
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void desativarProcessamento() {
        if (this.linha != null) {
            this.linha.desativarProcessamentoMensagens();
            this.saveChanges();
        } else {
            throw new IllegalArgumentException();
        }
    }
    
    private void saveChanges() {
        if (this.linha != null) {
            this.repository.save(this.linha);
        }
    }
}
