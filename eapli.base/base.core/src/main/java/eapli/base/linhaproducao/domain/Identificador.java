/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author ZDPessoa
 */
@Embeddable
public class Identificador implements ValueObject, Comparable<Identificador> {

    private static final long serialVersionUID = 1L;
    
    @XmlAttribute
    String identificador;

    protected Identificador() {

    }

    private Identificador(final String identificador) {
        Preconditions.nonEmpty(identificador, "Identificador não pode ser nulo ou vazio.");
        Preconditions.ensure(StringPredicates.isSingleWord(identificador), "Identificador não pode conter espaços");

        this.identificador = identificador;
    }

    public static Identificador valueOf(final String identificador) {
        return new Identificador(identificador);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Identificador)) {
            return false;
        }

        final Identificador that = (Identificador) o;
        return this.identificador.equals(that.identificador);
    }

    @Override
    public int hashCode() {
        return this.identificador.hashCode();
    }

    @Override
    public String toString() {
        return this.identificador;
    }

    @Override
    public int compareTo(Identificador o) {
        return identificador.compareTo(o.identificador);
    }

}
