/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Identificador;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.PersistenceException;

/**
 *
 * @author PC
 */
public class AdicionarMaquinaController {

    private final AuthorizationService authorizationService = AuthzRegistry.authorizationService();
    private final MaquinasRepository maqRepo = PersistenceContext.repositories().maquinas();
    private final LinhasProducaoRepository lpRepo = PersistenceContext.repositories().linhasProducao();

    public Maquina adicionarMaquina(String codigoInterno, String numSerie, String descricao, String strDate, String Marca, String Modelo, String lpId, Integer pos) {
        authorizationService.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.GESTOR_CHAO_FABRICA);

        final CodigoInternoMaquina codInterno = CodigoInternoMaquina.valueOf(codigoInterno);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar dataInstalacao = Calendar.getInstance();
        try {
            dataInstalacao.setTime(sdf.parse(strDate));
        } catch (ParseException ex) {
            Logger.getLogger(AdicionarMaquinaController.class.getName()).log(Level.SEVERE, null, ex);
        }

        final Maquina newMaq = new Maquina(codInterno, numSerie, descricao, dataInstalacao, Marca, Modelo);
        newMaq.definirPosicao(pos);
        LinhaProducao lp = this.lpRepo.ofIdentity(Identificador.valueOf(lpId)).get();
        if (lp == null) {
            System.out.println("Linha de Produção inexistente.");
            return null;
        } else {
            newMaq.definirLinhaProducao(lp);
            try {
                this.maqRepo.save(newMaq);
                this.lpRepo.save(lp);
            } catch (PersistenceException e) {
                System.out.println("\n\n\n\nSTACK: \n");
                e.printStackTrace();
                System.out.println("\nFIM STACK: \n\n\n");
                throw new IntegrityViolationException();
            } catch (Exception e) {
                System.out.println("\n\n\n\nSTACK: \n");
                e.printStackTrace();
                System.out.println("\nFIM STACK: \n\n\n");
                e.printStackTrace();
            }
        return newMaq;
    }
}

}
