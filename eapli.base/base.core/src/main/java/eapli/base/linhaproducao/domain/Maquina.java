/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ZDPessoa
 */
@Entity
@Table(
        name = "Maquina",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "codInterno")
        }
)
@XmlRootElement
public class Maquina implements AggregateRoot<CodigoInternoMaquina> {

    @Id
    @GeneratedValue
    @XmlTransient
    private Long id;
    @Version
    @XmlTransient
    private Long version;

    @Column(unique = true, nullable = false)
    @XmlElement
    CodigoInternoMaquina codInterno;
    
    @Column(unique = true, nullable = false)
    @XmlElement
    String numSerie;
    
    @XmlElement
    String descricao;

    @Temporal(TemporalType.DATE)
    @XmlElement
    private Calendar dataInstalacao;

    @XmlElement
    String marca;

    @XmlElement
    String modelo;

    @ManyToOne(targetEntity = LinhaProducao.class)
    @XmlTransient
    LinhaProducao linhaProducao;
    
    @XmlElement
    Integer posicao;
    
    @Lob // TxT da ficha de configuracao
    @Column(length = 100000, nullable = true)
    @XmlTransient
    String fichaConfiguracaoTxt;
    
    @XmlElement
    Boolean active;
    
    @Column(columnDefinition = "integer auto_increment")
    private Integer uniqueIdentity;
    
    private String enderecoRede;
    
    private long lastActiveTime;
    
    @Transient
    @XmlTransient
    private boolean isDirty;

    protected Maquina() {

    }

    public Maquina(CodigoInternoMaquina codInterno, String numSerie, String descricao, Calendar dataInstalacao, String Marca, String Modelo) {
        Preconditions.noneNull(codInterno, numSerie, descricao, dataInstalacao, Marca, Modelo);
        this.codInterno = codInterno;
        this.numSerie = numSerie;
        this.descricao = descricao;
        this.dataInstalacao = dataInstalacao;
        this.marca = Marca;
        this.modelo = Modelo;
        this.active = true;
        this.lastActiveTime = System.currentTimeMillis();
        this.isDirty = false;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Maquina)) {
            return false;
        }
        final Maquina that = (Maquina) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    public void addFichaConfig(String fichaTxt) {
        this.fichaConfiguracaoTxt = fichaTxt;
    }

    public void definirLinhaProducao(LinhaProducao lp) {
        Preconditions.noneNull(lp);
        this.linhaProducao = lp;
    }

    @Override
    public CodigoInternoMaquina identity() {
        return codInterno;
    }
    
    public void definirPosicao(int posicao) {
        this.posicao = posicao; 
    }
    
    public Integer posicao() {
        return this.posicao;
    }
    
    public String descricao() {
        return this.descricao;
    }
    
    public Integer identificadorUnico(){
        return uniqueIdentity;
    }
    
    public LinhaProducao linhaProducao() {
        return this.linhaProducao;
    }
    
    public void atualizarEnderecoRede(String newAddress) {
        this.enderecoRede = newAddress;
    }
    
    public String enderecoRede() {
        return this.enderecoRede;
    }
    
    public boolean isActive() {
        return this.active;
    }
    
    public void deactivate() {
        this.active = false;
    }

    public void activate() {
        this.active = true;
    }
    
    public boolean isDirty() {
        return this.isDirty;
    }
    
    public long lastActiveTime() {
        return this.lastActiveTime;
    }
    
    public void setDirty(Boolean bool) {
        this.isDirty = bool;
    }
    
    public void setLastActiveTime(Long activeTime) {
        this.lastActiveTime = activeTime;
    }
    
}
