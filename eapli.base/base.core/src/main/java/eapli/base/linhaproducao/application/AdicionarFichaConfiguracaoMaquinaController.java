/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

/**
 *
 * @author PC
 */
@UseCaseController
public class AdicionarFichaConfiguracaoMaquinaController {
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MaquinasRepository maqRepo = PersistenceContext.repositories().maquinas();
    
    public boolean adicionarFichaConfig(String filePath, String maqId){
        //Verificado anteriormente se o id é valido 
        try{
            StringBuilder contentBuilder = new StringBuilder();
            //ir buscar a maquina necessaria
            Maquina maq = maqRepo.ofIdentity(CodigoInternoMaquina.valueOf(maqId)).get();
            String fichaTxt;
            
            //Leitura do ficheiro 
            Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8);
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
            fichaTxt=contentBuilder.toString();
            
            //Adicionar a Ficha
            maq.addFichaConfig(fichaTxt);
            
            //Return da confirmação da atualização
            return this.maqRepo.save(maq).sameAs(maq);
        }catch(@SuppressWarnings("unused") NoSuchElementException ex){
            System.out.println("Erro a aceder à máquina");
            return false;
        }catch (IOException e) 
        {
            System.out.println("Erro a ler o ficheiro");
            return false;
        }
    }
   
    //Utilizar na ui antes de adicionar a ficha
    public boolean validarMaqId(String maqId){
        return maqRepo.containsOfIdentity(CodigoInternoMaquina.valueOf(maqId));
    }
}
