package eapli.base.mensagem.domain;

import eapli.base.linhaproducao.domain.Maquina;
import eapli.framework.domain.model.AggregateRoot;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity
public class Mensagem implements AggregateRoot<Long> {

    @Id
    @GeneratedValue
    private Long id;
    @Version
    private Long version;

    private String mensagem;

    private TipoMensagem tipo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHora;

    @ManyToOne
    private Maquina maquina;

    public Mensagem(String mensagem, TipoMensagem tipo, Maquina maquina, Date data) {
        this.mensagem = mensagem;
        this.tipo = tipo;
        this.maquina = maquina;
        this.dataHora = data;
    }

    protected Mensagem() {
        // ORM
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Mensagem)) {
            return false;
        }
        final Mensagem that = (Mensagem) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    @Override
    public Long identity() {
        return this.id;
    }

    public String mensagem() {
        return this.mensagem;
    }

    public TipoMensagem tipoMensagem() {
        return this.tipo;
    }

    public Date data() {
        return this.dataHora;
    }

    public Maquina maquina() {
        return this.maquina;
    }

    public void especificarTipo(TipoMensagem tipo) {
        this.tipo = tipo;
    }

    public void associarData(Date data) {
        this.dataHora = data;
    }

    public void associarMaquina(Maquina maq) {
        this.maquina = maq;
    }

    @Override
    public String toString() {
        return tipo.name() + " - " + mensagem;
    }

}
