package eapli.base.mensagem.domain;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum TipoMensagem {

    POR_PROCESSAR("Por Processar", "NA"),
    
    CONSUMO("Consumo", "C0"), 
    ENTREGA_PRODUCAO("Entrega de Produção", "C9"),
    
	PRODUCAO("Produção", "P1"),
	ESTORNO("Estorno", "P2"),
    
	INICIO_ATIVIDADE("Início de Atividade", "S0"),
	RETOMA_ATIVIDADE("Retoma de Atividade", "S1"),
	PARAGEM("Paragem", "S8"),
	FIM_ATIVIDADE("Fim de Atividade", "S9");
    
    private String tipo;
    private String codigo;
    
    private TipoMensagem(String tipo, String codigo) {
        this.tipo = tipo;
        this.codigo = codigo;
    }
    
    public String tipo() {
        return this.tipo;
    }
    
    public String codigo() {
        return this.codigo;
    }
    
    public static TipoMensagem getTipoMensagem(String codigo) {
        for (TipoMensagem tipo : TipoMensagem.values()) {
            if (tipo.codigo.equalsIgnoreCase(codigo)) {
                return tipo;
            }
        }
        return null;
    }
    
}