package eapli.base.mensagem.repositories;

import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.framework.domain.repositories.DomainRepository;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public interface MensagemRepository  extends DomainRepository<Long, Mensagem> {

    Iterable<Mensagem> mensagens();
    
    Iterable<Mensagem> findByTipoMensagem(final TipoMensagem tipo);
    
    Iterable<Mensagem> findByLinhaProducao(final LinhaProducao linha);
    
    Iterable<Mensagem> findByMaquina(final Maquina maquina);
    
    Iterable<Mensagem> findPorProcessarByLinhaProducao(final LinhaProducao linha);
    
    Iterable<Mensagem> findPorProcessarByMaquina(final Maquina maquina);
}
 

