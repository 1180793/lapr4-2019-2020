/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.domain;
import eapli.framework.domain.model.ValueObject;
import java.util.List;
import javax.persistence.Embeddable;
/**
 *
 * @author David
 */
@Embeddable
public class CatalogoProduto implements ValueObject {
    
    private static final long serialVersionUID = 1L;
    
    //Lista de produtos.
    private List<Produto> catalogo;

    /**
     * Contrutor completo.
     * @param catalogo 
     */
    public CatalogoProduto(List<Produto> catalogo) {
        this.catalogo = catalogo;
    }
    
    /**
     * Contrutor vazio(ORM).
     */
     protected CatalogoProduto() {
        this.catalogo = null;
    }
    
     /**
      * Método que devolve a lsita de produtos correspondente ao catalogo de produtos.
      * @return List<Produto>
      */
    public List<Produto> catalogo(){
        return catalogo;
    }
}
