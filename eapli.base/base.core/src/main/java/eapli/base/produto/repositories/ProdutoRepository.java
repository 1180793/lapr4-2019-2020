/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.repositories;

import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public interface ProdutoRepository extends DomainRepository<CodFabrico, Produto> {

    Iterable<Produto> produtos();

    Iterable<Produto> findProdutoByCodigoFabrico(CodFabrico codigo);

    Iterable<Produto> findProdutosWithoutFichaProducao();

}
