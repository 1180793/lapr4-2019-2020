/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.produto.domain.CodComercial;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Description;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import javax.persistence.PersistenceException;

/**
 *
 * @author ZDPessoa
 */
@UseCaseController
public class AdicionarProdutoController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProdutoRepository repository = PersistenceContext.repositories().produtos();

    public Produto adicionarProduto(String codFab, String codCom, String descBreve, String descCompleta, String uni, String catProduto) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        final CodFabrico codFabrico = new CodFabrico(codFab);
        final CodComercial codComercial = new CodComercial(codCom);
        final Description descricaoBreve = Description.valueOf(descBreve);
        final Description descricaoCompleta = Description.valueOf(descCompleta);
        final Designation unidade = Designation.valueOf(uni);
        final Designation categoriaProduto = Designation.valueOf(catProduto);

        final Produto newProduto = new Produto(codFabrico, codComercial, descricaoBreve, descricaoCompleta, unidade, categoriaProduto);
        try {
            return this.repository.save(newProduto);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }
    }

    public Produto adicionarProduto(Produto prod) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        return this.repository.save(prod);
    }

}
