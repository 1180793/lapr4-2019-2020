/*
 * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.application;

import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.produto.domain.Produto;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

//import javax.persistence.TypedQuery;
/**
 *
 * @author David
 */
public class ListProdutosWithoutFichaProducaoController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProdutoRepository repositoryFichas = PersistenceContext.repositories().produtos();

    public Iterable<Produto> listProdutosWithoutFichaProducao() {

        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        Iterable<Produto> queryResult;

        queryResult = repositoryFichas.findProdutosWithoutFichaProducao();

//        Iterable<Produto> produtos = repository.findAll();
////      Iterable<FichaProducao> fichas = repositoryFichas.findAll();
//
//        for (Produto prod : produtos) {
//            if (prod.hasFichaProducao()) {
//                queryResult.add(prod);//switch list from iterable for this method.
//            }
//        }
        return queryResult;
    }
}
