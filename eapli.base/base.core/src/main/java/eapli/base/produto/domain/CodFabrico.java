/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.StringMixin;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;

/**
 *
 * @author David
 */
@Embeddable
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CodFabrico implements ValueObject, Comparable<CodFabrico>, StringMixin  {

    private static final long serialVersionUID = 1L;

    @XmlID
    @XmlAttribute
    private String codFabrico;

    /**
     * Contrutor completo.
     *
     * @param code
     */
    public CodFabrico(final String code) {
        if (StringPredicates.isNullOrEmpty(code)) {
            throw new IllegalArgumentException(
                    "Codigo Interno nao deve ser nulo nem vazio");
        }

        this.codFabrico = code;
    }

    /**
     * Contrutor vazio(ORM).
     */
    protected CodFabrico() {
        this.codFabrico = null;
    }

    /**
     * Método que devolve uma instancia de CodFabrico apartir de uma String.
     *
     * @param code
     * @return
     */
    public static CodFabrico valueOf(final String code) {
        return new CodFabrico(code);
    }

    /**
     * Método equals reescrito.
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodFabrico)) {
            return false;
        }

        final CodFabrico that = (CodFabrico) o;
        return this.codFabrico.equals(that.codFabrico);
    }

    /**
     * Método hashCode reescrito.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return this.codFabrico.hashCode();
    }

    /**
     * Método toString reescrito.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.codFabrico;
    }

    /**
     * Método compareTo reescrito.
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(CodFabrico o) {
        return codFabrico.compareTo(o.codFabrico);
    }

}
