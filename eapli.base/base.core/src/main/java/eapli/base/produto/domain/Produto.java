/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.domain;

import eapli.base.fichaproducao.domain.Material;
import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.general.domain.model.Description;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.validations.Preconditions;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author David
 */
@Entity
@Table(
        name = "Produto",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "codFabrico")
        }
)
@XmlRootElement
public class Produto extends Material implements AggregateRoot<CodFabrico>, Comparable<CodFabrico> {

    private static final long serialVersionUID = 1L;

    /**
     * Variáveis de Instância
     */
    //Código de fabrico do produto.
    @XmlIDREF
    @Column(unique = true, nullable = false, name = "codFabrico")
    private CodFabrico codFabrico;

    //Código comercial do produto.
    @XmlElement
    @Column(name = "codComercial")
    private CodComercial codComercial;

    //Descrição breve do produto.
    @XmlElement
    @AttributeOverride(name = "value", column = @Column(name = "DescricaoBreve"))
    private Description descricaoBreve;

    //Decrição completa do produto.
    @XmlElement
    @AttributeOverride(name = "value", column = @Column(name = "DescricaoCompleta"))
    private Description descricaoCompleta;

    //Categoria de Produto
    @XmlElement
    @AttributeOverride(name = "name", column = @Column(name = "CategoriaProduto"))
    private Designation categoriaProduto;

    //Ficha de Producao
    @XmlElement
    @OneToOne(mappedBy = "prodFinal")
    private FichaProducao fichaProducao;

    //Unidade de persistência.
    @XmlElement
    @AttributeOverride(name = "name", column = @Column(name = "Unidade"))
    private Designation unidade;

    /**
     * Construtor Completo.
     * @param codFabrico
     * @param codComercial
     * @param descricaoBreve
     * @param descricaoCompleta
     * @param unidade
     * @param categoriaProduto
     */
    public Produto(CodFabrico codFabrico, CodComercial codComercial, Description descricaoBreve, Description descricaoCompleta, Designation unidade, Designation categoriaProduto) {
        if ((codFabrico != null) && (codComercial != null) && (descricaoBreve != null) && (descricaoCompleta != null) && (unidade != null) && (categoriaProduto != null)) {
            this.codFabrico = codFabrico;
            this.codComercial = codComercial;
            this.descricaoBreve = descricaoBreve;
            this.descricaoCompleta = descricaoCompleta;
            this.categoriaProduto = categoriaProduto;
            this.unidade = unidade;
            this.id = codFabrico.toString();
        } else {
            throw new IllegalArgumentException("Argumentos Inválidos!");
        }

    }

    /**
     * Construtor Completo c/Ficha de Produção.
     * @param codFabrico
     * @param codComercial
     * @param descricaoBreve
     * @param descricaoCompleta
     * @param ficha
     * @param categoriaProduto
     * @param unidade
     */
    public Produto(CodFabrico codFabrico, CodComercial codComercial, Description descricaoBreve, Description descricaoCompleta, FichaProducao ficha, Designation unidade, Designation categoriaProduto) {
        Preconditions.noneNull(ficha);
        if ((codFabrico != null) && (codComercial != null) && (descricaoBreve != null) && (descricaoCompleta != null) && (unidade != null) && (categoriaProduto != null)) {
            this.codFabrico = codFabrico;
            this.codComercial = codComercial;
            this.descricaoBreve = descricaoBreve;
            this.descricaoCompleta = descricaoCompleta;
            this.fichaProducao = ficha;
            this.categoriaProduto = categoriaProduto;
            this.unidade = unidade;
            this.id = codFabrico.toString();
        } else {
            throw new IllegalArgumentException("Argumentos Inválidos!");
        }
    }

    /**
     * Contrutor vazio(ORM).
     */
    protected Produto() {
    }

    /**
     * Método que devolve o código de fabrico do produto.
     *
     * @return Long
     */
    public CodFabrico codFabrico() {
        return codFabrico;
    }

    /**
     * Método que devolve o código comercial do produto.
     *
     * @return Description
     */
    public CodComercial codComercial() {
        return codComercial;
    }

    /**
     * Método que devolve a Descricao Breve do produto.
     *
     * @return Description
     */
    public Description descricaoBreve() {
        return descricaoBreve;
    }

    /**
     * Método que devolve a Descricao Completa do produto.
     *
     * @return Description
     */
    public Description descricaoCompleta() {
        return descricaoCompleta;
    }
    
    /**
     * Método que devolve a Categoria do produto.
     *
     * @return Description
     */
    public Designation categoria() {
        return categoriaProduto;
    }
    
    /**
     * Método que devolve a Unidade do produto.
     *
     * @return Description
     */
    public Designation unidade() {
        return unidade;
    }
    
    /**
     * Método que devolve a Ficha de Produção do produto.
     *
     * @return ficha de produção
     */
    public FichaProducao fichaProducao() {
        return this.fichaProducao;
    }

    //Métodos da Interface AggregateRoot
    /**
     * Override do método sameAs()
     *
     * @param other
     * @return boolean
     */
    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Produto)) {
            return false;
        }
        final Produto that = (Produto) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    /**
     * Override do método identity()
     *
     * @return
     */
    @Override
    public CodFabrico identity() {
        return codFabrico;
    }
    
    @Override
    public String id() {
        return codFabrico.toString();
    }
    
    @Override
    public String toString() {
        return codFabrico.toString() + " - " + descricaoBreve.toString();
    }

    @Override
    public boolean hasId(String id) {
        return CodFabrico.valueOf(id).equals(this.identity());
    }

    @Override
    public String nomeMaterial() {
        return this.descricaoBreve.toString();
    }
}
