/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.base.utils.Utils;
import eapli.framework.application.UseCaseController;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@UseCaseController
public class ImportarCatalogoProdutosController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProdutoRepository repository = PersistenceContext.repositories().produtos();
    private final AdicionarProdutoController addController = new AdicionarProdutoController();
    private final String IGNORAR = "i";

    public List<Produto> importarCatalogoProdutos(String csvPath, String decisao) throws IOException {
        List<Produto> produtosCsv;
        try {
            produtosCsv = Utils.readProdutosCsv(csvPath);
        } catch (IOException ex) {
            return null;
        }
        if (decisao.equals(IGNORAR)) {
            List<Produto> listaSemRepetidos = removerProdutosIguais(produtosCsv);
            for (Produto prod : listaSemRepetidos) {
                addController.adicionarProduto(prod);
            }
        } else {
            apagarProdutosRepetidos(produtosCsv);
            for (Produto produto : produtosCsv) {
                addController.adicionarProduto(produto);
            }
        }
        return produtosCsv;
    }

    private Iterable<Produto> produtos() {
        return this.repository.findAll();
    }

    private List<Produto> removerProdutosIguais(List<Produto> list) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        Iterable<Produto> produtos = produtos();
        int i = 0;

        for (Produto prod : list) {
            for (Produto prod2 : produtos()) {
                if (prod.identity().equals(prod2.identity())) {
                    list.remove(i);
                }
            }
            i++;
        }

        return list;
    }

    private List<Produto> apagarProdutosRepetidos(List<Produto> list) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        Iterable<Produto> produtos = produtos();
        int i = 0;
        for (Produto prod : produtos) {
            if (prod.identity().equals(list.get(i).identity())) {
                this.repository.delete(prod);

            }
            i++;

        }
        return list;
    }

}
