/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.StringMixin;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author David
 */
@Embeddable
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CodComercial implements ValueObject, Comparable<CodComercial>, StringMixin {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    private String codComercial;

    /**
     * Contrutor completo.
     *
     * @param code
     */
    public CodComercial(final String code) {
        if (StringPredicates.isNullOrEmpty(code)) {
            throw new IllegalArgumentException(
                    "Codigo Interno nao deve ser nulo nem vazio");
        }

        this.codComercial = code;
    }

    /**
     * Contrutor vazio(ORM).
     */
    protected CodComercial() {
        this.codComercial = null;
    }

    /**
     * Método que devolve uma instancia de CodComercial apartir de uma String.
     *
     * @param code
     * @return CodComercial
     */
    public static CodComercial valueOf(final String code) {
        return new CodComercial(code);
    }

    /**
     * Método equals reescrito.
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodComercial)) {
            return false;
        }

        final CodComercial that = (CodComercial) o;
        return this.codComercial.equals(that.codComercial);
    }

    /**
     * Método hashCode reescrito.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return this.codComercial.hashCode();
    }

    /**
     * Método toString reescrito.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.codComercial;
    }

    /**
     * Método compareTo reescrito.
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(CodComercial o) {
        return codComercial.compareTo(o.codComercial);
    }

}
