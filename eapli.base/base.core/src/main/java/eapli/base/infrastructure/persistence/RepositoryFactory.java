/**
 *
 */
package eapli.base.infrastructure.persistence;

import eapli.base.clientusermanagement.repositories.ClientUserRepository;

import eapli.base.clientusermanagement.repositories.SignupRequestRepository;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.framework.domain.repositories.TransactionalContext;
import eapli.framework.infrastructure.authz.domain.repositories.UserRepository;

/**
 * @author Paulo Gandra Sousa
 *
 */
public interface RepositoryFactory {

    /**
     * factory method to create a transactional context to use in the repositories
     *
     * @return
     */
    TransactionalContext newTransactionalContext();

    /**
     *
     * @param autoTx the transactional context to enrol
     * @return
     */
    UserRepository users(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    UserRepository users();

    /**
     *
     * @param autoTx the transactional context to enroll
     * @return
     */
    ClientUserRepository clientUsers(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    ClientUserRepository clientUsers();

    /**
     *
     * @param autoTx the transactional context to enroll
     * @return
     */
    SignupRequestRepository signupRequests(TransactionalContext autoTx);

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    SignupRequestRepository signupRequests();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    FichaProducaoRepository fichaProducao();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    LinhasProducaoRepository linhasProducao();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    OrdemProducaoRepository ordensProducao();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    DepositosRepository depositos();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    MaquinasRepository maquinas();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    ProdutoRepository produtos();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    CategoriaMateriaPrimaRepository categoriasMateriaPrima();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    MateriaPrimaRepository materiasPrimas();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    MensagemRepository mensagens();

    /**
     * repository will be created in auto transaction mode
     *
     * @return
     */
    ExecucaoOrdemProducaoRepository execucaoOrdensProducao();

}
