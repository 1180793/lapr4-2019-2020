/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.xml.application;

import java.io.File;
import java.io.IOException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author David
 */
public class ChaoFabricaSchemaOutputResolverXSD extends SchemaOutputResolver {
    
     /**
      * Métoo que define parâmetros para o output do ficheiro .xsd
      * @param namespaceURI
      * @param fileName
      * @return
      * @throws IOException 
      */
     public Result createOutput(String namespaceURI, String fileName) throws IOException {
        File file = new File(fileName);
        StreamResult result = new StreamResult(file);
        result.setSystemId(file.getAbsolutePath());
        return result;
    }
}
