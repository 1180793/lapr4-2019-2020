/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.xml.application;

import eapli.base.usermanagement.domain.BaseRoles;
import eapli.base.xml.domain.XSLT;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author David
 */
public class TransformerXSLTController {
    
    private final AuthorizationService authz = AuthzRegistry.authorizationService();

    public void applyXSLT(String inFilename, String outFilename, String xslFilename) {
        
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        XSLT.xsl(inFilename, outFilename, xslFilename);
    }
}
