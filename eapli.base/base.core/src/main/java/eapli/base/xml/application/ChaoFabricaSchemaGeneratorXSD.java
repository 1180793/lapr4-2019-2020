/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.xml.application;

import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.IOException;
import java.text.ParseException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;


/**
 *
 * @author David
 */
public class ChaoFabricaSchemaGeneratorXSD {
    
     private final AuthorizationService authz = AuthzRegistry.authorizationService();
    
    /**
     * Método que gera um documento XSD para validaçao do Chao de Fabrica com destino ao diretorio /eapli.base
     * @param name
     * @throws JAXBException
     * @throws ParseException
     * @throws IOException 
     */
    public void exportXSD(String name) throws JAXBException, ParseException, IOException {
        
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.GESTOR_PRODUCAO);
        
//        JAXBContext jaxbContext = JAXBContext.newInstance(eapli.base.xml.application.ChaoFabrica.class);
        SchemaOutputResolver resolver = new ChaoFabricaSchemaOutputResolverXSD();
        resolver.createOutput("http://www.w3.org/2001/XMLSchema", name);
//        jaxbContext.generateSchema(resolver);
    }
}
