/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.xml.application;

import eapli.base.utils.XMLUtils;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;

/**
 *
 * @author PC
 */
public class ToXMLController {

        private final AuthorizationService authz = AuthzRegistry.authorizationService();
        
        public void export(String path) throws Exception {
            if (!path.endsWith(".xml")) {
                StringBuilder sb = new StringBuilder();
                sb.append(path);    // ui
                sb.append(".xml");
                path = sb.toString();
            }
            XMLUtils xml = new XMLUtils();
            xml.export(path);
        }
    }