<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : ChaoFabricaStylesheet.xsl
    Created on : June 5, 2020, 3:47 PM
    Author     : David
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>ChaoFabricaStylesheet.xsl</title>
            </head>
            <body>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    
    
    <xsl:template match="">
        
    </xsl:template>

    </xsl:stylesheet>
