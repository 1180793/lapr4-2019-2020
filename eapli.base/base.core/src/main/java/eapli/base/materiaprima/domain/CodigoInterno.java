/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.strings.StringMixin;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CodigoInterno implements ValueObject, Comparable<CodigoInterno>, StringMixin {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    @XmlID
    private String codInterno;

    protected CodigoInterno() {
        //empty for ORM
    }

    protected CodigoInterno(final String codInterno) {
        if (StringPredicates.isNullOrEmpty(codInterno)) {
            throw new IllegalArgumentException(
                    "Codigo Interno nao deve ser nulo nem vazio");
        }
        
        Preconditions.ensure(StringPredicates.isSingleWord(codInterno),
                "Codigo Interno não deve conter espaços");

        this.codInterno = codInterno;
    }

    public static CodigoInterno valueOf(final String codigoInterno) {
        return new CodigoInterno(codigoInterno);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodigoInterno)) {
            return false;
        }

        final CodigoInterno other = (CodigoInterno) o;
        return codInterno.equals(other.codInterno);
    }

    @Override
    public int hashCode() {
        return new HashCoder().with(codInterno).code();
    }

    @Override
    public String toString() {
        return this.codInterno;
    }

    @Override
    public int compareTo(CodigoInterno o) {
        return codInterno.compareTo(o.codInterno);
    }

}
