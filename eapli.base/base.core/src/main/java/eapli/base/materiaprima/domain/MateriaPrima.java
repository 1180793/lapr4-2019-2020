/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import eapli.base.fichaproducao.domain.Material;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.general.domain.model.Description;
import eapli.framework.validations.Preconditions;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Entity
@Table(
        name = "MateriaPrima",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "codInterno")
        }
)
@XmlRootElement
public class MateriaPrima extends Material implements AggregateRoot<CodigoInterno>, Comparable<CodigoInterno> {

    @XmlElement
    @AttributeOverride(name = "value", column = @Column(name = "descricao"))
    private Description descricao;

    @XmlIDREF
    @Column(unique = true, nullable = false)
    private CodigoInterno codInterno;

    @Lob
    @Column(length = 100000)
    @XmlElement
    private FichaTecnica fichaTecnica;

    @ManyToOne(optional = false)
    @XmlIDREF
    @XmlElement
    private CategoriaMateriaPrima categoria;

    public MateriaPrima(final Description descricao, final CodigoInterno codInterno, final FichaTecnica ft, final CategoriaMateriaPrima cat) {
        if (descricao == null || codInterno == null || ft == null || cat == null) {
            throw new IllegalArgumentException("Um dos argumentos é nulo");
        }
        this.descricao = descricao;
        this.codInterno = codInterno;
        this.fichaTecnica = ft;
        this.categoria = cat;
        this.id = codInterno.toString();
    }

    protected MateriaPrima() {
        //empty for ORM
    }

    public CategoriaMateriaPrima categoria() {
        return this.categoria;
    }

    public boolean changeCategoria(CategoriaMateriaPrima cat) {
        Preconditions.noneNull(cat);
        if (cat.descricao().isEmpty() || cat.identity().isEmpty()) {
            return false;
        }
        this.categoria = cat;
        return true;
    }

    public FichaTecnica fichaTecnica() {
        return this.fichaTecnica;
    }

    public boolean changeFichaTecnica(FichaTecnica ft) {
        Preconditions.noneNull(ft);
        this.fichaTecnica = ft;
        return true;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof MateriaPrima)) {
            return false;
        }
        final MateriaPrima that = (MateriaPrima) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    @Override
    public CodigoInterno identity() {
        return codInterno;
    }

    @Override
    public String toString() {
        return this.descricao.toString();
    }

    @Override
    public boolean hasId(String id) {
        return CodigoInterno.valueOf(id).equals(this.identity());
    }
    
    @Override
    public String id() {
        return codInterno.toString();
    }

    @Override
    public String nomeMaterial() {
        return this.descricao.toString();
    }

}
