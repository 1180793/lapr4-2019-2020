/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import eapli.framework.domain.model.ValueObject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
public class FichaTecnica implements ValueObject {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    private String name;

    @XmlTransient
    private String path;

    @XmlTransient
    @Lob
    @Column(length = 100000)
    private byte[] pdf;

    protected FichaTecnica() {
    }

    public FichaTecnica(String name, String path) {
        this.name = name;
        this.path = path;
        pdf = convertFile(path);
    }

    public String fileName() {
        return this.name;
    }

    public String filePath() {
        return this.path;
    }

    public byte[] pdf() {
        return this.pdf;
    }

    public static byte[] convertFile(String path) {
        byte[] pdf = null;
        try {
            Path pdfPath = Paths.get(path);
            pdf = Files.readAllBytes(pdfPath);
        } catch (IOException e) {
            System.out.println("File not Found!");
        } catch (Exception e) {
            System.out.println("Invalid File!");
        }
        return pdf;
    }

    public static FichaTecnica valueOf(final String name, final String path) {
        return new FichaTecnica(name, path);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FichaTecnica dataSheet = (FichaTecnica) o;
        return Objects.equals(name, dataSheet.name)
                && Objects.equals(path, dataSheet.path)
                && Arrays.equals(pdf, dataSheet.pdf);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, path);
        result = 31 * result + Arrays.hashCode(pdf);
        return result;
    }

}
