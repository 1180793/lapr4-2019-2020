/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.repositories;

import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.domain.MateriaPrima;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public interface MateriaPrimaRepository extends DomainRepository<CodigoInterno, MateriaPrima> {

    /**
     * Returns the active categorias de materia prima.
     *
     * @return An iterable for CategoriaMateriaPrima.
     */
    Iterable<MateriaPrima> materiasPrimas();

}
