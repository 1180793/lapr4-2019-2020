/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Entity
@Table(name = "CategoriaMateriaPrima")
@XmlRootElement
public class CategoriaMateriaPrima implements AggregateRoot<String> {

    private static final long serialVersionUID = 1L;

    // ORM primary key
    @Id
    @GeneratedValue
    @XmlTransient
    private Long pk;
    @Version
    @XmlTransient
    private Long version;

    // business ID
    @Column(unique = true, nullable = false)
    @XmlID
    @XmlElement
    private String codAlfaNumerico;
    @XmlElement
    private String descricao;

    protected CategoriaMateriaPrima() {
        // for ORM
    }

    /**
     * CategoriaMateriaPrima constructor.
     *
     * @param codAlfaNumerico Mandatory
     * @param descricao Mandatory
     */
    public CategoriaMateriaPrima(final String codAlfaNumerico, final String descricao) {
        setCodigo(codAlfaNumerico);
        setDescricao(descricao);
    }

    /**
     * Sets and validates newDescricao.
     *
     * @param newDescricao
     */
    private void setDescricao(final String newDescricao) {
        if (descricaoMeetsMinimumRequirements(newDescricao)) {
            this.descricao = newDescricao;
        } else {
            throw new IllegalArgumentException("Descrição Inválida");
        }
    }

    /**
     * Sets and validates newCodigo.
     *
     * @param newCodigo The new Categoria de Materia Prima codigo.
     */
    private void setCodigo(final String newCodigo) {
        if (codigoMeetsMinimumRequirements(newCodigo)) {
            this.codAlfaNumerico = newCodigo;
        } else {
            throw new IllegalArgumentException("Código Alfanumérico Inválido");
        }
    }

    /**
     * Ensure name is not null or empty.
     *
     * @param codigo The name to assess.
     * @return True if name meets minimum requirements. False if name does not meet minimum requirements.
     */
    private static boolean codigoMeetsMinimumRequirements(final String codigo) {
        return !StringPredicates.isNullOrEmpty(codigo);
    }

    /**
     * Ensure description is not null or empty.
     *
     * @param descricao The description to assess.
     * @return True if description meets minimum requirements. False if description does not meet minimum requirements.
     */
    private static boolean descricaoMeetsMinimumRequirements(final String descricao) {
        return !StringPredicates.isNullOrEmpty(descricao);
    }

    public String descricao() {
        return this.descricao;
    }

    /**
     * Change CategoriaMateriaPrima description
     *
     * @param newDescricao New description.
     */
    public void changeDescricaoTo(final String newDescricao) {
        if (!descricaoMeetsMinimumRequirements(newDescricao)) {
            throw new IllegalArgumentException();
        }
        this.descricao = newDescricao;
    }

    @Override
    public boolean hasIdentity(final String id) {
        return id.equalsIgnoreCase(this.codAlfaNumerico);
    }

    @Override
    public String identity() {
        return this.codAlfaNumerico;
    }

    @Override
    public boolean sameAs(final Object other) {
        final CategoriaMateriaPrima categoriaMateriaPrima = (CategoriaMateriaPrima) other;
        return this.equals(categoriaMateriaPrima) && descricao().equals(categoriaMateriaPrima.descricao());
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

}
