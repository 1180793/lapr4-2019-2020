/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.materiaprima.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.materiaprima.domain.CategoriaMateriaPrima;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import javax.persistence.PersistenceException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@UseCaseController
public class EspecificarCategoriaMateriaPrimaController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final CategoriaMateriaPrimaRepository repository = PersistenceContext.repositories().categoriasMateriaPrima();

    public CategoriaMateriaPrima registerCategoriaMateriaPrima(final String codigo, final String descricao) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        if (!(codigo.matches("[A-Za-z0-9]+") && (codigo.length() <= 10))) {
            throw new IllegalArgumentException();
        }

        final CategoriaMateriaPrima newCategoriaMateriaPrima = new CategoriaMateriaPrima(codigo, descricao);

        try {
            return this.repository.save(newCategoriaMateriaPrima);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }

    }
}
