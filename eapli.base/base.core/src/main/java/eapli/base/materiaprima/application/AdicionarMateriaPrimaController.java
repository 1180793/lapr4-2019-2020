/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.materiaprima.domain.CategoriaMateriaPrima;
import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.domain.FichaTecnica;
import eapli.base.materiaprima.domain.MateriaPrima;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.general.domain.model.Description;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import javax.persistence.PersistenceException;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@UseCaseController
public class AdicionarMateriaPrimaController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final MateriaPrimaRepository repository = PersistenceContext.repositories().materiasPrimas();
    private final CategoriaMateriaPrimaRepository categoriasRepository = PersistenceContext.repositories().categoriasMateriaPrima();

    public MateriaPrima adicionarMateriaPrima(final String codInterno, final String descricao,
            final String ftPath, final String codCategoria) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        CategoriaMateriaPrima cat = verificaCategoria(codCategoria);
        if (cat == null) {
            throw new IllegalArgumentException();
        }
        String[] path = ftPath.split("\\\\");
        String nomePdf = path[path.length - 1];

        final MateriaPrima newMateriaPrima = new MateriaPrima(Description.valueOf(descricao), CodigoInterno.valueOf(codInterno), FichaTecnica.valueOf(nomePdf, ftPath), cat);

        try {
            return this.repository.save(newMateriaPrima);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }
    }

    public CategoriaMateriaPrima verificaCategoria(String codAlfaNumerico) {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER,
                BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);
        Iterable<CategoriaMateriaPrima> lista = categoriasRepository.categoriasMateriaPrima();
        for (CategoriaMateriaPrima cat : lista) {
            if (cat.identity().equalsIgnoreCase(codAlfaNumerico)) {
                return cat;
            }
        }
        return null;
    }

}
