package eapli.base.utils;

import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.materiaprima.domain.CategoriaMateriaPrima;
import eapli.base.materiaprima.domain.MateriaPrima;
import eapli.base.materiaprima.repositories.CategoriaMateriaPrimaRepository;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.execucaoOrdemProducao.domain.Lote;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Goncalo
 */
public class XMLUtils {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final ProdutoRepository produtoRepository = PersistenceContext.repositories().produtos();
    private final MateriaPrimaRepository materiaPrimaRepository = PersistenceContext.repositories().materiasPrimas();
    private final CategoriaMateriaPrimaRepository categoriaMateriaPrimaRepository = PersistenceContext.repositories().categoriasMateriaPrima();
    private final DepositosRepository depositoRepository = PersistenceContext.repositories().depositos();
    private final LinhasProducaoRepository linhaProducaoRepository = PersistenceContext.repositories().linhasProducao();
    private final OrdemProducaoRepository ordensProdRepo = PersistenceContext.repositories().ordensProducao();
    //private final LoteRepository lotesRepo = PersistenceContext.repositories().lotes();
    //private final ExecucaoOrdensProducaoRepository eopRepo = PersistenceContext.repositories().execucaoOrdensProducao();

    @XmlRootElement(name = "ChaoFabrica")
    static class ChaoFabrica {

        @XmlElement
        private CategoriasMateriaPrima categoriasMateriaPrima;

        @XmlElement
        private MateriasPrimas materiasPrimas;

        @XmlElement
        private Produtos produtos;

        @XmlElement
        private Depositos depositos;

        @XmlElement
        private LinhasProducao linhasProducao;

        @XmlElement
        private Maquinas maquinas;

        @XmlElement
        private OrdensProducao ordensProd;

//        @XmlElement 
//        private Lotes lotes;
//        
//        @XmlElement 
//        private ExecucaoOrdensProducao eops;
        public ChaoFabrica() {
            this.categoriasMateriaPrima = new CategoriasMateriaPrima();
            this.materiasPrimas = new MateriasPrimas();
            this.produtos = new Produtos();
            this.depositos = new Depositos();
            this.linhasProducao = new LinhasProducao();
            this.ordensProd = new OrdensProducao();
//            this.lotes = new Lotes();
//            this.eops = new ExecucaoOrdensProducao();
        }

        @XmlRootElement(name = "CategoriasMateriaPrima")
        static class CategoriasMateriaPrima {

            @XmlElement
            private List<CategoriaMateriaPrima> categoria = new ArrayList<>();

            public void addCategoria(CategoriaMateriaPrima cat) {
                this.categoria.add(cat);
            }
        }

        @XmlRootElement(name = "MateriasPrimas")
        static class MateriasPrimas {

            @XmlElement
            private List<MateriaPrima> materiaPrima = new ArrayList<>();

            public void addMateriaPrima(MateriaPrima materia) {
                this.materiaPrima.add(materia);
            }
        }

        @XmlRootElement(name = "Produtos")
        static class Produtos {

            @XmlElement
            private List<Produto> produto = new ArrayList<>();

            public void addProduto(Produto prod) {
                this.produto.add(prod);
            }
        }

        @XmlRootElement(name = "Depositos")
        static class Depositos {

            @XmlElement
            private List<Deposito> deposito = new ArrayList<>();

            public void addDeposito(Deposito depo) {
                this.deposito.add(depo);
            }
        }

        @XmlRootElement(name = "LinhasProducao")
        static class LinhasProducao {

            @XmlElement
            private List<LinhaProducao> linhaProducao = new ArrayList<>();

            public void addLinhaProducao(LinhaProducao lp) {
                this.linhaProducao.add(lp);
            }
        }

        @XmlRootElement(name = "Maquinas")
        static class Maquinas {

            @XmlElement
            private List<Maquina> maquina = new ArrayList<>();

            public void addMaquina(Maquina maq) {
                this.maquina.add(maq);
            }
        }

        @XmlRootElement(name = "OrdensProducao")
        static class OrdensProducao {

            @XmlElement
            private List<OrdemProducao> ordem = new ArrayList<>();

            public void addOrdem(OrdemProducao ordem) {
                this.ordem.add(ordem);
            }
        }

//        @XmlRootElement(name = "ExecucaoOrdensProducao")
//        static class ExecucaoOrdensProducao{
//            
//            @XmlElement
//            private List<ExecucaoOrdemProducao> eods = new ArrayList<>();
//            
//            public void addExecucaoOrdem(ExecucaoOrdemProducao eod){
//                this.eods.add(eod);
//            }
//        }
    }

    public void export(String path) throws ClassNotFoundException, IllegalAccessException, InstantiationException, JAXBException {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.GESTOR_PRODUCAO);
        ChaoFabrica chaoFabrica = new ChaoFabrica();

        // JAXB XML
        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(ChaoFabrica.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "ChaoFabrica.xsd");
            String parentPath = file.getAbsoluteFile().getParent();

            // Matérias Primas e Categorias
            for (CategoriaMateriaPrima cat : categoriaMateriaPrimaRepository.findAll()) {
                chaoFabrica.categoriasMateriaPrima.addCategoria(cat);
            }

            // Directory Utils for Ficha Tecnica
//        String[] pathTemp = path.split("\\\\");
////        String fileName = pathTemp[pathTemp.length - 1];
//        String directory;
//        if (pathTemp.length != 1) {
//            String[] t = Arrays.copyOf(pathTemp, pathTemp.length - 1);
//            directory = Arrays.toString(t);
//        } else {
//            directory = "";
//        }
            // Matérias Primas
            for (MateriaPrima mat : materiaPrimaRepository.findAll()) {
                chaoFabrica.materiasPrimas.addMateriaPrima(mat);
                try (OutputStream out = new FileOutputStream(parentPath + File.separator + mat.fichaTecnica().fileName())) {
                    out.write(mat.fichaTecnica().pdf());
                } catch (IOException ex) {
                    System.out.println("Erro ao tentar guardar a Ficha Tecnica da Matéria-Prima ID:'" + mat.id() + "'");
                }
            }

            // Produtos e Fichas de Produção
            Iterator<Produto> itProdutos = produtoRepository.findAll().iterator();
            while (itProdutos.hasNext()) {
                chaoFabrica.produtos.addProduto(itProdutos.next());
            }

            // Depósitos
            for (Deposito depo : depositoRepository.findAll()) {
                chaoFabrica.depositos.addDeposito(depo);
            }

            //Linhas De Produção
            for (LinhaProducao lp : linhaProducaoRepository.findAll()) {
                chaoFabrica.linhasProducao.addLinhaProducao(lp);
            }

            //Ordens de Producao
            for (OrdemProducao ordem : ordensProdRepo.findAll()) {
                chaoFabrica.ordensProd.addOrdem(ordem);
            }

            //Lotes
//        for(Lote lote : ){
//            chaoFabrica.lotes.addLote(lote);
//        }
            //  ExecucaoOrdensProducao
//        for(ExecucaoOrdemProducao eop : eopRepo){
//            chaoFabrica.eops.addExecucaoOrdem(eop);
//        }
            // Exportar tudo
            marshaller.marshal(chaoFabrica, file);
        } catch (Exception ex) {
            System.out.println("Ficheiro ou diretório de ficheiro inválido!");
        }
    }
}
