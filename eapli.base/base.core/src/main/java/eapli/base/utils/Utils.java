/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.utils;

import eapli.base.deposito.domain.Deposito;
import eapli.base.execucaoOrdemProducao.domain.AtividadeMaquina;
import eapli.base.execucaoOrdemProducao.domain.Consumo;
import eapli.base.execucaoOrdemProducao.domain.Desvio;
import eapli.base.execucaoOrdemProducao.domain.DesvioComponentes;
import eapli.base.execucaoOrdemProducao.domain.DetalhesTempo;
import eapli.base.execucaoOrdemProducao.domain.EntregaProducao;
import eapli.base.execucaoOrdemProducao.domain.Estorno;
import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.execucaoOrdemProducao.domain.Lote;
import eapli.base.execucaoOrdemProducao.domain.TipoAtividade;
import eapli.base.fichaproducao.domain.Componente;
import eapli.base.fichaproducao.domain.Material;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.ordemproducao.application.AddOrdemProducaoController;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.produto.domain.CodComercial;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.general.domain.model.Description;
import eapli.framework.general.domain.model.Designation;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class Utils {

    /**
     * Le o csv que contem os produtos e retorna uma lista de produtos
     *
     * @param filePath: caminho para o ficheiro csv
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static List<Produto> readProdutosCsv(String filePath) throws FileNotFoundException, IOException {
        List<Produto> prodsValidos = new ArrayList<>();

        File errors = new File("errorLogs" + File.separator + "produtosErrorLogs.txt");
        errors.createNewFile();
        try (FileWriter writer = new FileWriter(errors)) {
            try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
                br.readLine();
                String line;
                int lineCounter = 0;
                while ((line = br.readLine()) != null) {
                    lineCounter++;
                    String[] linha = line.split(";");
                    try {
                        CodFabrico codigoFabrico = CodFabrico.valueOf(linha[0]);
                        CodComercial codigoComercial = CodComercial.valueOf(linha[1]);
                        Description descricaoBreve = Description.valueOf(linha[2]);
                        Description descricaoCompleta = Description.valueOf(linha[3]);
                        Designation unidade = Designation.valueOf(linha[4]);
                        Designation categoria = Designation.valueOf(linha[5]);

                        if (codigoFabrico == null || codigoComercial == null || descricaoBreve == null || descricaoCompleta == null || unidade == null || categoria == null) {
                            writer.write(line);
                        } else {
                            try {
                                Produto prod = new Produto(codigoFabrico, codigoComercial, descricaoBreve, descricaoCompleta, unidade, categoria);
                                prodsValidos.add(prod);
                            } catch (IllegalArgumentException ex) {
                                writer.write(line);
                            }
                        }
                    } catch (Exception e) {
                        writer.write(line);
                    }
                }
            } catch (IOException ex) {
                System.out.println("O caminho para o Ficheiro é inválido!\n");
            }
        }
        return prodsValidos;

    }

    /**
     * Le o csv que contem as ordens de producao e adiciona na base de dados chamando o controller de adicionar ordens de producao
     *
     * @param filePath
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void readOrdemProducaoCsv(String filePath) throws FileNotFoundException, IOException {
        AddOrdemProducaoController theController = new AddOrdemProducaoController();

        File errors = new File("errorLogs" + File.separator + "errorOrdemProducaoLog.txt");
        errors.createNewFile();
        try (FileWriter writer = new FileWriter(errors)) {
            try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
                br.readLine();
                String line;
                int lineCounter = 0;
                while ((line = br.readLine()) != null) {
                    lineCounter++;
                    String[] linha = line.split(";");
                    try {
                        String codInterno = (linha[0]);

                        String dataEmissao = linha[1];
                        try {
                            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                            sdf1.parse(dataEmissao);
                        } catch (ParseException ex) {
                            throw new IllegalArgumentException("Data de Emissão Inválida! (Linha #" + lineCounter + ")");
                        }

                        String dataPrevistaExecucao = linha[2];
                        try {
                            SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
                            sdf1.parse(dataPrevistaExecucao);
                        } catch (ParseException ex) {
                            throw new IllegalArgumentException("Data Prevista de Execução Inválida! (Linha #" + lineCounter + ")");
                        }

                        String codigoFabricoProduto = linha[3];
                        if (!theController.isValidProduto(codigoFabricoProduto)) {
                            throw new IllegalArgumentException("Não foi encontrado um Produto com o Código de Fabrico introduzido! (Linha #" + lineCounter + ")");
                        }

                        Double quantidadeProduto = Double.parseDouble(linha[4]);
                        if (quantidadeProduto <= 0) {
                            throw new IllegalArgumentException("Quantidade de Produto Inválida! (Linha #" + lineCounter + ")");
                        }

                        String unidade = linha[5];

                        String encomenda = linha[6];
                        String[] encomendas = encomenda.split(",");
                        Set<Encomenda> setEncomendas = new HashSet<>();
                        for (String encomenda1 : encomendas) {
                            setEncomendas.add(Encomenda.valueOf(encomenda1));
                        }

                        if (setEncomendas.isEmpty() || codInterno == null || dataEmissao == null || dataPrevistaExecucao == null || codigoFabricoProduto == null || unidade == null || quantidadeProduto == 0.0) {
                            writer.write("#" + lineCounter + ": " + line);
                        } else {
                            try {
                                theController.adicionarOrdemProducao(codInterno, setEncomendas, dataEmissao, dataPrevistaExecucao, quantidadeProduto, unidade, codigoFabricoProduto);
                            } catch (IllegalArgumentException ex) {
                                writer.write("#" + lineCounter + ": " + line + "\n");

                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                        writer.write("#" + lineCounter + ": " + line);
                    }
                }
            } catch (IOException ex) {
                System.out.println("O caminho para o Ficheiro é inválido!\n");
            }
        }
    }

    public static <T> List<T> IterableToArray(Iterable<T> it, T type) {
        List<T> result = new ArrayList<>();
        for (T t : it) {
            result.add(t);
        }

        return result;
    }

    public static Map<TimeUnit, Long> computeDiff(Date date1, Date date2) {

        long diffInMillies = date2.getTime() - date1.getTime();

        //create the list
        List<TimeUnit> units = new ArrayList<>(EnumSet.allOf(TimeUnit.class));
        Collections.reverse(units);

        //create the result map of TimeUnit and difference
        Map<TimeUnit, Long> result = new LinkedHashMap<>();
        long milliesRest = diffInMillies;

        for (TimeUnit unit : units) {
            //calculate difference in millisecond 
            long diff = unit.convert(milliesRest, TimeUnit.MILLISECONDS);
            long diffInMilliesForUnit = unit.toMillis(diff);
            milliesRest = milliesRest - diffInMilliesForUnit;

            //put the result in the map
            result.put(unit, diff);
        }

        return result;
    }

    public static LocalDateTime toLocalDate(Date oldDate) {
        return oldDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Duration subtractDates(Date finishTime, Date beginTime) {
        return Duration.between(toLocalDate(beginTime), toLocalDate(finishTime));
    }

    public static void exportarOrdemProducao(ExecucaoOrdemProducao execucaoOrdem) {
        File file = new File("Relatorio_OrdemProducao_" + execucaoOrdem.identity() + ".md");
        String newLine = System.lineSeparator();
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), Charset.forName("UTF-8").newEncoder())) {
            // Titulo
            writer.write("## Relatório Ordem de Produção #" + execucaoOrdem.identity() + " ##");
            writer.write(newLine);
            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            writer.write("*Relatório gerado a: " + sdf.format(date) + "*");
            writer.write(newLine);
            writer.write("### Linha de Produção + Máquinas Usadas ###");
            writer.write(newLine);
            writer.write(newLine);
            writer.write("Linha de Produção: " + execucaoOrdem.linha().identity());
            writer.write(newLine);
            writer.write(newLine);

            for (Maquina maq : execucaoOrdem.linha().maquinas()) {
                writer.write("- " + maq.identity() + " -> " + maq.descricao());
                writer.write(newLine);
            }
            writer.write(newLine);
            writer.write("Linha = ");
            Iterator<Maquina> it = execucaoOrdem.linha().maquinas().iterator();
            while (it.hasNext()) {
                Maquina maq = it.next();
                if (it.hasNext()) {
                    writer.write(maq.identity() + " -> ");
                } else {
                    writer.write(maq.identity().toString());
                }
            }

            writer.write(newLine);
            writer.write(newLine);

            writer.write("### Ficha de Produção ###");
            writer.write(newLine);
            writer.write(newLine);
            writer.write("ID do Produto: " + execucaoOrdem.ordemProducao().produto().identity().toString());
            writer.write(newLine);
            writer.write(newLine);
            writer.write("Para fabricar " + execucaoOrdem.ordemProducao().produto().fichaProducao().quantidadeProdutoFinal()
                    + " " + execucaoOrdem.ordemProducao().produto().unidade() + " de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                    + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") são necessários: ");
            writer.write(newLine);
            for (Componente comp : execucaoOrdem.ordemProducao().produto().fichaProducao().componentes()) {
                writer.write("- " + comp.quantidade() + " unidades de " + comp.material().nomeMaterial() + "(" + comp.material().id() + ")");
                writer.write(newLine);
            }

            writer.write(newLine);
            writer.write("### Resultados ###");
            writer.write(newLine);
            writer.write(newLine);
            double totalProducao = 0.0;
            for (Lote lote : execucaoOrdem.lotes()) {
                totalProducao = totalProducao + lote.unidades();
            }
            writer.write("#### Produção ####");
            writer.write(newLine);
            writer.write("- " + totalProducao + " unidades de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                    + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") divididos nos seguintes lotes: ");
            writer.write(newLine);
            writer.write(newLine);
            writer.write("| Lote | Quantidade |");
            writer.write(newLine);
            writer.write("|:--------  |:-------------- |");
            writer.write(newLine);
            for (Lote lote : execucaoOrdem.lotes()) {
                writer.write("| " + lote.codigo() + " | " + lote.unidades() + " |");
                writer.write(newLine);
            }
            writer.write(newLine);
            writer.write(newLine);

            // Consumos
            Map<Material, Double> mapConsumos = new HashMap<>();
            Deposito deposito = null;
            for (Consumo consumo : execucaoOrdem.consumos()) {
                if (mapConsumos.containsKey(consumo.material())) {
                    mapConsumos.put(consumo.material(), mapConsumos.get(consumo.material()) + consumo.valorConsumo());
                } else {
                    mapConsumos.put(consumo.material(), consumo.valorConsumo());
                }
                deposito = consumo.deposito();
            }
            writer.write("#### Consumos ####");
            writer.write(newLine);
            for (Material mat : mapConsumos.keySet()) {
                writer.write("- Foram consumidas " + mapConsumos.get(mat) + " unidades de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                    + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") do Depósito ");
                if (deposito != null) {
                    writer.write(deposito.identity().toString());
                }
                writer.write(newLine);
            }
            
            writer.write(newLine);

            // Estornos
            Map<Material, Double> mapEstornos = new HashMap<>();
            deposito = null;
            for (Estorno estorno : execucaoOrdem.estornos()) {
                if (mapEstornos.containsKey(estorno.material())) {
                    mapEstornos.put(estorno.material(), mapEstornos.get(estorno.material()) + estorno.valorEstorno());
                } else {
                    mapEstornos.put(estorno.material(), estorno.valorEstorno());
                }
                deposito = estorno.deposito();
            }
            writer.write("#### Estornos ####");
            writer.write(newLine);
            for (Material mat : mapEstornos.keySet()) {
                writer.write("- Foram estornadas " + mapEstornos.get(mat) + " unidades de " + mat.nomeMaterial() + "(" + mat.id()
                        + ") para o Depósito ");
                if (deposito != null) {
                    writer.write(deposito.identity().toString());
                }
                writer.write(newLine);
            }

            writer.write(newLine);
            
            writer.write("#### Entregas de Produção ####");
            writer.write(newLine);
            Map<Deposito, Double> mapEntregas = new HashMap<>();
            for (EntregaProducao entrega : execucaoOrdem.entregasProducao()) {
                if (mapEntregas.containsKey(entrega.deposito())) {
                    mapEntregas.put(entrega.deposito(), mapEntregas.get(entrega.deposito()) + entrega.quantidade());
                } else {
                    mapEntregas.put(entrega.deposito(), entrega.quantidade());
                }
            }
            writer.write(newLine);
            for (Deposito depo : mapEntregas.keySet()) {
                writer.write("- Foram entregues " + mapEntregas.get(depo) + " unidades de " + " para o Depósito ");
                if (deposito != null) {
                    writer.write(deposito.identity().toString());
                }
                writer.write(newLine);
            }

            writer.write(newLine);

            writer.write("#### Desvios ####");
            writer.write(newLine);
            Desvio desvio = execucaoOrdem.desvio();
            if (desvio.diferencaQuantidade() < 0) {
                writer.write("- Foram produzidas menos " + Math.abs(desvio.diferencaQuantidade()) + " unidades de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                        + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") do que o previsto para a Ordem de Produção " + execucaoOrdem.ordemProducao().identity().toString());
            } else if (desvio.diferencaQuantidade() > 0) {
                writer.write("- Foram produzidas mais " + desvio.diferencaQuantidade() + " unidades de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                        + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") do que o previsto para a Ordem de Produção " + execucaoOrdem.ordemProducao().identity().toString());
            } else {
                writer.write("- Foram produzidas as unidades de " + execucaoOrdem.ordemProducao().produto().descricaoBreve()
                        + "(" + execucaoOrdem.ordemProducao().produto().identity().toString() + ") previstas para a Ordem de Produção " + execucaoOrdem.ordemProducao().identity().toString());
            }
            writer.write(newLine);
            for (DesvioComponentes desvioComponente : desvio.desvioComponentes()) {
                if (desvioComponente.quantidade() < 0) {
                    writer.write("- Foram consumidas menos " + desvioComponente.quantidade() + " unidades de " + desvioComponente.material().nomeMaterial() + "(" + desvioComponente.material().id()
                            + ") do que o estimado pela Ficha de Produção.");
                } else if (desvioComponente.quantidade() > 0) {
                    writer.write("- Foram consumidas mais " + desvioComponente.quantidade() + " unidades de " + desvioComponente.material().nomeMaterial() + "(" + desvioComponente.material().id()
                            + ") do que o estimado pela Ficha de Produção.");
                } else {
                    writer.write("- Foram consumidas as unidades de " + desvioComponente.material().nomeMaterial() + "(" + desvioComponente.material().id()
                            + ") estimadas pela Ficha de Produção.");
                }
                writer.write(newLine);
            }

            writer.write(newLine);

            writer.write("#### Tempos ####");
            writer.write(newLine);
            writer.write("- Início/Fim da Ordem de Produção: " + sdf.format(execucaoOrdem.horaInicio()) + " até " + sdf.format(execucaoOrdem.horaFim()));
            writer.write(newLine);

            List<Maquina> lstMaquinasPorOrdem = new ArrayList<>(execucaoOrdem.linha().maquinas());
            Collections.sort(lstMaquinasPorOrdem, new OrderMaquinas());
            for (Maquina maquina : lstMaquinasPorOrdem) {
                Date inicio = null;
                Date fim = null;
                for (AtividadeMaquina evento : execucaoOrdem.atividadesMaquina()) {
                    if (evento.maquina().sameAs(maquina)) {
                        if (evento.tipo() == TipoAtividade.INICIO_ATIVIDADE) {
                            inicio = evento.data();
                        } else if (evento.tipo() == TipoAtividade.FIM_ATIVIDADE) {
                            fim = evento.data();
                        }
                    }
                }
                if (inicio != null && fim != null) {
                    writer.write("- Início/Fim Operação " + maquina.identity().toString() + ": " + sdf.format(inicio) + " até " + sdf.format(fim));
                    writer.write(newLine);
                }
            }
            writer.write(newLine);
            writer.write(newLine);
            int secs = (int) execucaoOrdem.tempoBruto().getSeconds();
            String tempoBruto = String.format("%d minutos e %d segundos", (secs % 3600) / 60, (secs % 60));
            writer.write("- Tempo Bruto Ordem Produção: " + tempoBruto);
            writer.write(newLine);
            for (DetalhesTempo tempo : execucaoOrdem.tempos()) {
                secs = (int) tempo.tempoBruto().getSeconds();
                tempoBruto = String.format("%d minutos e %d segundos", (secs % 3600) / 60, (secs % 60));
                writer.write("- Tempo Bruto " + tempo.maquina().identity().toString() + ": " + tempoBruto);
                writer.write(newLine);
            }

            writer.write(newLine);
            writer.write(newLine);
            
            secs = (int) execucaoOrdem.tempoEfetivo().getSeconds();
            String tempoEfetivo = String.format("%d minutos e %d segundos", (secs % 3600) / 60, (secs % 60));
            writer.write("- Tempo Efetivo Ordem Produção: " + tempoEfetivo);
            writer.write(newLine);
            for (DetalhesTempo tempo : execucaoOrdem.tempos()) {
                secs = (int) tempo.tempoEfetivo().getSeconds();
                tempoEfetivo = String.format("%d minutos e %d segundos", (secs % 3600) / 60, (secs % 60));
                writer.write("- Tempo Efetivo " + tempo.maquina().identity().toString() + ": " + tempoEfetivo);
                writer.write(newLine);
            }
            
            writer.close();

        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static class OrderMensagens implements Comparator<Mensagem> {

        @Override
        public int compare(Mensagem o1, Mensagem o2) {

            Maquina maq1 = o1.maquina();
            Maquina maq2 = o2.maquina();

            int compare = maq1.posicao().compareTo(maq2.posicao());

            if (compare == 0) {
                Date o1Date = o1.data();
                Date o2Date = o2.data();

                if (o1Date.before(o2Date)) {
                    return -1;
                } else if (o1Date.after(o2Date)) {
                    return 1;
                } else {
                    return 0;
                }
            }
            return compare;
        }

    }

    public static class OrderMaquinas implements Comparator<Maquina> {

        @Override
        public int compare(Maquina o1, Maquina o2) {

            Integer maq1 = o1.posicao();
            Integer maq2 = o2.posicao();

            int compare = maq1.compareTo(maq2);
            return compare;
        }

    }

    public static class OrderAtividadeMaquina implements Comparator<AtividadeMaquina> {

        @Override
        public int compare(AtividadeMaquina o1, AtividadeMaquina o2) {

            Date o1Date = o1.data();
            Date o2Date = o2.data();

            if (o1Date.before(o2Date)) {
                return -1;
            } else if (o1Date.after(o2Date)) {
                return 1;
            } else {
                return 0;
            }
        }

    }

}
