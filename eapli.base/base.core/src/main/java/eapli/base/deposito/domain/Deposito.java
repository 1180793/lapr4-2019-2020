/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.deposito.domain;

import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.validations.Preconditions;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ZDPessoa
 */
@Entity
@Table(
        name = "Deposito",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "codInterno")
        }
)
@XmlRootElement
public class Deposito implements AggregateRoot<CodigoInternoDeposito> {

    @Id
    @GeneratedValue
    @XmlTransient
    private Long id;

    @Version
    @XmlTransient
    private Long version;

    @Column(unique = true, nullable = false)
    @XmlElement
    private CodigoInternoDeposito codInterno;

    @XmlElement
    private String descricao;

    public Deposito(CodigoInternoDeposito codInterno, String descricao) {
        Preconditions.noneNull(codInterno, descricao);
        this.codInterno = codInterno;
        this.descricao = descricao;
    }

    protected Deposito() {
        this.descricao = null;
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof LinhaProducao)) {
            return false;
        }
        final Deposito that = (Deposito) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    @Override
    public CodigoInternoDeposito identity() {
        return codInterno;
    }

}
