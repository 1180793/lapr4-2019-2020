/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.deposito.repositories;

import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author David
 */
public interface DepositosRepository extends DomainRepository<CodigoInternoDeposito, Deposito> {
    /**
     * Returns the active depositos.
     *
     * @return An iterable for Deposito.
     */
    Iterable<Deposito> depositos();
   
    Iterable<Deposito> findByCodigo(final CodigoInternoDeposito who);
}