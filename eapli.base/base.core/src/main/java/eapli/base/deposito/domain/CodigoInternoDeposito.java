/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.deposito.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.validations.Preconditions;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author ZDPessoa
 */
@Embeddable
public class CodigoInternoDeposito implements ValueObject, Comparable<CodigoInternoDeposito> {

    private static final long serialVersionUID = 1L;

    @XmlAttribute
    private String codInterno;

    protected CodigoInternoDeposito() {

    }

    private CodigoInternoDeposito(final String codigoInterno) {
        Preconditions.nonEmpty(codigoInterno, "Código Interno não pode ser nulo ou vazio.");
        Preconditions.ensure(StringPredicates.isSingleWord(codigoInterno), "Código Interno não pode conter espaços");

        this.codInterno = codigoInterno;
    }

    public static CodigoInternoDeposito valueOf(final String codigoInterno) {
        return new CodigoInternoDeposito(codigoInterno);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodigoInternoDeposito)) {
            return false;
        }

        final CodigoInternoDeposito that = (CodigoInternoDeposito) o;
        return this.codInterno.equals(that.codInterno);
    }

    @Override
    public int hashCode() {
        return this.codInterno.hashCode();
    }

    @Override
    public String toString() {
        return this.codInterno;
    }

    @Override
    public int compareTo(CodigoInternoDeposito o) {
        return codInterno.compareTo(o.codInterno);
    }

}
