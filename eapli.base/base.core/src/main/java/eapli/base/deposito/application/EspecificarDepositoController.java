/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.deposito.application;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.application.UseCaseController;
import eapli.framework.domain.repositories.IntegrityViolationException;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import javax.persistence.PersistenceException;

/**
 *
 * @author David
 */
@UseCaseController
public class EspecificarDepositoController {

    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private final DepositosRepository repositoryDep = PersistenceContext.repositories().depositos();

    public Deposito addDeposito(final String codigo, final String descricao) {

        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_CHAO_FABRICA);

        CodigoInternoDeposito code = CodigoInternoDeposito.valueOf(codigo);
        final Deposito newDeposito = new Deposito(code, descricao);

        try {
            return this.repositoryDep.save(newDeposito);
        } catch (PersistenceException e) {
            throw new IntegrityViolationException();
        }
    }
}
