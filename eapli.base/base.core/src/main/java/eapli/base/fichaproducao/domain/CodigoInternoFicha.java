/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;
import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.Embeddable;

/**
 *
 * @author PC
 */
@Embeddable
public class CodigoInternoFicha implements ValueObject, Comparable<CodigoInternoFicha> {

    private static final long serialVersionUID = 1L;
    private String number;

    public CodigoInternoFicha(final String codigoInternoFicha) {
        if (StringPredicates.isNullOrEmpty(codigoInternoFicha)) {
            throw new IllegalArgumentException("--vazio--");
        }
       
        this.number = codigoInternoFicha;
    }

    protected CodigoInternoFicha() {
        // for ORM
    }

    public static CodigoInternoFicha valueOf(final String codigoInternoFicha) {
        return new CodigoInternoFicha(codigoInternoFicha);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodigoInternoFicha)) {
            return false;
        }

        final CodigoInternoFicha that = (CodigoInternoFicha) o;
        return this.number.equals(that.number);
    }

    @Override
    public int hashCode() {
        return this.number.hashCode();
    }

    @Override
    public String toString() {
        return this.number;
    }

    @Override
    public int compareTo(final CodigoInternoFicha arg0) {
        return number.compareTo(arg0.number);
    }
}
