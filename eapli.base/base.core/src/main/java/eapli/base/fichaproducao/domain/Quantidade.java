/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.util.HashCoder;
import eapli.framework.validations.Preconditions;
import javax.persistence.*;

/**
 *
 * @author PC
 */
@Embeddable
public class Quantidade implements ValueObject {
    private final double quantidade;

    protected Quantidade() {
        quantidade=0;
    }
    
    public Quantidade(final double quantidade) {
        Preconditions.ensure(quantidade >= 0, "Quantidade não pode ser negativa");
        this.quantidade = quantidade;
    }
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Quantidade)) {
            return false;
        }

        final Quantidade that = (Quantidade) o;
        return quantidade == that.quantidade;
    }

    @Override
    public int hashCode() {
        return new HashCoder().with(quantidade).code();
    }

    @Override
    public String toString() {
        return String.valueOf(quantidade);
    }
}
