/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import eapli.framework.domain.model.ValueObject;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Entity
public class Componente implements ValueObject {
    
    @XmlTransient
    @Id
    @GeneratedValue
    private Long pk;
    @XmlTransient
    @Version
    private Long version;
    
    @XmlIDREF
    @ManyToOne(targetEntity = Material.class)
    private Material material;
    
    @XmlElement
    private Double quantidade;
    
    protected Componente() {
        //empty for ORM
    }
    
    public Componente(final Material material, final Double quantidade) {
        this.material = material;
        this.quantidade = quantidade;
    }
    
    public Material material() {
        return material;
    }
    
    public Double quantidade() {
        return this.quantidade;
    }
    
}
