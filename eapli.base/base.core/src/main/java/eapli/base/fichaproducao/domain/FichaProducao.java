/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.domain.model.AggregateRoot;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author PC
 */
@Entity
public class FichaProducao implements AggregateRoot<CodFabrico>{
    
    // ORM primary key
    @Id
    @GeneratedValue
    @XmlTransient
    private Long pk;
    @XmlTransient
    @Version
    private Long version;
    
    @XmlElement
    @OneToMany(targetEntity = Componente.class, cascade = CascadeType.ALL)
    private Set<Componente> componentes = new HashSet<>();
    
    @XmlElement
    private double quantidadeProdFinal;
    
    @OneToOne
    @JoinColumn(name = "Produto")
    @XmlTransient
    private Produto prodFinal;
    
    protected FichaProducao() {
    }
    
    public FichaProducao(final Produto prodFinal, Set<Componente> componentes, final double quantidadeProdFinal) {
        if (prodFinal.toString().isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.prodFinal = prodFinal;
        this.componentes = componentes;
        this.quantidadeProdFinal = quantidadeProdFinal;
    }
    
    public Produto produto() {
        return this.prodFinal;
    }
    
    public double quantidadeProdutoFinal() {
        return this.quantidadeProdFinal;
    }
    
    public Set<Componente> componentes() {
        return this.componentes;
    }
    
    @Override
    public boolean sameAs(Object other) {
         if (!this.equals(other)) {
            return false;
        }
        final FichaProducao that = (FichaProducao) other;
        return that.identity().equals(this.identity());
    }

    @Override
    public CodFabrico identity() {
        return prodFinal.identity();
    }
    
}
