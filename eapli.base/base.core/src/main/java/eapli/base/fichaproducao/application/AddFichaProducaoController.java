/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.application;

import eapli.base.fichaproducao.domain.Componente;
import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.base.fichaproducao.domain.Material;
import eapli.base.fichaproducao.repositories.FichaProducaoRepository;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.usermanagement.domain.BaseRoles;
import eapli.framework.infrastructure.authz.application.AuthorizationService;
import eapli.framework.infrastructure.authz.application.AuthzRegistry;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author PC
 */
public class AddFichaProducaoController {

    private final FichaProducaoRepository fichaRepository = PersistenceContext.repositories().fichaProducao();
    private final ProdutoRepository produtosRepository = PersistenceContext.repositories().produtos();
    private final MateriaPrimaRepository materiaPrimaRepository = PersistenceContext.repositories().materiasPrimas();
    private final AuthorizationService authz = AuthzRegistry.authorizationService();
    private Produto prodFinal = null;
    private Double qtddProdutoFinal = 0.0;
    final Set<Componente> componentes = new HashSet<>();
    
    public boolean isValidProduto(String id) {
        return getProduto(id) != null;
    }

    public boolean hasFichaProducao(String id) {
        for (FichaProducao ficha : fichaRepository.findAll()) {
            if (ficha.produto().hasIdentity(CodFabrico.valueOf(id))) {
                return true;
            }
        }
        return false;
    }

    public void setProdutoFinal(String id, Double quantidade) {
        this.prodFinal = (Produto) getProduto(id);
        this.qtddProdutoFinal = quantidade;
    }

    public boolean isValidMaterial(String id) {
        return getMaterial(id) != null;
    }

    public boolean hasRepeatedIdentity(String id) {
        return getProduto(id) != null && getMateriaPrima(id) != null;
    }

    public Material getMaterial(String id) {

        if (getMateriaPrima(id) != null) {
            return getMateriaPrima(id);
        }

        if (getProduto(id) != null) {
            return getProduto(id);
        }

        return null;
    }

    private Material getMateriaPrima(String id) {
        if (this.materiaPrimaRepository.containsOfIdentity(CodigoInterno.valueOf(id))) {
            return this.materiaPrimaRepository.ofIdentity(CodigoInterno.valueOf(id)).get();
        }
        return null;
    }

    private Material getProduto(String id) {
        if (this.produtosRepository.containsOfIdentity(CodFabrico.valueOf(id))) {
            return this.produtosRepository.ofIdentity(CodFabrico.valueOf(id)).get();
        }
        return null;
    }

    public void addComponente(String materialID, Double quantidadeComponente) {
        Componente comp = checkComponente(materialID);
        if (comp != null) {
            componentes.remove(comp);
        }
        Material material = getMaterial(materialID);
        Componente newComp = new Componente(material, quantidadeComponente);
        componentes.add(newComp);
    }
    
    private Componente checkComponente(String id) {
        for (Componente comp : componentes) {
            if (comp.material().hasId(id)) {
                return comp;
            }
        }
        return null;
    }

    public void addComponente(String materialID, Double quantidadeComponente, int option) {
        Componente comp = checkComponente(materialID);
        if (comp != null) {
            componentes.remove(comp);
        }
        if (option == 1) {
            Material material = (Material) getMateriaPrima(materialID);
            Componente newComp = new Componente(material, quantidadeComponente);
            componentes.add(newComp);
        }
        if (option == 2) {
            Material material = (Material) getProduto(materialID);
            Componente newComp = new Componente(material, quantidadeComponente);
            componentes.add(newComp);
        }
    }

    public void removePreviousFichaProducao() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        if (prodFinal != null) {
            for (FichaProducao ficha : fichaRepository.findAll()) {
                if (ficha.produto().hasIdentity(prodFinal.identity())) {
                    this.fichaRepository.remove(ficha);
                }
            }
        }
    }
    
    public void addFichaProducao() {
        authz.ensureAuthenticatedUserHasAnyOf(BaseRoles.POWER_USER, BaseRoles.ADMIN, BaseRoles.GESTOR_PRODUCAO);

        if (prodFinal != null && qtddProdutoFinal > 0 && !componentes.isEmpty()) {
            FichaProducao newFicha = new FichaProducao(prodFinal, componentes, qtddProdutoFinal);
            this.fichaRepository.save(newFicha);
        } else {
            throw new IllegalArgumentException("O número de Componentes tem que ser superior a 0.");
        }
    }
    
}
