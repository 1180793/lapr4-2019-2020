/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.repositories;

import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.domain.repositories.DomainRepository;

/**
 *
 * @author PC
 */
public interface FichaProducaoRepository extends DomainRepository<CodFabrico, FichaProducao> {

    Iterable<FichaProducao> findByCodigo(final CodFabrico who);
    

}
