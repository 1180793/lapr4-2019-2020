/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.execucaoOrdemProducao.domain;

import eapli.framework.domain.model.DomainEntity;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Entity
public class Lote implements DomainEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private Long version;

    @Column(unique = true, nullable = false)
    private CodInternoLote code;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataProducao;
    
    private Double numeroUnidades;
    
    @ManyToOne(targetEntity = ExecucaoOrdemProducao.class)
    private ExecucaoOrdemProducao execucaoOrdem;

    protected Lote() {
    }

    public Lote(CodInternoLote code, Date dataProducao, Double numeroUnidades) {
        this.code = code;
        this.dataProducao = dataProducao;
        this.numeroUnidades = numeroUnidades;
    }
    
    public void associarExecucaoOrdemProducao(ExecucaoOrdemProducao ordem) {
        this.execucaoOrdem = ordem;
    }

    /**
     * Método sameAs reescrito.
     *
     * @param other
     * @return
     */
    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Lote)) {
            return false;
        }
        final Lote that = (Lote) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }
    
    @Override
    public boolean equals(Object other) {
        return sameAs(other);
    }

    /**
     * Método que devolve CodInternoLote da instância de lote.
     *
     * @return
     */
    @Override
    public Object identity() {
        return code;
    }

    /**
     * Método compareTo reescrito.
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Object o) {
        return this.compareTo((Lote) o);
    }
    
    public void adicionarUnidades(Double quantidade) {
        this.numeroUnidades = this.numeroUnidades + quantidade;
    }
    
    public Double unidades() {
        return this.numeroUnidades;
    }
    
    public CodInternoLote codigo() {
        return this.code;
    }

}
