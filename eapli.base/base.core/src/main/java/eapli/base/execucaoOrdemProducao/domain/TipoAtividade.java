/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.execucaoOrdemProducao.domain;

import javax.persistence.Embeddable;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum TipoAtividade {

	INICIO_ATIVIDADE("Início de Atividade", "S0"),
	RETOMA_ATIVIDADE("Retoma de Atividade", "S1"),
	PARAGEM("Paragem", "S8"),
	FIM_ATIVIDADE("Fim de Atividade", "S9");
    
    private final String tipo;
    private final String codigo;

    private TipoAtividade(String tipo, String codigo) {
        this.tipo = tipo;
        this.codigo = codigo;
    }

    public String tipo() {
        return this.tipo;
    }

    public String codigo() {
        return this.codigo;
    }

}
