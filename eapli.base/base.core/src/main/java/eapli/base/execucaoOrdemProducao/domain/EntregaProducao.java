package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.deposito.domain.Deposito;
import eapli.base.produto.domain.Produto;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.time.util.Calendars;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Embeddable
public class EntregaProducao implements DomainEvent {

    private Double quantidade;
    
    @ManyToOne(targetEntity = Produto.class)
    private Produto produtoFinal;
    
    @ManyToOne(targetEntity = Deposito.class)
    private Deposito deposito;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOcorreu;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistada;

    /**
     * Construtor completo da Entrega de Produção
     *
     * @param quantidade
     * @param produtoFinal
     * @param deposito
     * @param data
     */
    public EntregaProducao(Double quantidade, Produto produtoFinal, Deposito deposito, Date data) {
        this.quantidade = quantidade;
        this.produtoFinal = produtoFinal;
        this.deposito = deposito;
        this.dataOcorreu = data;
        this.dataRegistada = new Date();
    }
    
    /**
     * Construtor vazio para o ORM
     */
    protected EntregaProducao() {
        //for ORM
    }
    
    public Double quantidade() {
        return this.quantidade;
    }
    
    public Produto produto() {
        return this.produtoFinal;
    }
    
    public Deposito deposito() {
        return this.deposito;
    }

    @Override
    public Calendar occurredAt() {
        return Calendars.fromDate(dataOcorreu);
    }

    @Override
    public Calendar registeredAt() {
        return Calendars.fromDate(dataRegistada);
    }
    
}
