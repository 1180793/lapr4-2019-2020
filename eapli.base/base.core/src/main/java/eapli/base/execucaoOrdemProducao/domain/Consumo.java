/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.deposito.domain.Deposito;
import eapli.base.fichaproducao.domain.Material;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.time.util.Calendars;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Quantidade de materia prima/produto consumido no processo da execuçao de uma ordem de produçao
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
public class Consumo implements DomainEvent {

    private static final long serialVersionUID = 1L;

    private Double consumo;

    @ManyToOne(targetEntity = Material.class)
    private Material material;

    @ManyToOne(targetEntity = Deposito.class)
    private Deposito deposito;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOcorreu;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistada;

    /**
     * Construtor completo do Consumo
     *
     * @param consumo
     * @param material
     * @param deposito
     * @param data
     */
    public Consumo(Double consumo, Material material, Deposito deposito, Date data) {
        this.consumo = consumo;
        this.material = material;
        this.deposito = deposito;
        this.dataOcorreu = data;
        this.dataRegistada = new Date();
    }

    /**
     * Construtor vazio para o ORM
     */
    protected Consumo() {
        //for ORM
    }

    /**
     * Método equals reescrito.
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Consumo)) {
            return false;
        }

        final Consumo that = (Consumo) o;
        return this.consumo.equals(that.consumo);
    }

    /**
     * Método hashCode reescrito.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.consumo);
        return hash;
    }

    /**
     * Método toString reescrito.
     *
     * @return String
     */
    @Override
    public String toString() {
        return String.format("Consumo:%.3f", consumo);
    }

    public Material material() {
        return this.material;
    }

    public Deposito deposito() {
        return this.deposito;
    }

    public Double valorConsumo() {
        return this.consumo;
    }

    @Override
    public Calendar occurredAt() {
        return Calendars.fromDate(dataOcorreu);
    }

    @Override
    public Calendar registeredAt() {
        return Calendars.fromDate(dataRegistada);
    }

}
