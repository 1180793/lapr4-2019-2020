package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.produto.domain.Produto;
import eapli.framework.domain.model.ValueObject;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * Classe desvio representa a diferença de uma determinada quantidade produto/materia prima que iamos usar com o seu consumo real
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
public class Desvio implements ValueObject {
    
    private static final long serialVersionUID = 1L;

    private Double unidadesDif;
    
    @ManyToOne(targetEntity = Produto.class)
    private Produto produtoFabricado;

    @ElementCollection
    @CollectionTable(name = "DesvioComponentes")
    private List<DesvioComponentes> lstDesvioComponentes;

    /**
     * Construtor completo do desvio
     *
     * @param produtoFabricado
     * @param unidadesDif
     */
    public Desvio(Produto produtoFabricado, Double unidadesDif) {
        this.produtoFabricado = produtoFabricado;
        this.unidadesDif = unidadesDif;
        this.lstDesvioComponentes = new ArrayList<>();
    }

    /**
     * Construtor vazio para o ORM
     */
    protected Desvio() {
        //for ORM
    }
    
    public Produto produtoFabricado() {
        return this.produtoFabricado;
    }

    public Double diferencaQuantidade() {
        return this.unidadesDif;
    }
    
    public List<DesvioComponentes> desvioComponentes() {
        return this.lstDesvioComponentes;
    }
    
    public void adicionarDesvioComponente(DesvioComponentes desvioComp) {
        this.lstDesvioComponentes.add(desvioComp);
    }
}
