package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.fichaproducao.domain.Material;
import eapli.framework.domain.model.ValueObject;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Embeddable
public class DesvioComponentes implements ValueObject {
    
    @ManyToOne(targetEntity = Material.class)
    private Material material;
    
    private Double quantidadeDif;
    
    public DesvioComponentes(Material material, Double quantidadeDif) {
        this.material = material;
        this.quantidadeDif = quantidadeDif;
    }
    
    protected DesvioComponentes() {
        // ORM
    }
    
    public Material material() {
        return this.material;
    }
    
    public Double quantidade() {
        return this.quantidadeDif;
    }
    
}
