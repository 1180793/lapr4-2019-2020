/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.execucaoOrdemProducao.domain;

import eapli.framework.domain.model.ValueObject;
import eapli.framework.strings.util.StringPredicates;
import javax.persistence.*;

/**
 *
 * @author David
 */
@Embeddable
public class CodInternoLote implements ValueObject, Comparable<CodInternoLote> {

    private static final long serialVersionUID = 1L;

    private String code;

    /**
     * Contrutor completo.
     *
     * @param code
     */
    public CodInternoLote(final String code) {
        if (StringPredicates.isNullOrEmpty(code)) {
            throw new IllegalArgumentException(
                    "Codigo Interno nao deve ser nulo nem vazio");
        }

        this.code = code;
    }

    /**
     * Contrutor vazio(ORM).
     */
    protected CodInternoLote() {
        this.code = null;
    }

    /**
     * Método que devolve uma instancia de CódigoInternoLote apartir de uma
     * String.
     *
     * @param code
     * @return
     */
    public static CodInternoLote valueOf(final String code) {
        return new CodInternoLote(code);
    }

    public String identity() {
        return this.code;
    }

    /**
     * Método equals reescrito.
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CodInternoLote)) {
            return false;
        }

        final CodInternoLote that = (CodInternoLote) o;
        return this.code.equals(that.code);
    }

    /**
     * Método hashCode reescrito.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return this.code.hashCode();
    }

    /**
     * Método toString reescrito.
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.code;
    }

    /**
     * Método compareTo reescrito.
     *
     * @param o
     * @return int
     */
    @Override
    public int compareTo(CodInternoLote o) {
        return code.compareTo(o.code);
    }

}
