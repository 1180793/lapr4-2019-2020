/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.execucaoOrdemProducao.repositories;

import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.framework.domain.repositories.DomainRepository;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public interface ExecucaoOrdemProducaoRepository  extends DomainRepository<CodInternoOrdemDeProducao, ExecucaoOrdemProducao> {

    ExecucaoOrdemProducao findByOrdem(final CodInternoOrdemDeProducao who);

}
