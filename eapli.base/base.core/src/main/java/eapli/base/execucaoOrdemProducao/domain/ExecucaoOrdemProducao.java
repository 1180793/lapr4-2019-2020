package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.fichaproducao.domain.Componente;
import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.base.fichaproducao.domain.Material;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.utils.Utils;
import eapli.base.utils.Utils.OrderAtividadeMaquina;
import eapli.framework.domain.model.AggregateRoot;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Entity
@Table(
        name = "ExecucaoOrdemProducao",
        uniqueConstraints = {
            @UniqueConstraint(columnNames = "codInterno")
        }
)
public class ExecucaoOrdemProducao implements AggregateRoot<CodInternoOrdemDeProducao> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long pk;
    @Version
    private Long version;

    @OneToOne
    private OrdemProducao ordem;

    @OneToOne
    private LinhaProducao linha;

    @AttributeOverride(name = "code", column = @Column(unique = true, nullable = false, name = "codInterno"))
    private CodInternoOrdemDeProducao codInterno;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date horaInicio;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date horaFim;

    private Boolean isActive;

    @ElementCollection
    @CollectionTable(name = "EventosMaquina")
    private List<AtividadeMaquina> eventosMaquina;

    @ElementCollection
    @CollectionTable(name = "Estorno")
    private List<Estorno> lstEstorno;

    @ElementCollection
    @CollectionTable(name = "Consumo")
    private List<Consumo> lstConsumo;

    @ElementCollection
    @CollectionTable(name = "EntregaProducao")
    private List<EntregaProducao> lstEntregaProducao;

    @ElementCollection
    @CollectionTable(name = "DetalhesTempo")
    private List<DetalhesTempo> lstDetalhesTempo;

    private Desvio desvio;

    private Duration tempoBruto;

    private Duration tempoEfetivo;

    @OneToMany(mappedBy = "execucaoOrdem", cascade = CascadeType.ALL)
    private List<Lote> lstLotes;

    public ExecucaoOrdemProducao(OrdemProducao ordem, Date dataInicio, LinhaProducao linha) {
        this.ordem = ordem;
        this.linha = linha;
        this.codInterno = ordem.identity();
        this.horaInicio = dataInicio;
        this.isActive = true;
        this.eventosMaquina = new ArrayList<>();
        this.lstEstorno = new LinkedList<>();
        this.lstConsumo = new LinkedList<>();
        this.lstLotes = new ArrayList<>();
        this.lstEntregaProducao = new LinkedList<>();
        this.lstDetalhesTempo = new ArrayList<>();
    }

    /**
     * Construtor vazio para o ORM
     */
    public ExecucaoOrdemProducao() {

    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof ExecucaoOrdemProducao)) {
            return false;
        }
        final ExecucaoOrdemProducao that = (ExecucaoOrdemProducao) other;
        if (this == that) {
            return true;
        }
        return identity().equals(that.identity());
    }

    @Override
    public CodInternoOrdemDeProducao identity() {
        return this.codInterno;
    }

    public OrdemProducao ordemProducao() {
        return this.ordem;
    }
    
    public LinhaProducao linha() {
        return this.linha;
    }
    
    public List<Lote> lotes() {
        return this.lstLotes;
    }
    
    public List<Consumo> consumos() {
        return this.lstConsumo;
    }
    
    public List<Estorno> estornos() {
        return this.lstEstorno;
    }
    
    public List<EntregaProducao> entregasProducao() {
        return this.lstEntregaProducao;
    }
    
    public List<DetalhesTempo> tempos() {
        return this.lstDetalhesTempo;
    }
    
    public List<AtividadeMaquina> atividadesMaquina() {
        return this.eventosMaquina;
    }
    
    public Desvio desvio() {
        return this.desvio;
    }
    
    public Date horaInicio() {
        return this.horaInicio;
    }
    
    public Date horaFim() {
        return this.horaFim;
    }
    
    public Duration tempoBruto() {
        return this.tempoBruto;
    }
    
    public Duration tempoEfetivo() {
        return this.tempoEfetivo;
    }

    public void finalizarExecucaoOrdemProducao(Date dataFim) {
        this.isActive = false;
        this.horaFim = dataFim;
        this.calcularDesvio();
        this.calcularDetalhesTempo();
        Utils.exportarOrdemProducao(this);
    }

    public void registarEventoMaquina(AtividadeMaquina evento) {
        this.eventosMaquina.add(evento);
    }

    public void registarConsumo(Consumo consumo) {
        this.lstConsumo.add(consumo);
    }

    public void registarEstorno(Estorno estorno) {
        this.lstEstorno.add(estorno);
    }

    public void registarProducao(Lote lote) {
        int index = lstLotes.indexOf(lote);
        if (index != -1) {
            lstLotes.get(index).adicionarUnidades(lote.unidades());
        } else {
            lote.associarExecucaoOrdemProducao(this);
            this.lstLotes.add(lote);
        }
    }

    public void registarEntregaProducao(EntregaProducao entrega) {
        this.lstEntregaProducao.add(entrega);
    }

    private void calcularDesvio() {
        FichaProducao ficha = this.ordem.produto().fichaProducao();
        HashMap<Material, Double> mapConsumos = new HashMap<>();
        Double unidadesFabricadas = 0.0;
        for (Lote lote : lstLotes) {
            unidadesFabricadas += lote.unidades();
        }

        Double unidadesDif = unidadesFabricadas - ordem.quantidade().quantidadeProduto();

        Desvio newDesvio = new Desvio(this.ordem.produto(), unidadesDif);

        for (Consumo consumo : lstConsumo) {
            if (mapConsumos.containsKey(consumo.material())) {

                mapConsumos.put(consumo.material(), mapConsumos.get(consumo.material()) + consumo.valorConsumo());
            } else {
                mapConsumos.put(consumo.material(), consumo.valorConsumo());
            }
        }
        for (Material mat : mapConsumos.keySet()) {
            Componente componente = findComponente(mat, ficha.componentes());
            Double realComponenteQuantidade = (componente.quantidade() * this.ordem.quantidade().quantidadeProduto()) / ficha.quantidadeProdutoFinal();
            Double componenteDif = mapConsumos.get(mat) - realComponenteQuantidade;
            DesvioComponentes desvioComponentes = new DesvioComponentes(componente.material(), componenteDif);
            newDesvio.adicionarDesvioComponente(desvioComponentes);
        }

        this.desvio = newDesvio;
    }

    private void calcularDetalhesTempo() {

        Collections.sort(eventosMaquina, new OrderAtividadeMaquina());

        this.tempoBruto = Utils.subtractDates(horaFim, horaInicio);
        // Iniciar Tempo Efetivo ao Tempo Bruto para Subtrair o tempo de Possiveis Interrupções existentes
        this.tempoEfetivo = Utils.subtractDates(horaFim, horaInicio);

        // Calcular Detalhes Tempo para as Máquinas
        Map<Maquina, Boolean> map = new HashMap<>();

        calculateDetalhesTemposMaquinas(map);
        
        calculateTempoEfetivoOrdem(map);
    }

    private void calculateDetalhesTemposMaquinas(Map<Maquina, Boolean> map) {
        List<Duration> lstParagens = new ArrayList<>();
        for (Maquina maquina : this.linha.maquinas()) {

            map.put(maquina, true);

            Date dateBrutoBegin = null;
            Date dateBrutoFinish = null;
            Date dateParagem = null;
            Date dateRetoma;
            for (AtividadeMaquina eventoMaquina : eventosMaquina) {
                if (eventoMaquina.maquina().sameAs(maquina)) {
                    if (null != eventoMaquina.tipo()) switch (eventoMaquina.tipo()) {
                        case INICIO_ATIVIDADE:
                            dateBrutoBegin = eventoMaquina.data();
                            break;
                        case FIM_ATIVIDADE:
                            dateBrutoFinish = eventoMaquina.data();
                            break;
                        case PARAGEM:
                            dateParagem = eventoMaquina.data();
                            break;
                        case RETOMA_ATIVIDADE:
                            dateRetoma = eventoMaquina.data();
                            Duration duration = Utils.subtractDates(dateRetoma, dateParagem);
                            lstParagens.add(duration);
                            dateParagem = null;
                            break;
                        default:
                            break;
                    }
                }
            }
            Duration tempoBrutoMaquina = Utils.subtractDates(dateBrutoFinish, dateBrutoBegin);
            Duration tempoEfetivoMaquina = tempoBrutoMaquina;
            for (Duration paragem : lstParagens) {
                tempoEfetivoMaquina = tempoEfetivoMaquina.minus(paragem);
            }
            lstParagens.clear();

            DetalhesTempo tempo = new DetalhesTempo(maquina, tempoBrutoMaquina, tempoEfetivoMaquina);
            lstDetalhesTempo.add(tempo);
        }
    }

    private void calculateTempoEfetivoOrdem(Map<Maquina, Boolean> map) {
        Collections.sort(eventosMaquina, new OrderAtividadeMaquina());
        List<Duration> lstParagens = new ArrayList<>();
        Date dateParagem = null;
        Date dateRetoma;
        for (AtividadeMaquina eventoMaquina : eventosMaquina) {
            if (eventoMaquina.tipo() == TipoAtividade.PARAGEM) {
                map.put(eventoMaquina.maquina(), false);
                boolean flag = areTodasMaquinasDesativadas(map);
                if (flag) {
                    dateParagem = eventoMaquina.data();
                }
            } else if (eventoMaquina.tipo() == TipoAtividade.RETOMA_ATIVIDADE) {
                map.put(eventoMaquina.maquina(), true);
                if (dateParagem != null) {
                    dateRetoma = eventoMaquina.data();
                    Duration duration = Utils.subtractDates(dateRetoma, dateParagem);
                    lstParagens.add(duration);
                    dateParagem = null;
                }
            }
        }

        for (Duration paragem : lstParagens) {
            this.tempoEfetivo = this.tempoEfetivo.minus(paragem);
        }
    }

    private boolean areTodasMaquinasDesativadas(Map<Maquina, Boolean> map) {
        for (Maquina maq : map.keySet()) {
            if (map.get(maq) == true) {
                return false;
            }
        }
        return true;
    }

    private Componente findComponente(Material material, Set<Componente> lstComponentes) {
        for (Componente comp : lstComponentes) {
            if (comp.material().id().equals(material.id())) {
                return comp;
            }
        }
        return null;
    }

}
