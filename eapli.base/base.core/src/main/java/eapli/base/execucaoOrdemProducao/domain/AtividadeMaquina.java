package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.linhaproducao.domain.Maquina;
import eapli.framework.domain.events.DomainEvent;
import eapli.framework.time.util.Calendars;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
@Embeddable
public class AtividadeMaquina implements DomainEvent {

    @ManyToOne(targetEntity = Maquina.class)
    private Maquina maquina;
    
    private TipoAtividade tipo;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataOcorreu;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataRegistada;
    
    public AtividadeMaquina(Maquina maquina, TipoAtividade tipo, Date data) {
        this.maquina = maquina;
        this.tipo = tipo;
        this.dataOcorreu = data;
        this.dataRegistada = new Date();
    }
    
    protected AtividadeMaquina() {
        // ORM
    }
    
    public Maquina maquina() {
        return this.maquina;
    }
    
    public TipoAtividade tipo() {
        return this.tipo;
    }
    
    public Date data() {
        return this.dataOcorreu;
    }

    @Override
    public Calendar occurredAt() {
        return Calendars.fromDate(dataOcorreu);
    }

    @Override
    public Calendar registeredAt() {
        return Calendars.fromDate(dataRegistada);
    }
    
}
