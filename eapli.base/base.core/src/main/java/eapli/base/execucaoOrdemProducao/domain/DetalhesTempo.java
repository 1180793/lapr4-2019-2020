/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.execucaoOrdemProducao.domain;

import eapli.base.linhaproducao.domain.Maquina;
import eapli.framework.domain.model.ValueObject;
import java.time.Duration;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * Classe que representa os detalhes de tempo da execuçao duma ordem de produçao: tempo efetivo e tempo bruto de execuçao.
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
@Embeddable
public class DetalhesTempo implements ValueObject {

    @ManyToOne(targetEntity = Maquina.class)
    private Maquina maquina;

    private Duration tempoBruto;

    private Duration tempoEfetivo;

    /**
     * Construtor completo do desvio
     *
     * @param maquina
     * @param tempoEfetivo
     * @param tempoBruto
     */
    public DetalhesTempo(Maquina maquina, Duration tempoBruto, Duration tempoEfetivo) {
        this.maquina = maquina;
        this.tempoBruto = tempoBruto;
        this.tempoEfetivo = tempoEfetivo;
    }

    /**
     * Construtor vazio para o ORM
     */
    protected DetalhesTempo() {

        //for ORM
    }

    /**
     * Método toString reescrito.
     *
     * @return String
     */
    @Override
    public String toString() {
        return String.format("Tempo Bruto:%f minutos\nTempo Efetivo:%f minutos", tempoBruto, tempoEfetivo);
    }
    
    public Maquina maquina() {
        return this.maquina;
    }
    
    public Duration tempoBruto() {
        return this.tempoBruto;
    }
    
    public Duration tempoEfetivo() {
        return this.tempoEfetivo;
    }

}
