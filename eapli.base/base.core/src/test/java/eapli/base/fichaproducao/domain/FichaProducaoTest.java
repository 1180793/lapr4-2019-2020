/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import eapli.base.produto.domain.CodComercial;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.framework.general.domain.model.Description;
import eapli.framework.general.domain.model.Designation;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author PC
 */
public class FichaProducaoTest {
    
    FichaProducao roscas;
    Produto quim;
    Produto componente;
    
    @Before
    public void setUp() throws Exception {
        final CodFabrico codFabrico = new CodFabrico("7023428");
        final CodComercial codComercial = new CodComercial("320296000094");
        final Description descricaoBreve = Description.valueOf("Fl.Prens.Discos Vinil");
        final Description descricaoCompleta = Description.valueOf("Fl.Prens.Discos Vinil - Completa");
        final Designation unidade = Designation.valueOf("UN");
        final Designation categoriaProduto = Designation.valueOf("SA-VN");
        componente = new Produto(codFabrico, codComercial, descricaoBreve, descricaoCompleta, unidade, categoriaProduto);
        Componente comp = new Componente(componente, 0.1);
        
        final CodFabrico codFabrico1 = new CodFabrico("7023420");
        final CodComercial codComercial1 = new CodComercial("320296000095");
        final Description descricaoBreve1 = Description.valueOf("Fl.Prens.Discos Vinil - 2");
        final Description descricaoCompleta1 = Description.valueOf("Fl.Prens.Discos Vinil - 2 - Completa");
        quim = new Produto(codFabrico1, codComercial1, descricaoBreve1, descricaoCompleta1, unidade, categoriaProduto);
        
        Set<Componente> componentes = new HashSet<>();
        componentes.add(comp);
        roscas = new FichaProducao(quim,componentes,10);
    }


    /**
     * Test of produto method, of class FichaProducao.
     */
    @Test
    public void testProduto() {
        System.out.println("getProdFinalTeste");
        Produto result = roscas.produto();
        assertEquals(quim, result);
    }

    /**
     * Test of identity method, of class FichaProducao.
     */
    @Test
    public void testIdentity() {
        System.out.println("identity");
        CodFabrico expResult = new CodFabrico("7023420");
        CodFabrico result = roscas.identity();
        assertEquals(expResult, result);
    }
}
