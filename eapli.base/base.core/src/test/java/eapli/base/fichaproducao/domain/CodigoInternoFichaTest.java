/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC
 */
public class CodigoInternoFichaTest {
    
    /**
     * Test of valueOf method, of class CodigoInternoFicha.
     */
    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        String codigoInternoFicha = "asd89";
        CodigoInternoFicha expResult = new CodigoInternoFicha("asd89");
        CodigoInternoFicha result = CodigoInternoFicha.valueOf(codigoInternoFicha);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class CodigoInternoFicha.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
   
        CodigoInternoFicha instance = new CodigoInternoFicha("asd89");
        CodigoInternoFicha instanceq = new CodigoInternoFicha("asd89");
        boolean expResult = true;
        boolean result = instance.equals(instanceq);
        
        CodigoInternoFicha instance1 = new CodigoInternoFicha("jk");
        CodigoInternoFicha instanceq1 = new CodigoInternoFicha("asd89");
        boolean expResult1 = false;
        boolean result1 = instance1.equals(instanceq1);
        
        assertEquals(expResult1, result1);
        assertEquals(expResult, result);
    }
}
