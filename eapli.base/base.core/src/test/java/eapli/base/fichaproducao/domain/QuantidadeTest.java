/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.fichaproducao.domain;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC
 */
public class QuantidadeTest {

    /**
     * Test of equals method, of class Quantidade.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Quantidade instanceq = new Quantidade(1.0);
        Quantidade instance = new Quantidade(1.0);
        boolean expResult = true;
        boolean result = instance.equals(instanceq);
        
        Quantidade instanceq1 = new Quantidade(2.0);
        Quantidade instance1 = new Quantidade(1.0);
        boolean expResult1 = false;
        boolean result1 = instance1.equals(instanceq1);
        
        assertEquals(expResult1, result1);
        assertEquals(expResult, result);
    }
    
}
