/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class MaquinaTest {
    
    private static final CodigoInternoMaquina COD_INTERNO_MAQUINA = CodigoInternoMaquina.valueOf("1-1");
    private static final String NUM_SERIE = String.valueOf("11111111");
    private static final String DESCRICAO = String.valueOf("maquina 1 da linha 1");
    private static final Calendar DATA_INSTALACAO = getCalendarInstance("2000-11-1");
    private static final String MARCA = String.valueOf("Leipzig");
    private static final String MODELO = String.valueOf("1.9");
    
    @Test
    public void ensureMaquinaWithCodInternoNumSerieDescDatMarcaModelo() {
        new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, DESCRICAO, DATA_INSTALACAO, MARCA, MODELO);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithCodInteroMaquinaNotNull() {
        new Maquina(null, NUM_SERIE, DESCRICAO, DATA_INSTALACAO, MARCA, MODELO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithNumSerieNotNull() {
        new Maquina(COD_INTERNO_MAQUINA, null, DESCRICAO, DATA_INSTALACAO, MARCA, MODELO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithDescricaoNotNull() {
        new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, null, DATA_INSTALACAO, MARCA, MODELO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithDataInstalacaoNotNull() {
        new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, DESCRICAO, null, MARCA, MODELO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithMarcaNotNull() {
        new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, DESCRICAO, DATA_INSTALACAO, null, MODELO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMaquinaWithCodInteroModeloNull() {
        new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, DESCRICAO, DATA_INSTALACAO, MARCA, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeLinhaProdToNull() {
        System.out.println("Change Descrição - New Descricao info must not be null");

        final Maquina subject = new Maquina(COD_INTERNO_MAQUINA, NUM_SERIE, DESCRICAO, DATA_INSTALACAO, MARCA, MODELO);
        subject.definirLinhaProducao(null);
    }
    
    private static Calendar getCalendarInstance(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar dataInstalacao = Calendar.getInstance();
            dataInstalacao.setTime(sdf.parse(strDate));
            return dataInstalacao;
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(MaquinaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
