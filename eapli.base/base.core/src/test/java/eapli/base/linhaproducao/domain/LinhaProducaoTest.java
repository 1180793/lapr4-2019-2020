/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class LinhaProducaoTest {
    
    private static final Identificador IDENTIFICADOR = Identificador.valueOf("SQ20");
    
    @Test
    public void ensureLinhaProducaoWithIdentificador() {
        new LinhaProducao(IDENTIFICADOR);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureLinhaProducaoWithIdentificadorNotNull() {
        new LinhaProducao(null);
    }
    
}
