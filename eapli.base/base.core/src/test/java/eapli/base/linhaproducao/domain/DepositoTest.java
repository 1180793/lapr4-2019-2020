/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.linhaproducao.domain;

import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.domain.CodigoInternoDeposito;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class DepositoTest {
    
    
    private static final CodigoInternoDeposito COD_INTERNO_MAQUINA = CodigoInternoDeposito.valueOf("D1");
    private static final String DESCRICAO = String.valueOf("DEPÓSITO 1");
    
    @Test
    public void ensureDepositoWithCodInternoDescricao() {
        new Deposito(COD_INTERNO_MAQUINA, DESCRICAO);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDepositoWithCodInteroNotNull() {
        new Deposito(null, DESCRICAO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDepositoWithDescricaoNotNull() {
        new Deposito(COD_INTERNO_MAQUINA, null);
    }
    
    
}
