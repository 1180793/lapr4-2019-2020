/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.produto.domain;

import eapli.base.fichaproducao.domain.FichaProducao;
import eapli.framework.general.domain.model.Description;
import eapli.framework.general.domain.model.Designation;
import java.util.HashSet;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class ProdutoTest {
    
    private static final CodFabrico COD_FABRICO = CodFabrico.valueOf("4500019");
    private static final CodComercial COD_COMERCIAL = CodComercial.valueOf("320080000192");
    private static final Description DESCRICAO_BREVE = Description.valueOf("Folhas de Rolos");
    private static final Description DESCRICAO_COMPLETA = Description.valueOf("Folhas de Rolos - 8248");
    private static final Designation UNIDADE = Designation.valueOf("UN");
    private static final Designation CATEGORIA_PRODUTO = Designation.valueOf("PA-FR");  
    
    @Test
    public void ensureProdWithCodFabCodComDescBrevDescCompUniCatProd() {
        new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, UNIDADE, CATEGORIA_PRODUTO);
        assertTrue(true);
    }
    
    private static final Produto PROD_TEST = new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, UNIDADE, CATEGORIA_PRODUTO);
    private static final FichaProducao FICHA_PROD = new FichaProducao(PROD_TEST, new HashSet<>(), 0.0);
    
    @Test
    public void ensureProdWithCodFabCodComDescBrevDescCompFichaProdUniCatProd() {
        new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, FICHA_PROD, UNIDADE, CATEGORIA_PRODUTO);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureProdWithFichaProdNotNull() {
        new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, null, UNIDADE, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveCodFabrico() {
       new Produto(null, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, UNIDADE, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveCodComercial() {
       new Produto(COD_FABRICO, null, DESCRICAO_BREVE, DESCRICAO_COMPLETA, UNIDADE, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveDescBreve() {
       new Produto(COD_FABRICO, COD_COMERCIAL, null, DESCRICAO_COMPLETA, UNIDADE, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveDescCompleta() {
       new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, null, UNIDADE, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveUnidade() {
       new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, null, CATEGORIA_PRODUTO);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMustHaveCategoriaProd() {
       new Produto(COD_FABRICO, COD_COMERCIAL, DESCRICAO_BREVE, DESCRICAO_COMPLETA, UNIDADE, null);
    }
    
    
    
    
}
