/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class CategoriaMateriasPrimasTest {
    
    private static final String COD_ALFA_NUMERICO = String.valueOf("MET");
    private static final String DESCRICAO = String.valueOf("METAL");
    
    
    @Test
    public void ensureCategoriaMateriaPrimaWithCodAlfaNumDesc() {
        new CategoriaMateriaPrima(COD_ALFA_NUMERICO, DESCRICAO);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeDescricaoToNull() {
        System.out.println("Change Descrição - New Descricao info must not be null");

        final CategoriaMateriaPrima subject = new CategoriaMateriaPrima(COD_ALFA_NUMERICO, DESCRICAO);
        subject.changeDescricaoTo(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeDescricaoToEmpty() {
        System.out.println("Change Descrição - New Descricao info must not be empty");

        final CategoriaMateriaPrima subject = new CategoriaMateriaPrima(COD_ALFA_NUMERICO, DESCRICAO);
        subject.changeDescricaoTo("");
    }
    
}
