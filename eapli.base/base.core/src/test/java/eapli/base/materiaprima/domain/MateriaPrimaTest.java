/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.materiaprima.domain;

import eapli.framework.general.domain.model.Description;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author ZDPessoa
 */
public class MateriaPrimaTest {
    
    private static final Description DESCRICAO = Description.valueOf("Ferro");
    private static final CodigoInterno COD_INTERNO = CodigoInterno.valueOf("A7123");
    private static final FichaTecnica FICHA_TECNICA = FichaTecnica.valueOf("Ficha1", "src/fichatecnica1.pdf");
    private static final CategoriaMateriaPrima CATEGORIA = new CategoriaMateriaPrima("MET", "METAL");
    
    @Test
    public void ensureMateriaPrimaWithDescCodInternoFichaTecCat() {
        new MateriaPrima(DESCRICAO, COD_INTERNO, FICHA_TECNICA, CATEGORIA);
        assertTrue(true);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMateriaPrimaWithDescricaoNotNull() {
        new MateriaPrima(null, COD_INTERNO, FICHA_TECNICA, CATEGORIA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMateriaPrimaWithCodigoInternoNotNull() {
        new MateriaPrima(DESCRICAO, null, FICHA_TECNICA, CATEGORIA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMateriaPrimaWithFichaTecnicaNotNull() {
        new MateriaPrima(DESCRICAO, COD_INTERNO, null, CATEGORIA);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMateriaPrimaWithCategoriaNotNull() {
        new MateriaPrima(DESCRICAO, COD_INTERNO, FICHA_TECNICA, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeCategoriaToNull() {
        System.out.println("Change Categoria de Materia Prima - New Categoria info must not be null");

        final MateriaPrima subject = new MateriaPrima(DESCRICAO, COD_INTERNO, FICHA_TECNICA, CATEGORIA);
        assertFalse(subject.changeCategoria(null));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeFichaTecnicaToNull() {
        System.out.println("Change Ficha Técnica - New FichaTecnica info must not be null");

        final MateriaPrima subject = new MateriaPrima(DESCRICAO, COD_INTERNO, FICHA_TECNICA, CATEGORIA);
        subject.changeFichaTecnica(null);
    }
}
