/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.linhaproducao.application.AdicionarMaquinaController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author PC
 */
public class MaquinasBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(MaquinasBootstrapper.class);

    @Override
    public boolean execute() {
        register("1M1", "19183740", "maquina 1 da linha 1", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_1, 1);
        register("1M2", "11992831", "maquina 2 da linha 1", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_1, 2);
        register("1M3", "18293841", "maquina 3 da linha 1", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_1, 3);

        register("2M1", "49141924", "maquina 1 da linha 2", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_2, 1);
        register("2M2", "51423491", "maquina 2 da linha 2", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_2, 2);
        register("2M3", "69182346", "maquina 3 da linha 2", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_2, 3);

        register("3M1", "71111111", "maquina 1 da linha 3", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_3, 1);
        register("3M2", "81111111", "maquina 2 da linha 3", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_3, 2);
        register("3M3", "91111111", "maquina 3 da linha 3", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_3, 3);
        
        register("T3", "00000001", "Transformadora/Cortadora", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_4, 1);
        register("DD4", "00000002", "Transportadora", "2000-11-1", "Leipzig", "1.9", TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_4, 2);

        return true;
    }

    private void register(String codInterno, String numSerie, String descricao, String dataInstalacao, String Marca, String Modelo, String lp, Integer posicao) {

        final AdicionarMaquinaController controller = new AdicionarMaquinaController();

        try {
            controller.adicionarMaquina(codInterno, numSerie, descricao, dataInstalacao, Marca, Modelo, lp, posicao);
        } catch (final IntegrityViolationException | ConcurrencyException e) {
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codInterno);
            LOGGER.trace("Assuming existing record", e);
        }
    }

    private Calendar getCalendarInstance(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar dataInstalacao = Calendar.getInstance();
            dataInstalacao.setTime(sdf.parse(strDate));
            return dataInstalacao;
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(MaquinasBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
