package eapli.base.infrastructure.bootstrapers;

import java.util.Calendar;

import eapli.framework.time.util.Calendars;

public final class TestDataConstants {

    /**
     * Bootstrap Categorias de Matéria-Prima
     */
    public static final String CATEGORIA_MATERIA_PRIMA_METAL = "MET";
    public static final String CATEGORIA_MATERIA_PRIMA_CORTICA = "COR";
    public static final String CATEGORIA_MATERIA_PRIMA_MADEIRA = "MAD";

    /**
     * Bootstrap Produtos
     */
    public static final String PRODUTO_COD_FABRICO_1 = "70234285";
    public static final String PRODUTO_COD_COMERCIAL_1 = "320296000094";
    public static final String PRODUTO_DESC_BREVE_1 = "Fl.Prens.Discos Vinil";
    public static final String PRODUTO_DESC_COMPLETA_1 = "Fl.Prens.Discos Vinil - Completa";
    public static final String PRODUTO_UNIDADE_1 = "UN";
    public static final String PRODUTO_CATEGORIA_1 = "SA-VN";

    public static final String PRODUTO_COD_FABRICO_2 = "70234203";
    public static final String PRODUTO_COD_COMERCIAL_2 = "320296000095";
    public static final String PRODUTO_DESC_BREVE_2 = "Fl.Prens.Discos Vinil 2";
    public static final String PRODUTO_DESC_COMPLETA_2 = "Fl.Prens.Discos Vinil 2 - Completa";
    public static final String PRODUTO_UNIDADE_2 = "UN";
    public static final String PRODUTO_CATEGORIA_2 = "SA-VN";

    public static final String PRODUTO_COD_FABRICO_3 = "70234291";
    public static final String PRODUTO_COD_COMERCIAL_3 = "320296078092";
    public static final String PRODUTO_DESC_BREVE_3 = "Fl.Prens.Discos Vinil 3";
    public static final String PRODUTO_DESC_COMPLETA_3 = "Fl.Prens.Discos Vinil 3 - Completa";
    public static final String PRODUTO_UNIDADE_3 = "UN";
    public static final String PRODUTO_CATEGORIA_3 = "SA-VN";

    public static final String PRODUTO_COD_FABRICO_4 = "50000106";
    public static final String PRODUTO_COD_COMERCIAL_4 = "320296078321";
    public static final String PRODUTO_DESC_BREVE_4 = "Placas de Madeira";
    public static final String PRODUTO_DESC_COMPLETA_4 = "Placas de Madeira";
    public static final String PRODUTO_UNIDADE_4 = "UN";
    public static final String PRODUTO_CATEGORIA_4 = "SA-VN";

    public static final String PRODUTO_COD_FABRICO_5 = "41000651";
    public static final String PRODUTO_COD_COMERCIAL_5 = "320223378332";
    public static final String PRODUTO_DESC_BREVE_5 = "Restos de Madeira";
    public static final String PRODUTO_DESC_COMPLETA_5 = "Restos de Madeira (provenientes de outras produções)";
    public static final String PRODUTO_UNIDADE_5 = "UN";
    public static final String PRODUTO_CATEGORIA_5 = "SA-VN";

    /**
     * Bootstrap Linhas de Produção
     */
    public static final String IDENTIFICADOR_LINHA_PRODUCAO_1 = "LP124";
    public static final String IDENTIFICADOR_LINHA_PRODUCAO_2 = "LP203";
    public static final String IDENTIFICADOR_LINHA_PRODUCAO_3 = "LP181";
    public static final String IDENTIFICADOR_LINHA_PRODUCAO_4 = "LP004";

    /**
     * Bootstrap Depósitos
     */
    public static final String DEPOSITO_1 = "L001";
    public static final String DEPOSITO_2 = "L002";
    public static final String DEPOSITO_3 = "L003";
    public static final String DEPOSITO_4 = "L040";
    public static final String DEPOSITO_5 = "L042";
    public static final String DEPOSITO_6 = "L030";

    public static final String DEPOSITO_DESC_1 = "DEPÓSITO 1";
    public static final String DEPOSITO_DESC_2 = "DEPÓSITO 2";
    public static final String DEPOSITO_DESC_3 = "DEPÓSITO 3";
    public static final String DEPOSITO_DESC_4 = "DEPÓSITO 40";
    public static final String DEPOSITO_DESC_5 = "DEPÓSITO 42";
    public static final String DEPOSITO_DESC_6 = "DEPÓSITO 30";

// ---------------------------------------------------------------------------------------------------------------------------------
    public static final String USER_TEST1 = "user1";

    @SuppressWarnings("squid:S2068")
    public static final String PASSWORD1 = "Password1";

    @SuppressWarnings("squid:S2885")
    public static final Calendar DATE_TO_BOOK = Calendars.of(2017, 12, 01);

    private TestDataConstants() {
        // ensure utility
    }
}
