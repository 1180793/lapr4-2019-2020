/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.produto.application.AdicionarProdutoController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProdutosBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(CategoriaMateriaPrimaBootstrapper.class);

    @Override
    public boolean execute() {
        register(TestDataConstants.PRODUTO_COD_FABRICO_1, TestDataConstants.PRODUTO_COD_COMERCIAL_1, TestDataConstants.PRODUTO_DESC_BREVE_1, TestDataConstants.PRODUTO_DESC_COMPLETA_1, TestDataConstants.PRODUTO_UNIDADE_1, TestDataConstants.PRODUTO_CATEGORIA_1);
        register(TestDataConstants.PRODUTO_COD_FABRICO_2, TestDataConstants.PRODUTO_COD_COMERCIAL_2, TestDataConstants.PRODUTO_DESC_BREVE_2, TestDataConstants.PRODUTO_DESC_COMPLETA_2, TestDataConstants.PRODUTO_UNIDADE_2, TestDataConstants.PRODUTO_CATEGORIA_2);
        register(TestDataConstants.PRODUTO_COD_FABRICO_3, TestDataConstants.PRODUTO_COD_COMERCIAL_3, TestDataConstants.PRODUTO_DESC_BREVE_3, TestDataConstants.PRODUTO_DESC_COMPLETA_3, TestDataConstants.PRODUTO_UNIDADE_3, TestDataConstants.PRODUTO_CATEGORIA_3);
        register(TestDataConstants.PRODUTO_COD_FABRICO_5, TestDataConstants.PRODUTO_COD_COMERCIAL_5, TestDataConstants.PRODUTO_DESC_BREVE_5, TestDataConstants.PRODUTO_DESC_COMPLETA_5, TestDataConstants.PRODUTO_UNIDADE_5, TestDataConstants.PRODUTO_CATEGORIA_5);
        
        return true;
    }

    /**
     *
     */
    private void register(final String codFabrico, final String codComercial, final String descricaoBreve, final String descricaoCompleta, final String unidade, final String categoriaProduto) {
        final AdicionarProdutoController theController = new AdicionarProdutoController();
        try {
            theController.adicionarProduto(codFabrico, codComercial, descricaoBreve, descricaoCompleta, unidade, categoriaProduto);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codFabrico);
            LOGGER.trace("Assuming existing record", ex);
        }
    }
}
