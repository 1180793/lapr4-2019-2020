/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.materiaprima.application.AdicionarMateriaPrimaController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class MateriaPrimaBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(CategoriaMateriaPrimaBootstrapper.class);

    @Override
    public boolean execute() {
        register("32000139", "Ferro", "dados_teste\\fichatecnica1.pdf", TestDataConstants.CATEGORIA_MATERIA_PRIMA_METAL);
        register("32000140", "Placa Cortiça", "dados_teste\\fichatecnica2.pdf", TestDataConstants.CATEGORIA_MATERIA_PRIMA_CORTICA);
        register("32000141", "Tronco Pinheiro", "dados_teste\\fichatecnica3.pdf", TestDataConstants.CATEGORIA_MATERIA_PRIMA_MADEIRA);
        register("32000142", "Teste Matéria Prima", "dados_teste\\fichatecnicateste.pdf", TestDataConstants.CATEGORIA_MATERIA_PRIMA_CORTICA);
        return true;
    }

    /**
     *
     */
    private void register(final String codInterno, final String descricao,
            final String ftPath, final String codCategoria) {
        final AdicionarMateriaPrimaController theController = new AdicionarMateriaPrimaController();
        try {
            theController.adicionarMateriaPrima(codInterno, descricao, ftPath, codCategoria);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codInterno);
            LOGGER.trace("Assuming existing record", ex);
        }
    }

}
