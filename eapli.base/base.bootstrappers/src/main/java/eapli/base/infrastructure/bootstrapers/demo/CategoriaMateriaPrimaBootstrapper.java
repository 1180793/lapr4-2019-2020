/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.materiaprima.application.EspecificarCategoriaMateriaPrimaController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class CategoriaMateriaPrimaBootstrapper implements Action {
    private static final Logger LOGGER = LogManager.getLogger(CategoriaMateriaPrimaBootstrapper.class);

    @Override
    public boolean execute() {
        register(TestDataConstants.CATEGORIA_MATERIA_PRIMA_METAL, "Metal");
        register(TestDataConstants.CATEGORIA_MATERIA_PRIMA_CORTICA, "Cortiça");
        register(TestDataConstants.CATEGORIA_MATERIA_PRIMA_MADEIRA, "Madeira");
        return true;
    }

    /**
     *
     */
    private void register(final String codigo, final String descricao) {
        final EspecificarCategoriaMateriaPrimaController theController = new EspecificarCategoriaMateriaPrimaController();
        try {
            theController.registerCategoriaMateriaPrima(codigo, descricao);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codigo);
            LOGGER.trace("Assuming existing record", ex);
        }
    }
}
