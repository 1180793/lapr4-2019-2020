/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.deposito.application.EspecificarDepositoController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author David
 */
public class DepositoBootstrapper implements Action{
     private static final Logger LOGGER = LogManager.getLogger(DepositoBootstrapper.class);
    
    @Override
    public boolean execute() {
        register(TestDataConstants.DEPOSITO_1, TestDataConstants.DEPOSITO_DESC_1);
        register(TestDataConstants.DEPOSITO_2, TestDataConstants.DEPOSITO_DESC_2);
        register(TestDataConstants.DEPOSITO_3, TestDataConstants.DEPOSITO_DESC_3);
        register(TestDataConstants.DEPOSITO_4, TestDataConstants.DEPOSITO_DESC_4);
        register(TestDataConstants.DEPOSITO_5, TestDataConstants.DEPOSITO_DESC_5);
        register(TestDataConstants.DEPOSITO_6, TestDataConstants.DEPOSITO_DESC_6);
        return true;
    }

    /**
     *
     */
    private void register(final String codigo, final String descricao) {
        final EspecificarDepositoController theController = new EspecificarDepositoController();
        try {
            theController.addDeposito(codigo, descricao);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codigo);
            LOGGER.trace("Assuming existing record", ex);
        }
    }
}
