/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.linhaproducao.application.EspecificarLinhaProducaoController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ZDPessoa
 */
public class LinhaProducaoBootstrapper implements Action{
    
    private static final Logger LOGGER = LogManager.getLogger(LinhaProducaoBootstrapper.class);
    
    @Override
    public boolean execute() {
        register(TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_1);
        register(TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_2);
        register(TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_3);
        register(TestDataConstants.IDENTIFICADOR_LINHA_PRODUCAO_4);
        return true;
    }
    
    private void register(final String identificador) {
        final EspecificarLinhaProducaoController theController = new EspecificarLinhaProducaoController();
        try {
            theController.adicionarLinhaProducao(identificador);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", identificador);
            LOGGER.trace("Assuming existing record", ex);
        }
    }
    
}
