/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.infrastructure.bootstrapers.demo;

import eapli.base.fichaproducao.application.AddFichaProducaoController;
import eapli.base.infrastructure.bootstrapers.TestDataConstants;
import eapli.base.ordemproducao.application.AddOrdemProducaoController;
import eapli.base.ordemproducao.domain.Encomenda;
import eapli.base.produto.application.AdicionarProdutoController;
import eapli.framework.actions.Action;
import eapli.framework.domain.repositories.ConcurrencyException;
import eapli.framework.domain.repositories.IntegrityViolationException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class OrdemProducaoBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(CategoriaMateriaPrimaBootstrapper.class);

    @Override
    public boolean execute() {
        registerProduto(TestDataConstants.PRODUTO_COD_FABRICO_4, TestDataConstants.PRODUTO_COD_COMERCIAL_4, TestDataConstants.PRODUTO_DESC_BREVE_4, TestDataConstants.PRODUTO_DESC_COMPLETA_4, TestDataConstants.PRODUTO_UNIDADE_4, TestDataConstants.PRODUTO_CATEGORIA_4);
        registerOrdemProducao();
        registerFichaProducao();
        return true;
    }

    private void registerProduto(final String codFabrico, final String codComercial, final String descricaoBreve, final String descricaoCompleta, final String unidade, final String categoriaProduto) {
        final AdicionarProdutoController theController = new AdicionarProdutoController();
        try {
            theController.adicionarProduto(codFabrico, codComercial, descricaoBreve, descricaoCompleta, unidade, categoriaProduto);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codFabrico);
            LOGGER.trace("Assuming existing record", ex);
        }
    }

    /**
     *
     */
    private void registerOrdemProducao() {
        final AddOrdemProducaoController theController = new AddOrdemProducaoController();

        // 100003363;12/05/2020;20200514;50000106;65000;UN;EC2020/00030
        String codInterno = "100003363";
        String dataEmissao = "12/05/2020";
        String dataPrevistaExecucao = "14/05/2020";
        String codigoFabricoProduto = "50000106";
        Double quantidadeProduto = 65000.0;
        String unidade = "UN";
        Set<Encomenda> setEncomendas = new HashSet<>();
        setEncomendas.add(Encomenda.valueOf("EC2020/00030"));

        try {
            theController.adicionarOrdemProducao(codInterno, setEncomendas, dataEmissao, dataPrevistaExecucao, quantidadeProduto, unidade, codigoFabricoProduto);
        } catch (final IntegrityViolationException | ConcurrencyException ex) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            LOGGER.warn("Assuming {} already exists (activate trace log for details)", codInterno);
            LOGGER.trace("Assuming existing record", ex);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(OrdemProducaoBootstrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void registerFichaProducao() {
        AddFichaProducaoController theController = new AddFichaProducaoController();

        String idProdFinal = "50000106";
        Double quantidadeProdFinal = 1000.0;
        theController.setProdutoFinal(idProdFinal, quantidadeProdFinal);

        theController.addComponente("32000142", 24.4);
        theController.addFichaProducao();
    }
}
