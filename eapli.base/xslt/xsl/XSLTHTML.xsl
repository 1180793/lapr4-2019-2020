<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<xsl:output method="html"/>
	<xsl:template match="/ChaoFabrica">
		<html><head><title>Chão de Fábrica
			</title></head><body>
			<xsl:apply-templates select="categoriasMateriaPrima"/>
			<xsl:apply-templates select="materiasPrimas"/>
			<xsl:apply-templates select="produtos"/>
			<xsl:apply-templates select="depositos"/>
			<xsl:apply-templates select="linhasProducao"/>
			<xsl:apply-templates select="ordensProd"/>
		</body></html>
	</xsl:template>
	
<xsl:template match="categoriasMateriaPrima">	
	<h2>Categorias de Materia Prima</h2>
	<xsl:for-each select="categoria">
	<h3>Categoria Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Código: </b> 
	<xsl:value-of select="codAlfaNumerico"/></p1><br></br>
	<p1><b>Descrição: </b>
	<xsl:value-of select="descricao"/></p1>
	</xsl:for-each>
</xsl:template>

<xsl:template match="materiasPrimas">	
	<h2>Materias Primas</h2>
	<xsl:for-each select="materiaPrima">
	<h3>Materia Prima Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Descrição: </b> 
	<xsl:value-of select="descricao/@*"/></p1><br></br>
	<p1><b>Código Interno: </b>
	<xsl:value-of select="codInterno"/></p1><br></br>
	<p1><b>Ficha Técnica: </b>
	<xsl:value-of select="fichaTecnica/@*"/></p1><br></br>
	<p1><b>Categoria: </b>
	<xsl:value-of select="categoria"/></p1>
	</xsl:for-each>
</xsl:template>

<xsl:template match="produtos">	
	<h2>Produtos</h2>
	<xsl:for-each select="produto">
	<h3>Produto Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Código Fabrico: </b> 
	<xsl:value-of select="codFabrico"/></p1><br></br>
	<p1><b>Código Comercial: </b>
	<xsl:value-of select="codComercial/@*"/></p1><br></br>
	<p1><b>Descrição Breve: </b>
	<xsl:value-of select="descricaoBreve/@*"/></p1><br></br>
	<p1><b>Descrição Completa: </b>
	<xsl:value-of select="descricaoCompleta/@*"/></p1><br></br>
	<p1><b>Categoria de Produto: </b>
	<xsl:value-of select="categoriaProduto/@*"/></p1><br></br>
	<xsl:for-each select="fichaProducao/componentes">
		<p1><b>Componente Nº<xsl:value-of select="position()"/>: </b></p1><br></br>
			<p1><b>Material: </b> 
			<xsl:value-of select="material"/></p1><br></br>
			<p1><b>Quantidade: </b>
			<xsl:value-of select="quantidade"/></p1><br></br>
	</xsl:for-each>
	<p1><b>Unidade: </b>
	<xsl:value-of select="unidade/@*"/></p1>
	</xsl:for-each>
</xsl:template>

<xsl:template match="depositos">	
	<h2>Depósitos</h2>
	<xsl:for-each select="deposito">
	<h3>Depósito Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Código Interno: </b> 
	<xsl:value-of select="codInterno/@*"/></p1><br></br>
	<p1><b>Descrição: </b>
	<xsl:value-of select="descricao"/></p1>
	</xsl:for-each>
</xsl:template>

<xsl:template match="linhasProducao">	
	<h2>Linhas de Produção</h2>
	<xsl:for-each select="linhaProducao">
	<h3>Linha de Produção Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Identificador: </b> 
	<xsl:value-of select="identificador/@*"/></p1><br></br>
		<xsl:for-each select="maquinas">
			<h4>Máquina Nº<xsl:value-of select="position()"/>:</h4>
			<p1><b>Código Interno: </b>
			<xsl:value-of select="codInterno/@*"/></p1><br></br>
			<p1><b>Número de Série: </b>
			<xsl:value-of select="numSerie"/></p1><br></br>
			<p1><b>Descrição: </b>
			<xsl:value-of select="descricao"/></p1><br></br>
			<p1><b>Data de Instalação: </b>
			<xsl:value-of select="dataInstalacao"/></p1><br></br>
			<p1><b>Marca: </b>
			<xsl:value-of select="marca"/></p1><br></br>
			<p1><b>Modelo: </b>
			<xsl:value-of select="modelo"/></p1><br></br>
			<p1><b>Posição: </b>
			<xsl:value-of select="posicao"/></p1>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="ordensProd">	
	<h2>Ordens de Produção</h2>
	<xsl:for-each select="ordem">
	<h3>Ordem de Produção Nº<xsl:value-of select="position()"/>:</h3>
	<p1><b>Código: </b> 
	<xsl:value-of select="code/@*"/></p1><br></br>
	<xsl:for-each select="encomenda/encomenda">
		<p1><b>Encomenda Nº<xsl:value-of select="position()"/>: </b> 
		<xsl:value-of select="@*"/></p1><br></br>
	</xsl:for-each>
	<p1><b>Status: </b> 
	<xsl:value-of select="status"/></p1><br></br>
	<p1><b>Data de Emissão: </b> 
	<xsl:value-of select="dataEmissao"/></p1><br></br>
	<p1><b>Data Prevista: </b> 
	<xsl:value-of select="dataPrevista"/></p1><br></br>
	<p1><b>Quantidade de Produto: </b> 
	<xsl:value-of select="quantidadeProduto/@*"/></p1><br></br>
	<p1><b>Unidade: </b> 
	<xsl:value-of select="unidade/@*"/></p1><br></br>
	<p1><b>Produto: </b> 
	<xsl:value-of select="produto"/></p1>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>



