<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
	<xsl:template match="/ChaoFabrica">
	{"Chão de Fábrica" : {
			<xsl:apply-templates select="categoriasMateriaPrima"/>
			<xsl:apply-templates select="materiasPrimas"/>
			<xsl:apply-templates select="produtos"/>
			<xsl:apply-templates select="depositos"/>
			<xsl:apply-templates select="linhasProducao"/>
			<xsl:apply-templates select="ordensProd"/>}}
	</xsl:template>
	
<xsl:template match="categoriasMateriaPrima">	
	"Categorias de Materia Prima" : {
	<xsl:for-each select="categoria">
		"Categoria Nº<xsl:value-of select="position()"/>" : {
		"Código" : "<xsl:value-of select="codAlfaNumerico"/>",
	<xsl:choose>
		<xsl:when test="position()!=last()">
			"Descrição": "<xsl:value-of select="descricao"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Descrição": "<xsl:value-of select="descricao"/>"}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},
</xsl:template>

<xsl:template match="materiasPrimas">	
	"Materias Primas" : {
	<xsl:for-each select="materiaPrima">
		"Materia Prima Nº<xsl:value-of select="position()"/>" : {
		"Descrição" : "<xsl:value-of select="descricao/@*"/>",
		"Código Interno" : "<xsl:value-of select="codInterno"/>",
		"Ficha Técnica" : "<xsl:value-of select="fichaTecnica/@*"/>",
	<xsl:choose>
		<xsl:when test="position()!=last()">
			"Categoria" : "<xsl:value-of select="categoria"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Categoria" : "<xsl:value-of select="categoria"/>"}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},
</xsl:template>

<xsl:template match="produtos">	
	"Produtos" : {
	<xsl:for-each select="produto">
		"Produto Nº<xsl:value-of select="position()"/>" : {
		"Código Fabrico" : "<xsl:value-of select="codFabrico"/>",
		"Código Comercial" : "<xsl:value-of select="codComercial/@*"/>",
		"Descrição Breve" : "<xsl:value-of select="descricaoBreve/@*"/>",
		"Descrição Completa" : "<xsl:value-of select="descricaoCompleta/@*"/>",
		"Categoria de Produto" : "<xsl:value-of select="categoriaProduto/@*"/>",
	<xsl:if test="fichaProducao">
	"Ficha de Produção" : { 
	<xsl:for-each select="fichaProducao/componentes">
		"Componente Nº<xsl:value-of select="position()"/>" : {
		"Material" : "<xsl:value-of select="material"/>",
	<xsl:choose>
		<xsl:when test="position()!=last()">
			"Quantidade" : "<xsl:value-of select="quantidade"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Quantidade" : "<xsl:value-of select="quantidade"/>"}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},</xsl:if>
	<xsl:choose>
		<xsl:when test="position()!=last()">
			"Unidade" : "<xsl:value-of select="unidade/@*"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Unidade" : "<xsl:value-of select="unidade/@*"/>"}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},
</xsl:template>

<xsl:template match="depositos">
	"Depósitos" : {
	<xsl:for-each select="deposito">
		"Depósito Nº<xsl:value-of select="position()"/>" : {
		"Código Interno" : "<xsl:value-of select="codInterno/@*"/>",
	<xsl:choose>
		<xsl:when test="position()!=last()">
			"Descrição" : "<xsl:value-of select="descricao"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Descrição" : "<xsl:value-of select="descricao"/>"}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},
</xsl:template>

<xsl:template match="linhasProducao">	
	"Linhas de Produção" : {
	<xsl:for-each select="linhaProducao">
		"Linha de Produção Nº<xsl:value-of select="position()"/>" : {
		"Identificador" : "<xsl:value-of select="identificador/@*"/>",
	<xsl:for-each select="maquinas">
		"Máquina Nº<xsl:value-of select="position()"/>" : {
		"Código Interno" : "<xsl:value-of select="codInterno/@*"/>",
		"Número de Série" : "<xsl:value-of select="numSerie"/>",
		"Descrição" : "<xsl:value-of select="descricao"/>",
		"Data de Instalação" : "<xsl:value-of select="dataInstalacao"/>",
		"Marca" : "<xsl:value-of select="marca"/>",
		"Modelo" : "<xsl:value-of select="modelo"/>",
		<xsl:choose>
		<xsl:when test="position()!=last()">
			"Posição" : "<xsl:value-of select="posicao"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Posição" : "<xsl:value-of select="posicao"/>"}
		</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
	<xsl:choose>
		<xsl:when test="position()!=last()">
			},
		</xsl:when>
		<xsl:otherwise>
			}
		</xsl:otherwise>
	</xsl:choose>
	</xsl:for-each>},
</xsl:template>

<xsl:template match="ordensProd">	
	"Ordens de Produção" : {
	<xsl:for-each select="ordem">
		"Ordem de Produção Nº<xsl:value-of select="position()"/>" : {
		"Código" : "<xsl:value-of select="code/@*"/>",
	<xsl:for-each select="encomenda/encomenda">
		"Encomenda Nº<xsl:value-of select="position()"/>" : "<xsl:value-of select="@*"/>",
	</xsl:for-each>
		"Status" : "<xsl:value-of select="status"/>",
		"Data de Emissão" : "<xsl:value-of select="dataEmissao"/>",
		"Data Prevista" : "<xsl:value-of select="dataPrevista"/>",
		"Quantidade de Produto" : "<xsl:value-of select="quantidadeProduto/@*"/>",
		"Unidade" : "<xsl:value-of select="unidade/@*"/>",
		<xsl:choose>
		<xsl:when test="position()!=last()">
			"Produto" : "<xsl:value-of select="produto"/>"},
		</xsl:when>
		<xsl:otherwise>
			"Produto" : "<xsl:value-of select="produto"/>"}
		</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>}
</xsl:template>

</xsl:stylesheet>