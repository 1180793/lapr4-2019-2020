<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
	<xsl:template match="/ChaoFabrica">
	Chão de Fábrica
			<xsl:apply-templates select="categoriasMateriaPrima"/>
			<xsl:apply-templates select="materiasPrimas"/>
			<xsl:apply-templates select="produtos"/>
			<xsl:apply-templates select="depositos"/>
			<xsl:apply-templates select="linhasProducao"/>
			<xsl:apply-templates select="ordensProd"/>
	</xsl:template>
	
<xsl:template match="categoriasMateriaPrima">	
	<xsl:text>&#10;</xsl:text>
	Categorias de Materia Prima
	<xsl:for-each select="categoria">
	Categoria Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Código: <xsl:value-of select="codAlfaNumerico"/>
		Descrição: <xsl:value-of select="descricao"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template match="materiasPrimas">	
	<xsl:text>&#10;</xsl:text>
	Materias Primas
	<xsl:for-each select="materiaPrima">
	Materia Prima Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Descrição: <xsl:value-of select="descricao/@*"/>
		Código Interno: <xsl:value-of select="codInterno"/>
		Ficha Técnica: <xsl:value-of select="fichaTecnica/@*"/>
		Categoria: <xsl:value-of select="categoria"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template match="produtos">	
	<xsl:text>&#10;</xsl:text>
	Produtos
	<xsl:for-each select="produto">
	Produto Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Código Fabrico: <xsl:value-of select="codFabrico"/>
		Código Comercial: <xsl:value-of select="codComercial/@*"/>
		Descrição Breve: <xsl:value-of select="descricaoBreve/@*"/>
		Descrição Completa: <xsl:value-of select="descricaoCompleta/@*"/>
		Categoria de Produto: <xsl:value-of select="categoriaProduto/@*"/>
		Ficha de Produção:
		<xsl:for-each select="fichaProducao/componentes">
		Componente Nº<xsl:value-of select="position()"/>: 
			Material: <xsl:value-of select="material"/>
			Quantidade: <xsl:value-of select="quantidade"/>
		</xsl:for-each>
		<xsl:text>&#10;</xsl:text>
		Unidade: <xsl:value-of select="unidade/@*"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template match="depositos">
	<xsl:text>&#10;</xsl:text>	
	Depósitos
	<xsl:for-each select="deposito">
	Depósito Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Código Interno: <xsl:value-of select="codInterno/@*"/>
		Descrição: <xsl:value-of select="descricao"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template match="linhasProducao">	
	<xsl:text>&#10;</xsl:text>
	Linhas de Produção
	<xsl:for-each select="linhaProducao">
	Linha de Produção Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Identificador: <xsl:value-of select="identificador/@*"/>
	<xsl:for-each select="maquinas">
		Máquina Nº<xsl:value-of select="position()"/>:
			Código Interno: <xsl:value-of select="codInterno/@*"/>
			Número de Série: <xsl:value-of select="numSerie"/>
			Descrição: <xsl:value-of select="descricao"/>
			Data de Instalação: <xsl:value-of select="dataInstalacao"/>
			Marca: <xsl:value-of select="marca"/>
			Modelo: <xsl:value-of select="modelo"/>	
			Posição: <xsl:value-of select="posicao"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="ordensProd">	
	<xsl:text>&#10;</xsl:text>
	Ordens de Produção
	<xsl:for-each select="ordem">
	Ordem de Produção Nº<xsl:value-of select="position()"/>:<xsl:text>&#10;</xsl:text>
		Código: <xsl:value-of select="code/@*"/>
	<xsl:for-each select="encomenda/encomenda">
		Encomenda Nº<xsl:value-of select="position()"/>: <xsl:value-of select="@*"/>
	</xsl:for-each>
		Status: <xsl:value-of select="status"/>
		Data de Emissão: <xsl:value-of select="dataEmissao"/>
		Data Prevista: <xsl:value-of select="dataPrevista"/>
		Quantidade de Produto: <xsl:value-of select="quantidadeProduto/@*"/>
		Unidade: <xsl:value-of select="unidade/@*"/>
		Produto: <xsl:value-of select="produto"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>



