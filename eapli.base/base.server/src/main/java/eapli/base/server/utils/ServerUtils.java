/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.utils;

import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.fichaproducao.domain.Material;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.repositories.ProdutoRepository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServerUtils {

    // Offset
    public static final int VERSION_OFFSET = 0;
    public static final int CODE_OFFSET = 1;
    public static final int ID_OFFSET = 2;
    public static final int DATA_LENGTH_OFFSET = 4;
    public static final int RAW_DATA_OFFSET = 6;

    // Length
    public static final int VERSION_LENGTH = 1;
    public static final int CODE_LENGTH = 1;
    public static final int ID_LENGTH = 2;
    public static final int DATA_LENGTH_LENGTH = 2;

    public static final int MESSAGE_NO_RAW_DATA_LENGTH = 6;

    // Codes
    public static final int HELLO = 0;
    public static final int MSG = 1;
    public static final int CONFIG = 2;
    public static final int RESET = 3;
    public static final int ACK = 150;
    public static final int NACK = 151;

    // Codigos de Mensagem
    private static final String CONSUMO = "C0";
    private static final String ENTREGA_PRODUCAO = "C9";
    private static final String PRODUCAO = "P1";
    private static final String ESTORNO = "P2";

    private static final String INICIO_ATIVIDADE = "S0";
    private static final String RETOMA_ATIVIDADE = "S1";
    private static final String PARAGEM = "S8";
    private static final String FIM_ATIVIDADE = "S9";

    // Repository
    private static final ProdutoRepository PROD_REPO = PersistenceContext.repositories().produtos();
    private static final MateriaPrimaRepository MATERIAPRIMA_REPO = PersistenceContext.repositories().materiasPrimas();
    private static final DepositosRepository DEPO_REPO = PersistenceContext.repositories().depositos();
    private static final OrdemProducaoRepository ORDEMPROD_REPO = PersistenceContext.repositories().ordensProducao();
    private static final MensagemRepository MESSAGE_REPO = PersistenceContext.repositories().mensagens();

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String getAddress(String ip) {
        String[] temp = ip.split(":");
        return temp[0];
    }

    public static Integer getPort(String ip) {
        String[] temp = ip.split(":");
        return Integer.parseInt(temp[1]);
    }

    /**
     * Verifica a estrutura da mensagem e se os seus campos sao validos
     *
     * @param msg
     * @return
     */
    public static boolean validarMensagem(Mensagem msg) {
        try {
            if (msg == null) {
                return false;
            }
            String[] temp = msg.mensagem().split(";");
            switch (temp[1]) {
                case CONSUMO:
                    if (temp.length != 6) {
                        return false;
                    } else if (getMaterial(temp[3]) == null) {
                        return false;
                    } else if (getDeposito(temp[5]) == null) {
                        return false;
                    } else if (Double.parseDouble(temp[4]) <= 0) {
                        return false;
                    } else {
                        try {
                            sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                case ENTREGA_PRODUCAO:
                    if (temp.length != 6) {
                        return false;
                    } else if (getProduto(temp[3]) == null) {
                        return false;
                    } else if (getDeposito(temp[5]) == null) {
                        return false;
                    } else if (Double.parseDouble(temp[4]) <= 0) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                case PRODUCAO:
                    if (temp.length != 6) {
                        return false;
                    } else if (getProduto(temp[3]) == null) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                case ESTORNO:
                    if (temp.length != 6) {
                        return false;
                    } else if (getMaterial(temp[3]) == null) {
                        return false;
                    } else if (getDeposito(temp[5]) == null) {
                        return false;
                    } else if (Double.parseDouble(temp[4]) <= 0) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                case INICIO_ATIVIDADE:
                    if (temp.length != 4) {
                        return false;
                    } else if (getOrdemProducao(temp[3]) == null) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                case RETOMA_ATIVIDADE:
                    if (temp.length != 3) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }
                    return true;

                case PARAGEM:
                    if (temp.length != 4) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }
                    return true;

                case FIM_ATIVIDADE:
                    if (temp.length != 4) {
                        return false;
                    } else if (getOrdemProducao(temp[3]) == null) {
                        return false;
                    } else {
                        try {
                            Date testDate = sdf.parse(temp[2]);
                        } catch (ParseException ex) {
                            return false;
                        }
                    }

                    break;

                default:
                    return false;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }

    public synchronized static Material getMaterial(String id) {

        if (getMateriaPrima(id) != null) {
            return getMateriaPrima(id);
        }

        if (getProduto(id) != null) {
            return getProduto(id);
        }

        return null;
    }

    public synchronized static Material getMateriaPrima(String id) {
        if (MATERIAPRIMA_REPO.containsOfIdentity(CodigoInterno.valueOf(id))) {
            return MATERIAPRIMA_REPO.ofIdentity(CodigoInterno.valueOf(id)).get();
        }
        return null;
    }

    public synchronized static Material getProduto(String id) {
        if (PROD_REPO.containsOfIdentity(CodFabrico.valueOf(id))) {
            return PROD_REPO.ofIdentity(CodFabrico.valueOf(id)).get();
        }
        return null;
    }

    public synchronized static Deposito getDeposito(String id) {
        if (DEPO_REPO.containsOfIdentity(CodigoInternoDeposito.valueOf(id))) {
            return DEPO_REPO.ofIdentity(CodigoInternoDeposito.valueOf(id)).get();
        }
        return null;
    }

    public synchronized static OrdemProducao getOrdemProducao(String id) {
        if (ORDEMPROD_REPO.containsOfIdentity(CodInternoOrdemDeProducao.valueOf(id))) {
            return ORDEMPROD_REPO.ofIdentity(CodInternoOrdemDeProducao.valueOf(id)).get();
        }
        return null;
    }

}
