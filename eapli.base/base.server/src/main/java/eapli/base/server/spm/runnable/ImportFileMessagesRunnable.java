/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.spm.runnable;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.CodigoInternoMaquina;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.server.utils.ServerUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author ZDPessoa
 */
public class ImportFileMessagesRunnable implements Runnable {

    private final File file;
    private final MensagemRepository msgRepository = PersistenceContext.repositories().mensagens();
    private final MaquinasRepository maqRepository = PersistenceContext.repositories().maquinas();

    public ImportFileMessagesRunnable(File file) {
        this.file = file;
    }

    @Override
    public void run() {
        File errors = new File("errorLogs" + File.separator + "mensagens" + file.getName() + ".txt");
        System.out.println("Importando mensagens do ficheiro '" + file.getName() + "'...");
        int cont = 0, contErros = 0;
        String msg = "";
        try {
            errors.createNewFile();
        } catch (IOException ex) {
        }
        try (FileWriter writer = new FileWriter(errors)) {
            try (Scanner reader = new Scanner(file)) {
                while (reader.hasNextLine()) {
                    try {
                        cont++;
                        msg = reader.nextLine();

                        String[] dataFields = msg.split(";");
                        String strData = dataFields[2];

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                        Date dataEmissao = sdf.parse(strData);

                        Maquina maquina = null;
                        for (Maquina maq : maqRepository.findAll()) {
                            if (maq.identity().equals(CodigoInternoMaquina.valueOf(dataFields[0]))) {
                                maquina = maq;
                            }
                        }
                        Mensagem mensagem = null;
                        if (maquina != null) {
                            mensagem = new Mensagem(msg, TipoMensagem.POR_PROCESSAR, maquina, dataEmissao);
                        } else {
                            writer.write("#" + cont + " - " + msg + "\n");
                            System.out.println("MAQUINA DESCONHECIDA LINHA #" + cont);
                            contErros++;
                            throw new IllegalArgumentException("Máquina Desconhecida. ID: " + dataFields[0]);
                        }

                        if (ServerUtils.validarMensagem(mensagem) == false) {
                            System.out.println("ERRO AO VALIDAR LINHA #" + cont);
                            contErros++;
                            throw new IllegalArgumentException();
                        }
                        msgRepository.save(mensagem);
                    } catch (ArrayIndexOutOfBoundsException | ParseException | IllegalArgumentException ex) {
                        System.err.println("Mensagem Inválida! Ficheiro: " + file.getName() + " | Linha #" + cont);
                        writer.write("#" + cont + " - " + msg + "\n");
                        contErros++;
                    }
                }
                System.out.println("Importação de Mensagens do ficheiro '" + file.getName() + "' terminada.");
                if (contErros > 0) {
                    System.out.println("Foram encontrados '" + contErros + "' erros.");
                    System.out.println("Para informação adicional verifique o ficheiro de registo de erros: " + errors.getPath());
                }
            } catch (IOException ex) {

            }
        } catch (IOException ex) {
        }
    }
}
