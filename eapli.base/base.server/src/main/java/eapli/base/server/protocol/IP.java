package eapli.base.server.protocol;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class IP {

    private final String address;
    private final int port;

    public IP(String address, int port) {
        this.address = address;
        this.port = port;
    }

    public String address() {
        return this.address;
    }

    public int port() {
        return this.port;
    }

}
