package eapli.base.server.udp;

import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.server.protocol.IP;
import eapli.base.server.udp.runnable.UdpFinalThread;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe que simula o servidor de monotorizaçao baseado em comunicaçoes UDP
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class UdpServer {

    private static final int PORT_NUMBER = 30602;
    private static final int DATA_LENGHT = 512;

    public static List<Maquina> maquinas = new ArrayList<>();
    public static List<IP> addresses = new ArrayList<>();
    public static Map<Long, IP> addressesMap = new HashMap<>();

    public int objectMutext = 0;

    public static void addIP(IP ip) {
        synchronized (addresses) {
            addresses.add(ip);
        }
    }

    public static void remIP(IP ip) {
        synchronized (addresses) {
            addresses.remove(ip);
        }
    }

    public static void addIP_Map(Long id, IP ip) {
        synchronized (addressesMap) {
            addressesMap.put(id, ip);
        }
    }

    static DatagramSocket sock;

    public static void main(String args[]) throws Exception {

        try {
            //Create a socket to listen at port PORT_NUMBER 
            sock = new DatagramSocket(PORT_NUMBER);
            System.out.println("DatagramSocket Open");

        } catch (BindException ex) {
            System.out.println("Bind to local port failed");
            System.exit(1);
        }
        System.out.println("Listening for UDP requests (IPv6/IPv4). Use CTRL+C to terminate the server");

        new Thread(new UdpFinalThread(sock)).start();
    }

}
