package eapli.base.server.tcp.runnable;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.server.utils.ByteUtils;
import eapli.base.server.utils.ServerUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TcpConfigRequest implements Runnable {

    private Socket s;
    private long idMaquina;
    private DataOutputStream sOut;
    private final MaquinasRepository maquinaRepository = PersistenceContext.repositories().maquinas();
    private final MensagemRepository msgRepository = PersistenceContext.repositories().mensagens();
    private Maquina maquina;

    public TcpConfigRequest(Socket cli_s, Long id) {
        s = cli_s;
        idMaquina = id;
    }

    @Override
    public void run() {
        long version, aux, i, code, dataLength;
        String rawData;

        InetAddress clientIP;

        clientIP = s.getInetAddress();
        System.out.println("Nova conexão de uma Máquina.");
        System.out.println("Endereço = '" + clientIP.getHostAddress() + "'");
        System.out.println("Port = '" + s.getPort() + "'");

        try {
            sOut = new DataOutputStream(s.getOutputStream());

            // Match Máquina
            int id = (int) idMaquina;
            for (Maquina maq : maquinaRepository.findAll()) {
                if (maq.identificadorUnico() == id) {
                    this.maquina = maq;
                }
            }

            if (maquina != null) {
                sendMessage(sOut, 0, ServerUtils.CONFIG, idMaquina, "Pedido de Configuracao da Maquina ID: " + id);
            } else {
                System.err.println("Erro ao conectar com a Máquina! ID desconhecido.");
            }

            s.close();
        } catch (IOException ex) {
            System.out.println("IOException");
        }
    }

    public void sendMessage(DataOutputStream sOut, long version, long code, long id, String rawData) throws UnsupportedEncodingException {
        try {
            byte[] rawDataBytes = rawData.getBytes("UTF_8");
            int rawDataLength = rawDataBytes.length;

            sOut.write(ByteUtils.longToBytes(version), ServerUtils.VERSION_OFFSET, ServerUtils.VERSION_LENGTH);
            sOut.write(ByteUtils.longToBytes(code), ServerUtils.CODE_OFFSET, ServerUtils.CODE_LENGTH);
            sOut.write(ByteUtils.longToBytes(id), ServerUtils.ID_OFFSET, ServerUtils.ID_LENGTH);
            sOut.write(ByteUtils.longToBytes(rawDataLength), ServerUtils.DATA_LENGTH_OFFSET, ServerUtils.DATA_LENGTH_LENGTH);
            sOut.write(rawDataBytes, ServerUtils.RAW_DATA_OFFSET, rawDataLength);

        } catch (IOException ex) {
            System.out.println("IOException");
        }
    }
}
