/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.base.server.utils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TcpUtils {

    public static void sendMessage(DataOutputStream sOut, long version, long code, long id, String rawData) throws UnsupportedEncodingException {
        try {
            byte[] rawDataBytes = rawData.getBytes();
            int rawDataLength = 0, rawDataLengthCopy = 0;

            if (rawData == null || rawData.isEmpty()) {
                rawDataLength = 0;
                rawDataLengthCopy = 0;
            } else {
                rawDataLength = rawDataBytes.length;
                rawDataLengthCopy = rawDataBytes.length;
            }

            for (int i = 0; i < ServerUtils.VERSION_LENGTH; i++) {
                sOut.write((byte) (version % 256));
                version = version / 256;
            }

            for (int i = 0; i < ServerUtils.CODE_LENGTH; i++) {
                sOut.write((byte) (code % 256));
                code = code / 256;
            }

            for (int i = 0; i < ServerUtils.ID_LENGTH; i++) {
                sOut.write((byte) (id % 256));
                id = id / 256;
            }

            for (int i = 0; i < ServerUtils.DATA_LENGTH_LENGTH; i++) {
                sOut.write((byte) (rawDataLength % 256));
                rawDataLength = rawDataLength / 256;
            }

            if (rawDataLength != 0) {
                for (int i = 0; i < rawDataLengthCopy; i++) {
                    sOut.write(rawDataBytes[i]);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("IOException-sendMessage");
        }
    }
    
}
