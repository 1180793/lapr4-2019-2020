/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.utils;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UdpUtils {
    
    public static byte[] createHelloMessage() {
        return sendMessage(0, ServerUtils.HELLO, 0, "");
    }

    public static byte[] sendMessage(long version, long code, long id, String rawData) {

        int totalSize;
        int dataLength = 0;
        boolean hasMessage = true;

        if (rawData == null || rawData.isEmpty()) {
            totalSize = 6;
            hasMessage = false;
        } else {
            dataLength = rawData.length();
            totalSize = 6 + dataLength;
        }

        byte[] bytes = new byte[totalSize];

        for (int i = ServerUtils.VERSION_OFFSET; i < (ServerUtils.VERSION_OFFSET + ServerUtils.VERSION_LENGTH); i++) {
            bytes[i] = (byte) (version % 256);
            version = version / 256;
        }

        for (int i = ServerUtils.CODE_OFFSET; i < (ServerUtils.CODE_OFFSET + ServerUtils.CODE_LENGTH); i++) {
            bytes[i] = (byte) (code % 256);
            code = code / 256;
        }

        for (int i = ServerUtils.ID_OFFSET; i < (ServerUtils.ID_OFFSET + ServerUtils.ID_LENGTH); i++) {
            bytes[i] = (byte) (id % 256);
            id = id / 256;
        }

        for (int i = ServerUtils.DATA_LENGTH_OFFSET; i < (ServerUtils.DATA_LENGTH_OFFSET + ServerUtils.DATA_LENGTH_LENGTH); i++) {
            bytes[i] = (byte) (dataLength % 256);
            dataLength = dataLength / 256;
        }

        if (hasMessage) {
            byte[] rawDataBytes = rawData.getBytes();
            int counter = 0;
            for (int i = 0; i < rawDataBytes.length; i++) {
                bytes[ServerUtils.RAW_DATA_OFFSET + i] = rawDataBytes[i];
                counter++;
            }
        }
        
        return bytes;
        
    }
}
