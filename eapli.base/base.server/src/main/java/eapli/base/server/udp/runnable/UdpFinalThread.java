package eapli.base.server.udp.runnable;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.server.protocol.IP;
import eapli.base.server.protocol.Protocol;
import eapli.base.server.protocol.ProtocolMessage;
import eapli.base.server.udp.UdpServer;
import eapli.base.server.utils.ServerUtils;
import eapli.base.server.utils.UdpUtils;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UdpFinalThread implements Runnable {

    private static final int DATA_LENGTH = 512;
    private static final String BROADCAST_ADDRESS = "255.255.255.255";
    private static final int PORT_NUMBER = 30602;

    private static final int HEARTBEAT_LIMIT = 3;
    private static final int INACTIVE_LIMITE = 60;

    private final MaquinasRepository maqRepository = PersistenceContext.repositories().maquinas();

    private DatagramSocket sock;

    private boolean running = false;
    private boolean runBroadcast = false;

    public UdpFinalThread(DatagramSocket s) {
        this.sock = s;
        this.running = true;
        this.runBroadcast = true;
    }

    @Override
    public void run() {

        long startTime;
        long delta;

        while (running == true) {
            startTime = System.currentTimeMillis(); // agora () //11:20:20
            try {
                if (runBroadcast) {
                    broadcastLogic();
                    runBroadcast = false;
                }

                InetAddress broadcastAddr;
                broadcastAddr = InetAddress.getByName(BROADCAST_ADDRESS);
                byte[] receivedData = new byte[DATA_LENGTH];
                DatagramPacket udpPacket = new DatagramPacket(receivedData, receivedData.length, broadcastAddr, PORT_NUMBER);

                try {
                    sock.receive(udpPacket);
                    System.out.println("Received Message");
                    byte[] received = udpPacket.getData();
                    System.out.println("Trying to decode...");

                    //Recebem mensagem
                    ProtocolMessage msg = Protocol.interpretarMensagem(received, udpPacket.getAddress().getHostAddress(), udpPacket.getPort());
                    handleMessages(msg);
                    delta = System.currentTimeMillis() - startTime;

                    if (delta >= HEARTBEAT_LIMIT * 1000) {
                        sendHeartbeat(udpPacket);
                    }

                    checkInactiveMachines();

                } catch (SocketTimeoutException ex) {
                    System.out.println("No reply from Maquina");
                }

            } catch (UnknownHostException ex) {
                System.out.println("O endereço de broadcast nao e reconhecido");
                Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SocketException ex) {
                System.out.println("Nao foi possivel fazer a ligaçao a socket");
                Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                System.out.println("Erro ao enviar PacketUDP");
                Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void broadcastLogic() {
        try {
            InetAddress broadcastAddr;
            broadcastAddr = InetAddress.getByName(BROADCAST_ADDRESS);
            this.sock.setBroadcast(true);
            this.sock.setSoTimeout(HEARTBEAT_LIMIT * 1000);

            byte[] broadcastData = UdpUtils.createHelloMessage();
            DatagramPacket udpPacket = new DatagramPacket(broadcastData, broadcastData.length, broadcastAddr, PORT_NUMBER);
            udpPacket.setData(broadcastData);
            udpPacket.setLength(broadcastData.length);

            this.sock.send(udpPacket);
            this.sock.setBroadcast(true);
        } catch (UnknownHostException ex) {
            System.out.println("O Endereço de Broadcast não é reconhecido");
            Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SocketException ex) {
            System.out.println("Nao foi possivel fazer a ligaçao ao Socket");
            Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.println("Erro ao enviar Packet");
            Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleMessages(ProtocolMessage msg) {
        if (msg.code() == ServerUtils.ACK) {

            UdpServer.addIP(msg.ip());
            UdpServer.addIP_Map(msg.idMaquina(), msg.ip());

            Maquina m = maqRepository.findByCodigoComunicacao(msg.idMaquina()).iterator().next();

            m.setLastActiveTime(System.currentTimeMillis());
            m.setDirty(true);

            maqRepository.save(m);
        }
    }

    public void sendHeartbeat(DatagramPacket udpPacket) {
        try {
            this.sock.setBroadcast(false);
            this.sock.setSoTimeout(HEARTBEAT_LIMIT * 1000);

            byte[] data = UdpUtils.createHelloMessage(); //UdpUtils.sendMessage(0, ServerUtils.HELLO, 0, "");

            udpPacket.setData(data);
            udpPacket.setLength(data.length);

            synchronized (UdpServer.addressesMap) {
                for (Map.Entry<Long, IP> entry : UdpServer.addressesMap.entrySet()) {

                    udpPacket.setAddress(InetAddress.getByName(entry.getValue().address()));
                    udpPacket.setPort(entry.getValue().port());

                    this.sock.send(udpPacket);
                }
            }
        } catch (SocketException ex) {
            Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UdpFinalThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkInactiveMachines() {
        for (Maquina m : UdpServer.maquinas) {
            long now = System.currentTimeMillis();
            if (now - m.lastActiveTime() >= INACTIVE_LIMITE * 1000) {
                m.deactivate();
                this.maqRepository.save(m);
            }
        }
    }

}
