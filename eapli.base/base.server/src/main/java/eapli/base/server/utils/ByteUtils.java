package eapli.base.server.utils;

import java.nio.ByteBuffer;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ByteUtils {
    
    private static final ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);    

    public static byte[] longToBytes(long x) {
        buffer.putLong(0, x);
        return buffer.array();
    }

    public static long bytesToLong(byte[] bytes) {
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();//need flip 
        return buffer.getLong();
    }
}