package eapli.base.server.protocol;

/**
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProtocolMessage {

    private final long version;
    private final long code; 
    private final long idMaquina;
    private final long dataLength;
    private final String data;
    private final IP ip;
    
    public ProtocolMessage(long version, long code, long idMaquina, long dataLength, String data, String address, int port) {
        this.version = version;
        this.code = code;
        this.idMaquina = idMaquina;
        this.dataLength = dataLength;
        this.data = data;
        this.ip = new IP(address, port);
    }
    
    public long version() {
        return this.version;
    }
    
    public long code() {
        return this.code;
    }
    
    public long idMaquina() {
        return this.idMaquina;
    }
    
    public long dataLenght() {
        return this.dataLength;
    }
    
    public String data() {
        return this.data;
    }
    
    public IP ip() {
        return this.ip;
    }
            
}
