/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.tcp.runnable;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.server.tcp.TcpServer;
import eapli.base.server.utils.ServerUtils;
import eapli.base.server.utils.TcpUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TcpMessageThread implements Runnable {

    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private final MaquinasRepository maquinaRepository = PersistenceContext.repositories().maquinas();
    private final MensagemRepository msgRepository = PersistenceContext.repositories().mensagens();
    private Maquina maquina;

    public TcpMessageThread(Socket cli_s) {
        s = cli_s;
    }

    @Override
    public void run() {
        long version, aux, i, code, idMaquina, dataLength;
        String rawData;

        InetAddress clientIP;

        clientIP = s.getInetAddress();

        try {
            TcpServer.semaphore.acquire();

            System.out.println("Nova conexão de uma Máquina. (MessageThread)");
            System.out.println("Endereço = '" + clientIP.getHostAddress() + "'");
            System.out.println("Port = '" + s.getPort() + "'");

            sOut = new DataOutputStream(s.getOutputStream());
            sIn = new DataInputStream(s.getInputStream());
            // Ler Version
            version = 0;
            aux = 1;
            for (i = 0; i < ServerUtils.VERSION_LENGTH; i++) {
                version = version + aux * sIn.read();
                aux = aux * 256;
            }

            boolean flag = true;
            while (flag) {
                rawData = "";
                // Ler Message Code
                code = 0;
                aux = 1;
                for (i = 0; i < ServerUtils.CODE_LENGTH; i++) {
                    code = code + aux * sIn.read();
                    aux = aux * 256;
                }

                // Ler ID Máquina
                idMaquina = 0;
                aux = 1;
                for (i = 0; i < ServerUtils.ID_LENGTH; i++) {
                    idMaquina = idMaquina + aux * sIn.read();
                    aux = aux * 256;
                }
                // Ler Data Length
                dataLength = 0;
                aux = 1;
                for (i = 0; i < ServerUtils.DATA_LENGTH_LENGTH; i++) {
                    dataLength = dataLength + aux * sIn.read();
                    aux = aux * 256;
                }
                
                // Ler Raw Data
                if (dataLength != 0) {
                    for (i = 0; i < dataLength; i++) {
                        char a = (char) sIn.read();
                        rawData = rawData + "" + a;
                    }
                }

                System.out.printf("\nVersion: %d\n", version);
                System.out.printf("Code: %d\n", code);
                System.out.printf("Id: %d\n", idMaquina);
                System.out.printf("Data Length: %d", dataLength);
                if (dataLength > 0) {
                    System.out.printf("\nRaw Data: %s\n", rawData);
                }

                // Match Máquina
                for (Maquina maq : maquinaRepository.findAll()) {
                    if (maq.identificadorUnico() == idMaquina) {
                        this.maquina = maq;
                    }
                }
                if (maquina != null) {
                    System.out.printf("Machine with ID '%d' was found\n", maquina.identificadorUnico());
                }

                if (code == ServerUtils.MSG) {
                    if (maquina == null || maquina.identificadorUnico() != idMaquina || (!maquina.enderecoRede().equalsIgnoreCase(clientIP.getHostAddress()))) {
                        TcpUtils.sendMessage(sOut, version, ServerUtils.NACK, idMaquina, "");
                    } else {
                        try {
                            String[] rawDataFields = rawData.split(";");
                            String strData = rawDataFields[2];

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                            Date dataEmissao = sdf.parse(strData);

                            Mensagem msg = new Mensagem(rawData, TipoMensagem.POR_PROCESSAR, maquina, dataEmissao);
                            this.msgRepository.save(msg);
                            System.out.println("Message Stored and Sent 'ACK' Message to Machine...");
                            TcpUtils.sendMessage(sOut, version, ServerUtils.ACK, idMaquina, "");
                        } catch (Exception e) {
                            TcpUtils.sendMessage(sOut, version, ServerUtils.NACK, idMaquina, "");
                            e.printStackTrace();
                        }
                    }
                }

                TimeUnit.SECONDS.sleep(3);

                version = sIn.read();
                if (version == -1) {
                    flag = false;
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("IOException");
        } catch (InterruptedException ex) {
            Logger.getLogger(TcpMessageThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Máquina disconectada. (MessageThread)");
        System.out.println("Endereço = '" + clientIP.getHostAddress() + "'");
        System.out.println("Port = '" + s.getPort() + "'");

        try {
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(TcpMessageThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        TcpServer.semaphore.release();
    }
}
