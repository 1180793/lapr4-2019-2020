/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.spm;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.server.spm.runnable.ProcessamentoRunnable;
import eapli.framework.io.util.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author PC
 */
public class ProcessamentoInitializer {

    public static void main(String args[]) throws Exception {
        LinhasProducaoRepository lpRepository = PersistenceContext.repositories().linhasProducao();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        boolean tempoInicialFlag = true, tempoFinalFlag = true;
        String strDataInicial, strDataFinal;
        Date dataInicial = null, dataFinal = null;

//        Pedir Tempo Inicial
        while (tempoInicialFlag) {
            strDataInicial = Console.readLine("Introduza o tempo inicial (dd/MM/yyyy HH:mm): ");
            try {
                dataInicial = sdf.parse(strDataInicial);
                tempoInicialFlag = false;
            } catch (ParseException ex) {
                System.out.println("Data Prevista de Execução Inválida!\n");
            }
        }

//        Pedir Tempo Final
        while (tempoFinalFlag) {
            strDataFinal = Console.readLine("Introduza o tempo final (dd/MM/yyyy HH:mm): ");
            try {
                dataFinal = sdf.parse(strDataFinal);
                tempoFinalFlag = false;
            } catch (ParseException ex) {
                System.out.println("Data Prevista de Execução Inválida!\n");
            }
        }
        
        // Processamento
        for (LinhaProducao linha : lpRepository.findAll()) {
            if (linha.hasProcessamentoMensagensAtivado()) {
                new Thread(new ProcessamentoRunnable(dataInicial, dataFinal, linha)).start();
            }
        }
    }
}
