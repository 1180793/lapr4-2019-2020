package eapli.base.server.protocol;

import eapli.base.server.utils.ServerUtils;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Protocol {

    public static ProtocolMessage interpretarMensagem(byte[] received, String address, int port) {
        long version, code, id, dataLength;
        String rawData;
        int aux;

        version = 0;
        aux = 1;
        for (int i = 0; i < ServerUtils.VERSION_LENGTH; i++) {
            version = version + aux * received[ServerUtils.VERSION_OFFSET + i];
            aux = aux * 256;
        }

        code = 0;
        aux = 1;
        for (int i = 0; i < ServerUtils.CODE_LENGTH; i++) {
            code = code + aux * received[ServerUtils.CODE_OFFSET + i];
            aux = aux * 256;
        }

        id = 0;
        aux = 1;
        for (int i = 0; i < ServerUtils.ID_LENGTH; i++) {
            id = id + aux * received[ServerUtils.ID_OFFSET + i];
            aux = aux * 256;
        }

        dataLength = 0;
        aux = 1;
        for (int i = 0; i < ServerUtils.DATA_LENGTH_LENGTH; i++) {
            dataLength = dataLength + aux * received[ServerUtils.DATA_LENGTH_OFFSET + i];
            aux = aux * 256;
        }

        if (dataLength != 0) {
            byte[] bytesRawData = new byte[(int) dataLength];
            for (int i = 0; i < dataLength; i++) {
                bytesRawData[i] = received[ServerUtils.DATA_LENGTH_OFFSET + i];
            }
            rawData = new String(bytesRawData);
            ProtocolMessage msg = new ProtocolMessage(version, code, id, dataLength, rawData, address, port);
            return msg;
        } else {
            ProtocolMessage msg = new ProtocolMessage(version, code, id, dataLength, "", address, port);
            return msg;
        }
    }

}
