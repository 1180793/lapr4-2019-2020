/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.spm.runnable;

import eapli.base.deposito.domain.CodigoInternoDeposito;
import eapli.base.deposito.domain.Deposito;
import eapli.base.deposito.repositories.DepositosRepository;
import eapli.base.execucaoOrdemProducao.domain.AtividadeMaquina;
import eapli.base.execucaoOrdemProducao.domain.Consumo;
import eapli.base.execucaoOrdemProducao.domain.EntregaProducao;
import eapli.base.execucaoOrdemProducao.domain.Estorno;
import eapli.base.execucaoOrdemProducao.domain.ExecucaoOrdemProducao;
import eapli.base.execucaoOrdemProducao.domain.Lote;
import eapli.base.execucaoOrdemProducao.domain.TipoAtividade;
import eapli.base.execucaoOrdemProducao.repositories.ExecucaoOrdemProducaoRepository;
import eapli.base.fichaproducao.domain.Material;
import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.LinhaProducao;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.LinhasProducaoRepository;
import eapli.base.materiaprima.domain.CodigoInterno;
import eapli.base.materiaprima.repositories.MateriaPrimaRepository;
import eapli.base.mensagem.domain.Mensagem;
import eapli.base.mensagem.domain.TipoMensagem;
import eapli.base.mensagem.repositories.MensagemRepository;
import eapli.base.execucaoOrdemProducao.domain.CodInternoLote;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.ordemproducao.domain.CodInternoOrdemDeProducao;
import eapli.base.ordemproducao.domain.OrdemProducao;
import eapli.base.ordemproducao.domain.Status;
import eapli.base.ordemproducao.repositories.OrdemProducaoRepository;
import eapli.base.produto.domain.CodFabrico;
import eapli.base.produto.domain.Produto;
import eapli.base.produto.repositories.ProdutoRepository;
import eapli.base.server.utils.ServerUtils;
import eapli.base.utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

/**
 *
 * @author PC
 */
public class ProcessamentoRunnable implements Runnable {

    // Repository
    private final MensagemRepository msgRepository = PersistenceContext.repositories().mensagens();
    private final DepositosRepository depositoRepository = PersistenceContext.repositories().depositos();
    private final OrdemProducaoRepository ordemProducaoRepository = PersistenceContext.repositories().ordensProducao();
    private final ExecucaoOrdemProducaoRepository execucaoOrdemProducaoRepository = PersistenceContext.repositories().execucaoOrdensProducao();
    private final ProdutoRepository produtoRepository = PersistenceContext.repositories().produtos();
    private final MateriaPrimaRepository materiaPrimaRepository = PersistenceContext.repositories().materiasPrimas();
    private final LinhasProducaoRepository linhaRepository = PersistenceContext.repositories().linhasProducao();
    private final MaquinasRepository maquinaRepository = PersistenceContext.repositories().maquinas();

    //var de controlo de fim da thread
    private final Date dataInicial;
    private final Date dataFinal;
    private final LinhaProducao linha;
    private OrdemProducao ordem;
    private String strOrdem = "";

    private Maquina lastMaquina;

    public ProcessamentoRunnable(Date dataInicial, Date dataFinal, LinhaProducao linha) {
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
        this.linha = linha;
    }

    @Override
    public void run() {

        processamentoMensagens();

        try {
            List<Maquina> sortedMaquinas = new ArrayList<>(linha.maquinas());
            Collections.sort(sortedMaquinas, new Utils.OrderMaquinas());
            if (!sortedMaquinas.isEmpty()) {
                this.lastMaquina = sortedMaquinas.get(sortedMaquinas.size() - 1);
            } else {
                return;
            }
        } catch (Exception ex) {
            return;
        }

        Iterable<Mensagem> itMensagens = findMensagensPorProcessarByLinhaProducao(linha);
        List<Mensagem> lstMensagens = new ArrayList<>();
        itMensagens.forEach(lstMensagens::add);
        Collections.sort(lstMensagens, new Utils.OrderMensagens());
        //Pegar nas Mensagens por processar da Linha de Produção
        int count = 0;
        for (Mensagem msg : lstMensagens) {
            count++;
            boolean isValid = betweenTime(msg) && ServerUtils.validarMensagem(msg);
            if (isValid) {
                processarMensagem(msg);
            }
        }
    }

    /**
     * Verifica se a mensagem está entre os limites de tempo estabelecidos pelo Utilizador
     *
     * @param msg
     * @return
     */
    private boolean betweenTime(Mensagem msg) {
        Date data = msg.data();
        return data.after(dataInicial) && data.before(dataFinal);
    }

    private synchronized void processarMensagem(Mensagem mensagem) {
        String[] msgFields = mensagem.mensagem().split(";");
        String strTipoMensagem = msgFields[1];
        TipoMensagem tipoMensagem = TipoMensagem.getTipoMensagem(strTipoMensagem);
        if (tipoMensagem == null) {
            return;
        }
        mensagem.especificarTipo(tipoMensagem);
        printMessage(mensagem);
        Deposito deposito;
        Material material;
        AtividadeMaquina evento;
        Produto produto;
        Lote lote;
        ExecucaoOrdemProducao exec = null;
        switch (tipoMensagem) {
            case INICIO_ATIVIDADE:
                if (this.ordem == null) {
                    String strOrdemProducao = msgFields[3];
                    this.ordem = getOrdemProducao(strOrdemProducao);
                    strOrdem = this.ordem.identity().toString();
                }
                try {
                    exec = getExecucaoOrdemProducao(strOrdem);
                    if (exec == null) {
                        throw new NoSuchElementException();
                    }
                } catch (NoSuchElementException ex) {
                    executarOrdem(this.ordem);
                    exec = createExecucaoOrdemProducao(mensagem);
                }
                evento = new AtividadeMaquina(mensagem.maquina(), TipoAtividade.INICIO_ATIVIDADE, mensagem.data());
                registarEventoMaquina(exec, evento);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                break;

            case FIM_ATIVIDADE:
                if (this.ordem == null) {
                    String strOrdemProducao = msgFields[3];
                    this.ordem = getOrdemProducao(strOrdemProducao);
                    strOrdem = this.ordem.identity().toString();
                }
                exec = getExecucaoOrdemProducao(strOrdem);
                evento = new AtividadeMaquina(mensagem.maquina(), TipoAtividade.FIM_ATIVIDADE, mensagem.data());
                registarEventoMaquina(exec, evento);
                saveMensagem(mensagem);

                if (this.lastMaquina.sameAs(mensagem.maquina())) {
                    terminarOrdem(this.ordem);
                    registarFimExecucaoOrdemProducao(exec, mensagem.data());
                }
                saveExecucaoOrdemProducao(exec);
                break;

            case RETOMA_ATIVIDADE:
                exec = getExecucaoOrdemProducao(strOrdem);
                evento = new AtividadeMaquina(mensagem.maquina(), TipoAtividade.RETOMA_ATIVIDADE, mensagem.data());
                registarEventoMaquina(exec, evento);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                //ativarMaquina(mensagem.maquina());
                break;

            case PARAGEM:
                exec = getExecucaoOrdemProducao(strOrdem);
                evento = new AtividadeMaquina(mensagem.maquina(), TipoAtividade.PARAGEM, mensagem.data());
                registarEventoMaquina(exec, evento);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                //desativarMaquina(mensagem.maquina());
                break;

            case CONSUMO:
                if (this.ordem != null) {
                    exec = getExecucaoOrdemProducao(strOrdem);
                }
                deposito = getDeposito(msgFields[5]);
                material = getMaterial(msgFields[3]);
                Consumo newConsumo = new Consumo(Double.parseDouble(msgFields[4]), material, deposito, mensagem.data());
                registarConsumo(exec, newConsumo);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                break;

            case ESTORNO:
                if (this.ordem != null) {
                    exec = getExecucaoOrdemProducao(strOrdem);
                }
                deposito = getDeposito(msgFields[5]);
                material = getMaterial(msgFields[3]);
                Estorno newEstorno = new Estorno(Double.parseDouble(msgFields[4]), material, deposito, mensagem.data());
                registarEstorno(exec, newEstorno);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                break;

            case ENTREGA_PRODUCAO:
                exec = getExecucaoOrdemProducao(strOrdem);
                deposito = getDeposito(msgFields[5]);
                produto = (Produto) getProduto(msgFields[3]);
                EntregaProducao entrega = new EntregaProducao(Double.parseDouble(msgFields[4]), produto, deposito, mensagem.data());
                registarEntrega(exec, entrega);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                break;

            case PRODUCAO:
                exec = getExecucaoOrdemProducao(strOrdem);
                lote = new Lote(CodInternoLote.valueOf(msgFields[5]), mensagem.data(), Double.parseDouble(msgFields[4]));
                registarProducao(exec, lote);
                saveMensagem(mensagem);
                saveExecucaoOrdemProducao(exec);
                break;

            default:
                break;
        }
    }

    private void printMessage(Mensagem m) {
        System.out.println(m.maquina().identity() + " - " + m.tipoMensagem().codigo() + " - " + m.data().toString());
    }

    private ExecucaoOrdemProducao createExecucaoOrdemProducao(Mensagem mensagem) {
        ExecucaoOrdemProducao exec = new ExecucaoOrdemProducao(ordem, mensagem.data(), this.linha);
        return exec;
    }

    private Iterable<Mensagem> findMensagensPorProcessarByLinhaProducao(LinhaProducao linha) {
        return msgRepository.findPorProcessarByLinhaProducao(linha);
    }

    private void saveMensagem(Mensagem mensagem) {
        msgRepository.save(mensagem);
    }

    private void saveExecucaoOrdemProducao(ExecucaoOrdemProducao exec) {
        execucaoOrdemProducaoRepository.save(exec);
    }

    private OrdemProducao getOrdemProducao(String id) {
        if (ordemProducaoRepository.containsOfIdentity(CodInternoOrdemDeProducao.valueOf(id))) {
            return ordemProducaoRepository.ofIdentity(CodInternoOrdemDeProducao.valueOf(id)).get();
        }
        return null;
    }

    private Deposito getDeposito(String id) {
        if (depositoRepository.containsOfIdentity(CodigoInternoDeposito.valueOf(id))) {
            return depositoRepository.ofIdentity(CodigoInternoDeposito.valueOf(id)).get();
        }
        return null;
    }

    public ExecucaoOrdemProducao getExecucaoOrdemProducao(String id) {
        if (execucaoOrdemProducaoRepository.containsOfIdentity(CodInternoOrdemDeProducao.valueOf(id))) {
            return execucaoOrdemProducaoRepository.ofIdentity(CodInternoOrdemDeProducao.valueOf(id)).get();
        }
        return null;
    }

    private void registarEventoMaquina(ExecucaoOrdemProducao exec, AtividadeMaquina evento) {
        exec.registarEventoMaquina(evento);
    }

    private void registarConsumo(ExecucaoOrdemProducao exec, Consumo consumo) {
        exec.registarConsumo(consumo);
    }

    private void registarEstorno(ExecucaoOrdemProducao exec, Estorno estorno) {
        exec.registarEstorno(estorno);
    }

    private void registarEntrega(ExecucaoOrdemProducao exec, EntregaProducao entrega) {
        exec.registarEntregaProducao(entrega);
    }

    private void registarProducao(ExecucaoOrdemProducao exec, Lote lote) {
        exec.registarProducao(lote);
    }

    private void registarFimExecucaoOrdemProducao(ExecucaoOrdemProducao exec, Date data) {
        exec.finalizarExecucaoOrdemProducao(data);
    }

    private void ativarMaquina(Maquina maquina) {
        maquina.activate();
        this.maquinaRepository.save(maquina);
    }

    private void desativarMaquina(Maquina maquina) {
        maquina.deactivate();
        this.maquinaRepository.save(maquina);
    }

    private void executarOrdem(OrdemProducao ordem) {
        ordem.atualizarStatus(Status.EM_EXECUCAO);
        this.ordemProducaoRepository.save(ordem);
    }

    private void terminarOrdem(OrdemProducao ordem) {
        ordem.atualizarStatus(Status.CONCLUIDA);
        this.ordemProducaoRepository.save(ordem);
    }

    private void processamentoMensagens() {
        Date date = new Date();
        this.linha.processarMensagens(date);
        linhaRepository.save(this.linha);
    }

    private Material getMaterial(String id) {
        if (getMateriaPrima(id) != null) {
            return getMateriaPrima(id);
        }
        if (getProduto(id) != null) {
            return getProduto(id);
        }
        return null;
    }

    private Material getMateriaPrima(String id) {
        if (materiaPrimaRepository.containsOfIdentity(CodigoInterno.valueOf(id))) {
            return materiaPrimaRepository.ofIdentity(CodigoInterno.valueOf(id)).get();
        }
        return null;
    }

    private Material getProduto(String id) {
        if (produtoRepository.containsOfIdentity(CodFabrico.valueOf(id))) {
            return produtoRepository.ofIdentity(CodFabrico.valueOf(id)).get();
        }
        return null;
    }

}
