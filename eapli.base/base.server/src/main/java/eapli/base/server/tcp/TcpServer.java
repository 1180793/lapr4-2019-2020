package eapli.base.server.tcp;

import eapli.base.Application;
import eapli.base.server.spm.runnable.ImportFileMessagesRunnable;
import eapli.base.server.tcp.runnable.TcpMessageThread;
import eapli.base.server.tcp.runnable.TcpServerThread;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TcpServer {

    private static final int PORT_NUMBER = 30602;

    public static Semaphore semaphore = new Semaphore(1);

    static final String TRUSTED_STORE = "server_J.jks";

    static final String KEYSTORE_PASS = "password";

    static ServerSocket sock;

    private static final File FOLDER = new File(Application.settings().getMensagensPath());
    //private static final File FOLDER = new File("dados_teste" + File.separator + "Maquinas");
    private static final File[] LIST_OF_FILES = FOLDER.listFiles();
    private static final List<File> FILE_ARRAY = new ArrayList<>();

    public static void main(String args[]) throws Exception {
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(java.util.logging.Level.OFF);
        
        SSLServerSocket sslSock = null;

        Socket cliSock;

        // Trust these certificates provided by authorized clients
        System.setProperty("javax.net.ssl.trustStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.trustStorePassword", KEYSTORE_PASS);

        // Use this certificate and private key as server certificate
        System.setProperty("javax.net.ssl.keyStore", TRUSTED_STORE);
        System.setProperty("javax.net.ssl.keyStorePassword", KEYSTORE_PASS);

        SSLServerSocketFactory sslF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        try {

            sslSock = (SSLServerSocket) sslF.createServerSocket(PORT_NUMBER);
            sslSock.setNeedClientAuth(true);
            
            System.out.println("Socket Open");
        } catch (IOException ex) {
            System.out.printf("Failed to open server socket! Port: %d\n", PORT_NUMBER);
            ex.printStackTrace();
            System.exit(1);
        }

        for (File file : LIST_OF_FILES) {
            if (file.isFile()) {
                FILE_ARRAY.add(file);
            }
        }

        FILE_ARRAY.forEach((file) -> {
            new Thread(new ImportFileMessagesRunnable(file)).start();
        });

        while (true) {
            cliSock = sslSock.accept();
            System.out.println("Recebe conexão da Máquina");
            Thread threadHello = new Thread(new TcpServerThread(cliSock));
            threadHello.start();
            Thread threadMessage = new Thread(new TcpMessageThread(cliSock));
            threadMessage.start();
        }
    }
}
