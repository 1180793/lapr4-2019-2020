/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.base.server.tcp.runnable;

import eapli.base.infrastructure.persistence.PersistenceContext;
import eapli.base.linhaproducao.domain.Maquina;
import eapli.base.linhaproducao.repositories.MaquinasRepository;
import eapli.base.server.tcp.TcpServer;
import eapli.base.server.utils.ServerUtils;
import eapli.base.server.utils.TcpUtils;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TcpServerThread implements Runnable {

    private Socket s;
    private DataOutputStream sOut;
    private DataInputStream sIn;
    private final MaquinasRepository maquinaRepository = PersistenceContext.repositories().maquinas();
    private Maquina maquina;

    public TcpServerThread(Socket cli_s) {
        s = cli_s;
    }

    @Override
    public void run() {
        long version, aux, i, code, idMaquina, dataLength;
        String rawData = "";

        InetAddress clientIP;

        clientIP = s.getInetAddress();
        try {

            TcpServer.semaphore.acquire();

            System.out.println("Nova conexão de uma Máquina. - Hello Request");
            System.out.println("Endereço = '" + clientIP.getHostAddress() + "'");
            System.out.println("Port = '" + s.getPort() + "'");

            sOut = new DataOutputStream(s.getOutputStream());
            sIn = new DataInputStream(s.getInputStream());

            version = 0;
            aux = 1;
            for (i = 0; i < ServerUtils.VERSION_LENGTH; i++) {
                version = version + aux * sIn.read();
                aux = aux * 256;
            }

            // Ler Message Code
            code = 0;
            aux = 1;
            for (i = 0; i < ServerUtils.CODE_LENGTH; i++) {
                code = code + aux * sIn.read();
                aux = aux * 256;
            }

            // Ler ID Máquina
            idMaquina = 0;
            aux = 1;
            for (i = 0; i < ServerUtils.ID_LENGTH; i++) {
                idMaquina = idMaquina + aux * sIn.read();
                aux = aux * 256;
            }

            // Ler Data Length
            dataLength = 0;
            aux = 1;
            for (i = 0; i < ServerUtils.DATA_LENGTH_LENGTH; i++) {
                dataLength = dataLength + aux * sIn.read();
                aux = aux * 256;
            }

            // Ler Raw Data
            byte[] bytesRawData;
            if (dataLength > 0) {
                bytesRawData = new byte[(int) dataLength];
                sIn.read(bytesRawData);
                rawData = new String(bytesRawData, StandardCharsets.UTF_8);
            }

            // Match Máquina
            for (Maquina maq : maquinaRepository.findAll()) {
                if (maq.identificadorUnico() == idMaquina) {
                    this.maquina = maq;
                }
            }
//            if (maquina != null) {
//                System.out.printf("ENCONTRAMOS A MAQUINA COM ID : %d", maquina.identificadorUnico());
//            }

            if (code == ServerUtils.HELLO) {
                if (maquina == null) {
                    TcpUtils.sendMessage(sOut, version, ServerUtils.NACK, idMaquina, "");
                } else {
                    this.maquina.atualizarEnderecoRede(clientIP.getHostAddress());
                    this.maquinaRepository.save(maquina);
                    TcpUtils.sendMessage(sOut, version, ServerUtils.ACK, idMaquina, "");
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("IOException");
        } catch (InterruptedException ex) {
            Logger.getLogger(TcpServerThread.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("Máquina disconectada. - HELLO REQUEST AND SEND");
        System.out.println("Endereço = '" + clientIP.getHostAddress() + "'");
        System.out.println("Port = '" + s.getPort() + "'");
        TcpServer.semaphore.release();
    }
}
